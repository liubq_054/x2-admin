layui.use(['layer', 'form', 'admin', 'ax','func' ], function () {
    var $ = layui.jquery;
    var $ax = layui.ax;
    var form = layui.form;
    var admin = layui.admin;

    //获取部门信息
    var ajax = new $ax(Feng.ctxPath + "/dept/detail/" + Feng.getUrlParam("deptId"));
    var result = ajax.start();
    form.val('deptForm', result);
    form.render();

    /**绑定上级点击事件*/
    $('#pName').click(function () {

    });


    // 添加表单验证方法
    form.verify({
        maxlength6: function(value) {
            if (value.length > 30) return "长度不能大于6";
        },
        maxlength50: function(value) {
            if (value.length > 30) return "长度不能大于50";
        },
        maxlength255: function(value) {
            if (value.length > 30) return "长度不能大于255";
        }
    });

    // 表单提交事件
    form.on('submit(btnSubmit)', function (data) {
        var ajax = new $ax(Feng.ctxPath + "/dept/update", function (result) {
            Feng.success("修改成功！");
            admin.putTempData('formOk', true);
            admin.closeThisDialog();
        }, function (data) { Feng.error("修改失败！" + data.responseJSON.message) });
        ajax.set(data.field);
        ajax.start();
        return false;
    });

});
