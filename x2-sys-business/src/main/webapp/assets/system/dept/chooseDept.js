var SimpleSelect = {tree:null};
SimpleSelect.callbackFun = null;

//初始化页面
SimpleSelect.initSelect = function (selectVO, callback) {
    SimpleSelect.selectVO = selectVO;
    SimpleSelect.selectedIdList = SimpleSelect.selectVO.selectedIdList;
    if (!SimpleSelect.selectedIdList) {
        SimpleSelect.selectedIdList = "";
    } else {
        SimpleSelect.selectedIdList = "," + SimpleSelect.selectedIdList + ",";
    }
    SimpleSelect.callbackFun = callback;
    layui.use(['layer', 'ax', 'util'], function () {
        var $ = layui.jquery;
        var form = layui.form;
        var $ax = layui.ax;
        var tree = layui.tree;
        /**加载树*/
        SimpleSelect.loadTree = function () {
            var result;
            var ajax = new $ax("/dept/layuiTree");
            if (SimpleSelect.selectVO.extendCond) {
                ajax.set(SimpleSelect.selectVO.extendCond);
            }
            result = ajax.start();
            if (SimpleSelect.selectVO.openMode == 'multiple') {
                SimpleSelect.tree = tree.render({
                    elem: '#deptSelectTree',
                    data: SimpleSelect.preProcess(result),
                    onlyIconControl: false,
                    showCheckbox: true,
                    autoCheckParent: false,
                    autoCheckChild:false,
                    edit: false                 
            });
            } else {
                SimpleSelect.tree = tree.render({
                    elem: '#deptSelectTree',
                    data: SimpleSelect.preProcess(result),
                    edit: false,
                    click: function (node) {
                        var dataList = [];
                        dataList.push({
                            "deptId": node.data.id,
                            "fullName": node.data.title,
                            "simpleName": node.data.title,
                            "deptPathName": node.data.extendInfo
                        });
                        SimpleSelect.callbackFun(dataList);
                    }
                });
                //不需要确定
                $("#okBtn").hide();
            }

        };
        //前处理
        SimpleSelect.preProcess = function (dataTree) {
            //第一级关闭，不打开
            var firstChildList = dataTree[0].children;
            for (var idx = 0; idx < firstChildList.length; idx++) {
                firstChildList[idx]['spread'] = false;
            }
            //选择数据
            SimpleSelect.selectData(dataTree);
            return dataTree;
        };
        //选择数据
        SimpleSelect.selectData = function (dataTree) {
            if (SimpleSelect.selectedIdList.length > 0) {
                for (var ii = 0; ii < dataTree.length; ii++) {
                    if (SimpleSelect.selectedIdList.indexOf("," + dataTree[ii].id + ",") >= 0) {
                        dataTree[ii]['checked'] = true;
                    }
                    SimpleSelect.selectData(dataTree[ii].children);
                }
            }
        };

        $("#deptSelectTreeDiv").show();

        SimpleSelect.loadTree();

        $("#okBtn").click(function () {
            var deptList = [];
            var inputObj;
            var value;
            $("#deptSelectTree").find(".layui-form-checked").each(function () {
                inputObj = $(this).prev()[0];
                value = $(inputObj).attr("svalue");
                deptList.push({"deptId": inputObj.value, "fullName": value, "simpleName": value});
            });
            SimpleSelect.callbackFun(deptList);
        });

    });
};