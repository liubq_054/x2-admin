/**
 * 角色详情对话框
 */
var RoleInfoDlg = {
    data: {
        pid: "",
        pName: ""
    }
};
function setSelectData(node){
    RoleInfoDlg.data.pid = node.id;
    RoleInfoDlg.data.pName = node.name;
    layui.jquery("#pid").val(node.id);
    layui.jquery("#pName").val(node.name);
}

layui.use(['layer', 'form', 'admin', 'ax'], function () {
    var $ = layui.jquery;
    var $ax = layui.ax;
    var form = layui.form;
    var admin = layui.admin;
    var layer = layui.layer;

    //初始化角色的详情数据
    var ajax = new $ax(Feng.ctxPath + "/role/view/" + Feng.getUrlParam("roleId"));
    var result = ajax.start();
    form.val('roleForm',result.data);

    // 点击上级角色时
    $('#pName').click(function () {
        var cond = {};
        cond['title'] = "父级角色选择";
        cond['formUrl'] = "common/tree_dlg.html";
        cond['listUrl'] = "/role/roleTreeList";
        cond['extendCond'] = {};
        cond['height'] = 550;
        cond['width'] = 400;
        kitFun.show(cond,function (data) {
            var tempPid = "";
            var tempPName = "";
            if(data.length > 0){
                tempPid = data[0].treeId;
                tempPName = data[0].treeName;
            }
            $("#pid").val(tempPid);
            $("#pName").val(tempPName);
            RoleInfoDlg.data.pid =  tempPid;
            RoleInfoDlg.data.pName =  tempPName;
        })
    });

    // 表单提交事件
    form.on('submit(btnSubmit)', function (data) {
        var ajax = new $ax(Feng.ctxPath + "/role/edit", function (data) {
            Feng.success("修改成功!");

            //传给上个页面，刷新table用
            admin.putTempData('formOk', true);

            //关掉对话框
            admin.closeThisDialog();

        }, function (data) {
            Feng.error("修改失败!" + data.responseJSON.message + "!");
        });
        ajax.set(data.field);
        ajax.start();

        //添加 return false 可成功跳转页面
        return false;
    });
});