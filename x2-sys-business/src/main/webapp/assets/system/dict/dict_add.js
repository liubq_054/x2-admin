/**
 * 详情对话框
 */
var DictInfoDlg = {
    data: {
        dictTypeId: "",
        code: "",
        name: "",
        parentId: "",
        parentName: "",
        status: "",
        description: "",
        createTime: "",
        updateTime: "",
        createUser: "",
        updateUser: ""
    }
};
function setSelectData(node){
    DictInfoDlg.data.parentId = node.id;
    DictInfoDlg.data.parentName = node.name;
    layui.jquery("#parentId").val(node.id);
    layui.jquery("#parentName").val(node.name);
}
layui.use(['form', 'ax'], function () {
    var $ = layui.jquery;
    var $ax = layui.ax;
    var form = layui.form;
    var admin = layui.admin;

    //表单提交事件
    form.on('submit(btnSubmit)', function (data) {
        var ajax = new $ax(Feng.ctxPath + "/dict/addItem", function (data) {
            Feng.success("添加成功！");

            //传给上个页面，刷新table用
            admin.putTempData('formOk', true);

            //关掉对话框
            admin.closeThisDialog();

        }, function (data) {
            Feng.error("添加失败！" + data.responseJSON.message)
        });
        ajax.set(data.field);
        ajax.start();

        return false;
    });

    //父级字典时
    $('#parentName').click(function () {
        var formName = encodeURIComponent("parent.DictInfoDlg.data.parentName");
        var formId = encodeURIComponent("parent.DictInfoDlg.data.parentId");
        var treeUrl = encodeURIComponent("/dict/ztree?dictTypeId=" + $("#dictTypeId").val());

        var url =  Feng.ctxPath + '/system/commonTree?formName=' + formName + "&formId=" + formId + "&treeUrl=" + treeUrl;
        var options = {
            type: 2,
            title: '父级字典',
            width: "350",
            height: "400",
            url: url,
            callBack: function (index, layero) {
                var body = layer.getChildFrame('body', index);
                $("#parentId").val(body.find('#treeId').val());
                $("#parentName").val(body.find('#treeName').val());
                DictInfoDlg.data.parentId =  $("#parentId").val();
                DictInfoDlg.data.parentName =  $("#parentName").val();
                layer.close(index);
            }
        };
        Feng.openModal(options);
        /*layer.open({
            type: 2,
            title: '父级字典',
            area: ['300px', '400px'],
            content: Feng.ctxPath + '/system/commonTree?formName=' + formName + "&formId=" + formId + "&treeUrl=" + treeUrl,
            end: function () {
                $("#parentId").val(DictInfoDlg.data.parentId);
                $("#parentName").val(DictInfoDlg.data.parentName);
            }
        });*/
    });
});