layui.use(['layer', 'form', 'admin', 'ax', 'upload'], function () {
	var $ = layui.jquery;
	var $ax = layui.ax;
	var form = layui.form;
	var admin = layui.admin;
	var laydate = layui.laydate;
	var upload = layui.upload;

	laydate.render({ elem: '#birthday' });
	laydate.render({ elem: '#graduateTime' });
	laydate.render({ elem: '#entryTime' });
	laydate.render({ elem: '#quitDate' });


	$('#deptName').click(function () {
		SysCommon.deptSelect({},function (data){
			if(data.length > 0){
				$('#deptId').val(data[0].deptId);
				$('#deptName').val(data[0].simpleName);
			} else  {
				$('#deptId').val("");
				$('#deptName').val("");
			}
		})
	});

	// 添加表单验证方法
	form.verify({
		maxlength30: function(value) {
			if (value.length > 30) return "长度不能大于30";
		},
		maxlength50: function(value) {
			if (value.length > 30) return "长度不能大于50";
		},
		maxlength255: function(value) {
			if (value.length > 30) return "长度不能大于255";
		},
		checkAccount: function (value) {
			if ($('#isLoginY').prop('checked') == true && value == "") {
				return "登录账号不能为空";
			}
		},
		checkPassword: function (value) {
			if ($('#isLoginY').prop('checked') == true && value == "") {
				return "登录密码不能为空";
			}
		},
		checkEmail: function(value) {
			if (value != "") {
				if (value.length > 30) {
					return "长度不能大于30";
				}
				if (!/(^$)|^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
					return '邮箱格式不正确';
				}
			}
		},
		ip: function(value) {
			if (value != "") {
				if (value.length > 15) {
					return "IP地址长度不能大于15";
				}
				if (!/^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/.test(value)) {
					return 'IP地址不符合规则';
				}
			}
		}
	});

	form.on('radio', function (data) {
		var name = data.elem.getAttribute("name");
		if (name == 'isLogin') {
			data.value == 'Y' ? $('#accountContainer').show() : $('#accountContainer').hide();
		}
	});

	// 表单提交事件
	form.on('submit(btnSubmit)', function (data) {
		var ajax = new $ax(Feng.ctxPath + "/mgr/add", function (result) {
			if (result.code != 200) {
				Feng.error(result.message);
				return;
			}
			Feng.success("添加成功！");
			top.layui.index.refreshTab({"title": "员工档案", "url": "/mgr"});
		}, function (data) { Feng.error("添加失败！" + data.responseJSON.message) });
		ajax.set(data.field);
		ajax.start();
		return false;
	});

	upload.render({
		elem: '#uploadPhoto',
		url: Feng.ctxPath + '/sys/file/upload',
		before: function(obj){
			obj.preview(function(index, file, result) { $('#photo').attr('src', result); });
		},
		done: function(res) {
			if(res.code !== 0){
				Feng.error(res.message);
				return;
			}
			$('#avatar').val(res.data.fileId);
		}
	});

	$('#cancelBtn').click(function () {
		top.layui.index.closeNowTab();
	});
});
