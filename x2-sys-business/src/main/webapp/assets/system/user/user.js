var userModel = [];
userModel.layuiModule = null;

//点击人员查看
userModel.userClick = function(userId) {
    userModel.layuiModule.userView(userId);
};

layui.use(['layer', 'form', 'table', 'admin', 'ax', 'func', 'tree', 'util', 'tableCheckBoxUtil','upload', 'element'], function () {
    var layer = layui.layer;
    var form = layui.form;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;
    var tree = layui.tree;
    var upload = layui.upload;
    var element = layui.element;

    /**系统管理--用户管理*/
    var MgrUser = {
        table: null,
        tableId: "userTable",
        condition: ['name', 'deptId'],
        selectTreeInfo: {deptId: '', deptName: ''}
    };


    /**
     * 初始化表格的列
     */
    MgrUser.initColumn = function () {
        return [[
            {type: 'numbers', fixed: 'left', title: '序号'},
            {field: 'name', sort: false, title: '姓名', fixed: 'left', align: 'center',
                templet: function(res) {
                    return '<a href="javascript:void(0);" style="color: #01AAED;" onclick="userModel.userClick(\'' + res.userId + '\')">' + res.name + '</a>';
                }
            },
            {field: 'account', sort: false, title: '账号',  align: 'center'},
            {field: 'deptName', sort: false, title: '部门名称',  align: 'center'},
            {field: 'cardTypeValue', sort: false, title: '证件类型',  align: 'center'},
            {field: 'cardNo', sort: false, title: '证件号',  align: 'center'},
            {field: 'phone', sort: false, title: '手机号',  align: 'center'},
            {align: 'center', toolbar: '#tableBar', title: '操作',  fixed: 'right'}
        ]];
    };

    /**拼装条件*/
    MgrUser.queryParams = function () {
        var queryData = {n: new Date().getTime()};
        for (var i = 0; i < MgrUser.condition.length; i++) {
            var value = $('#' + MgrUser.condition[i]).val();
            if (value.length > 0) {
                queryData[MgrUser.condition[i]] = value;
            }
        }
        return queryData;
    };

    /**点击查询按钮*/
    MgrUser.search = function () {
        table.reload(MgrUser.tableId, {page: {curr: 1}});
    };

    /**
     * 点击删除用户按钮
     * @param data 点击按钮时候的行数据
     */
    MgrUser.onDeleteUser = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/mgr/delete", function (result) {
                if (result.code != 200) {
                    Feng.error(result.message);
                    return;
                }
                table.reload(MgrUser.tableId);
                Feng.success("删除成功!");
            }, function (data) { Feng.error("删除失败!" + data.responseJSON.message + "!"); });
            ajax.set("userId", data.userId);
            ajax.start();
        };
        Feng.confirm("是否删除用户" + data.name + "?", operation);
    };

    MgrUser.table = table.render({
        elem: '#' + MgrUser.tableId,
        url: Feng.ctxPath + '/mgr/list',
        toolbar: '#toolbarDemo', //开启头部工具栏，并为其绑定左侧模板
        defaultToolbar: ['filter', {title: '导出', layEvent: 'EXPORT', icon: 'layui-icon-export'}, 'print'],
        queryParams: MgrUser.queryParams,
        page: true,
        limit: 20,
        height: "full-98",
        cellMinWidth: 100,
        cols: MgrUser.initColumn()
    });
    table.on('toolbar(' + MgrUser.tableId + ')', function (obj) {
        if (obj.event === 'EXPORT') {
            xyexport.do(layui, MgrUser.table, {"headPk":"userId"});
        }
    });

    // 初始化部门树
    var ajax = new $ax(Feng.ctxPath + "/dept/layuiTree", function (data) {
        tree.render({
            elem: '#userSelectDeptTree',
            data: data,
            click: function (obj) {
                MgrUser.selectTreeInfo.deptId = obj.data.id;
                MgrUser.selectTreeInfo.deptName = obj.data.title;
                $('#deptId').val(obj.data.id);
                MgrUser.search();
            },
            onlyIconControl: true
        });
    }, function (data) {});
    ajax.start();

    table.on('tool(' + MgrUser.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'edit') {
            var url = Feng.ctxPath + '/mgr/user_edit?userId=' + data.userId;
            var title = '修改员工档案';
            kitFun.openTab({title: title, url: url});
        } else if (layEvent === 'delete') {
            MgrUser.onDeleteUser(data);
        }
    });

    /**点击人员查询*/
    MgrUser.userView = function (userId) {
        var url = Feng.ctxPath + '/mgr/user_show?userId=' + userId;
        var title = '查看员工档案';
        kitFun.openTab({title: title, url: url});
    };

    //选完文件后不自动上传
    upload.render({
        elem: '#selFile'
        ,url: Feng.ctxPath+"/mgr/importUser"//改成您自己的上传接口
        ,auto: true
        // ,multiple: true
        ,accept: 'file' //普通文件
        ,exts: 'xlsx|xls'
        // ,bindAction: '#impFile'
        ,choose: function (obj) {
            var files = this.files = obj.pushFile();  //将每次选择的文件追加到文件队列
            //读取本地文件
            // obj.preview(function (index, file, result) {
            //     console.log(file.name);
            // });
        }
        ,done: function(data,index){
            if (data.success) {
                Feng.successCallback("导入成功", function () {
                    MgrUser.table.reload({page: {curr: 1}});
                });
            } else {
                Feng.alert(data.message);
            }
            //删除数组文件中上传成功的图片，防止重复上传
            delete this.files[index];
            Feng.closeLoading();
        },
        error: function (res) {
            Feng.closeLoading();
        }
    });

    //人员导入模板下载
    MgrUser.openDown = function () {
        window.open(Feng.ctxPath + '/assets/excel/user_model.xlsx');
    };

    $('#btnAdd').click(function() {
        var url = Feng.ctxPath + '/mgr/user_add?selectDeptId=' + MgrUser.selectTreeInfo.deptId + "&selectDeptName=" + encodeURIComponent(MgrUser.selectTreeInfo.deptName);
        var title = '添加员工档案';
        kitFun.openTab({title: title, url: url});
    });

    $('#btnSearch').click(function () {
        MgrUser.search();
    });

    $('#btnReset').click(function() {
        for (var i = 0; i < MgrUser.condition.length; i++) {
            $('#' + MgrUser.condition[i]).val("");
        }
        form.render();
    });

    //点击人员导入模板下载
    $('#btnDown').click(function () {
        MgrUser.openDown();
    });

    userModel.layuiModule = MgrUser;
});

$(function () {
    var panehHidden = false;
    if ($(this).width() < 769) {
        panehHidden = true;
    }
    $('#myContiner').layout({initClosed: panehHidden, west__size: 260});
});
