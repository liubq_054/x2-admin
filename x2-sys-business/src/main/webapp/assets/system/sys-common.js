var SysCommon = {};
//菜单选择
SysCommon.menuSelect = function (extendCond,callback){
    var cond = {};
    cond['title'] = "菜单选择";
    cond['formUrl'] = "/system/menu/chooseMenu.html";
    cond['extendCond'] = extendCond;
    cond['height'] = 580;
    cond['width'] = 500;
    kitFun.show(cond,callback)
}
//部门选择
SysCommon.deptSelect = function (extendCond,callback){
    var cond = {};
    cond['title'] = "部门选择";
    cond['formUrl'] = "/system/dept/chooseDept.html";
    cond['extendCond'] = extendCond;
    cond['height'] = 580;
    cond['width'] = 500;
    kitFun.show(cond,callback)
}