/**
 * 详情对话框
 */
var MenuInfoDlg = {
    data: {
        pid: "",
        pcodeName: ""
    }
};
function setSelectData(node){
    MenuInfoDlg.data.pid = node.id;
    MenuInfoDlg.data.pcodeName = node.name;
    layui.jquery("#pid").val(node.id);
    layui.jquery("#pcodeName").val(node.name);
}
layui.use(['layer', 'form', 'admin', 'laydate', 'ax', 'iconPicker'], function () {
    var $ = layui.jquery;
    var $ax = layui.ax;
    var form = layui.form;
    var admin = layui.admin;
    var laydate = layui.laydate;
    var layer = layui.layer;
    var iconPicker = layui.iconPicker;


    //获取菜单信息
    var ajax = new $ax(Feng.ctxPath + "/menu/getMenuInfo?menuId=" + Feng.getUrlParam("menuId"));
    var result = ajax.start();
    MenuInfoDlg.data.pid = result.data.pid;
    MenuInfoDlg.data.pcodeName = result.data.pcodeName;
    form.val('menuForm', result.data);

    // 点击父级菜单
    $('#pcodeName').click(function () {
        SysCommon.menuSelect({},function (data){
            if(data.length > 0){
                $('#pid').val(data[0].menuId);
                $('#pcodeName').val(data[0].menuName);
            } else  {
                $('#pid').val("0");
                $('#pcodeName').val("顶级");
            }
        })
    });

    // 表单提交事件
    form.on('submit(btnSubmit)', function (data) {
        var ajax = new $ax(Feng.ctxPath + "/menu/edit", function (data) {
            Feng.success("修改成功！");

            //传给上个页面，刷新table用
            admin.putTempData('formOk', true);

            //关掉对话框
            admin.closeThisDialog();

        }, function (data) {
            Feng.error("修改失败！" + data.responseJSON.message)
        });
        ajax.set(data.field);
        ajax.start();

        //添加 return false 可成功跳转页面
        return false;
    });

    // //初始化图标选择
    // iconPicker.render({
    //     elem: '#icon',
    //     type: 'fontClass',
    //     search: true,
    //     page: true,
    //     limit: 12,
    //     click: function (data) {
    //
    //     }
    // });
    // iconPicker.checkIcon('iconPicker', result.data.icon);
});