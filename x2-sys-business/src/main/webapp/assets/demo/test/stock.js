layui.use(['table', 'admin', 'ax', 'func' , 'laydate'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;
    var laydate = layui.laydate;
    //管理
    var Stock = {
        tableId: "stockTable"
    };


    //初始化表格的列
    Stock.initColumn = function () {
        return [[
            {field: 'productId', sort: true, title: 'productId'},
            {field: 'count', sort: true, title: 'count'},
            {align: 'center', toolbar: '#tableBar', title: '操作', width:250,  fixed: 'right'}
        ]];
    };
    //点击查询按钮
    Stock.queryParams = function () {
        var queryData = {};
        var query_productId = $("#query_productId").val();
        if(query_productId){
            queryData['productId'] = query_productId;
        }

        queryData['n_'] = new Date().getTime();
        return queryData;
    };
    //点击查询按钮
    Stock.search = function () {
        table.reload(Stock.tableId, {page: {curr: 1}});
    };
    //弹出添加对话框
    Stock.openAddDlg = function () {
        func.open({
            title: '添加',
            height: '420',
            content: Feng.ctxPath + '/stock/add',
            tableId: Stock.tableId
        });
    };
    //点击编辑
    Stock.openEditDlg = function (data) {
        func.openEx({
            title: '修改',
            height: '420',
            content: Feng.ctxPath + '/stock/edit?id=' + data.id,
            tableId: Stock.tableId
        });
    };
    //点击查看
    Stock.openViewDlg = function (data) {
        func.openEx({
            title: '查看',
            height: '420',
            content: Feng.ctxPath + '/stock/edit?openMode=look&id=' + data.id,
            tableId: Stock.tableId
        });
    };
    //点击删除
    Stock.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/stock/delete", function (data) {
                if(data.success){
                    Feng.success("删除成功!");
                    table.reload(Stock.tableId);
                } else {
                    Feng.error(data.message);
                }
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };
    // 渲染表格
    Stock.tableResult = table.render({
        elem: '#' + Stock.tableId,
        url: Feng.ctxPath + '/stock/list',
        queryParams: function(){return Stock.queryParams()},
        page: true,
        limit:20,
        height: "full-98",
        cellMinWidth: 100,
        cols: Stock.initColumn()
    });
    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        Stock.search();
    });
    // 重置按钮点击事件
    $("#btnReset").click(function () {
        $('#searchForm')[0].reset();
    });
    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        Stock.openAddDlg();
    });
    // 工具条点击事件
    table.on('tool(' + Stock.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'edit') {
            Stock.openEditDlg(data);
        } else if (layEvent === 'view') {
            Stock.openViewDlg(data);
        } else if (layEvent === 'delete') {
            Stock.onDeleteItem(data);
        }
    });
});