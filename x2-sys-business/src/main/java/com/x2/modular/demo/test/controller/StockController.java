package com.x2.modular.demo.test.controller;


import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.core.base.controller.BaseController;
import com.x2.modular.demo.test.model.StockBean;
import com.x2.modular.demo.test.service.IStockService;
import com.x2.kernel.model.response.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 控制器
 *
 * @author liubq
 * @Date 2020-09-07 10:25:00
 */
@Controller
@RequestMapping("/stock")
public class StockController extends BaseController {

    private String PREFIX = "/demo/test";

    @Autowired
    private IStockService stockService;

    /**
     * 跳转到主页面
     */
    @RequestMapping("/manage")
    public String manage() {
        return PREFIX + "/stock.html";
    }

    /**
     * 新增页面
     */
    @RequestMapping("/add")
    public String add() {
        return edit();
    }

    /**
     * 编辑页面
     */
    @RequestMapping("/edit")
    public String edit() {
        return PREFIX + "/stockForm.html";
    }

    /**
     * 新增接口
     */
    @RequestMapping("/addItem")
    @ResponseBody
    public ResponseData addItem(StockBean param) {
        try {
            this.stockService.add(param);
            return ResponseData.success();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseData.error(ex.getMessage());
        }
    }

    /**
     * 编辑接口
     */
    @RequestMapping("/editItem")
    @ResponseBody
    public ResponseData editItem(StockBean param) {
        try {
            this.stockService.update(param);
            return ResponseData.success();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseData.error(ex.getMessage());
        }
    }

    /**
     * 删除接口
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResponseData delete(StockBean param) {
        try {
            this.stockService.delete(param);
            return ResponseData.success();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseData.error(ex.getMessage());
        }
    }

    /**
     * 查看详情接口
     */
    @RequestMapping("/detail")
    @ResponseBody
    public ResponseData detail(StockBean param) {
        StockBean detail = this.stockService.findBySpec(param);
        return ResponseData.success(detail);
    }

    /**
     * 查询列表
     */
    @ResponseBody
    @RequestMapping("/list")
    public LayuiPageInfo list(StockBean param) {
        return this.stockService.findPageBySpec(param);
    }

}

