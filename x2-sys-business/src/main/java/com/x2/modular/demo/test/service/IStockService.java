package com.x2.modular.demo.test.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.modular.demo.test.entity.Stock;
import com.x2.modular.demo.test.model.StockBean;

import java.util.List;

/**
 * 服务
 *
 * @Version 1.0
 */
public interface IStockService extends IService<Stock> {

    void add(StockBean param) throws Exception;

    void update(StockBean param) throws Exception;

    void delete(StockBean param) throws Exception;

    StockBean findBySpec(StockBean param);

    List<StockBean> findListBySpec(StockBean param);

    LayuiPageInfo findPageBySpec(StockBean param);


}
