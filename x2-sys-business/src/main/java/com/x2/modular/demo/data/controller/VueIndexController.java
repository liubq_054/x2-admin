package com.x2.modular.demo.data.controller;


import com.x2.core.base.controller.BaseController;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;


/**
 * 控制器
 *
 * @author liubq
 * @Date 2020-09-07 10:25:00
 */
@Controller
@RequestMapping("/index")
public class VueIndexController extends BaseController {
    /**
     * 跳转到主页面
     */
    @RequestMapping("/home")
    public String manage() {
        return "/index/home.html";
    }


    @RequestMapping("/list")
    @ResponseBody
    public Map<String, Object> list(HttpServletRequest request) {
        Map<String, Object> resMap = new HashMap<>();
        resMap.put("data", "");
        resMap.put("errcode", 0);
        resMap.put("errmsg", 0);
        return resMap;
    }
}

