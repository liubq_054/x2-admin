package com.x2.modular.demo.test.mapper;

import com.x2.modular.demo.test.entity.Stock;
import com.x2.modular.demo.test.model.StockBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @title  
 * @version  
 */
public interface StockMapper extends BaseMapper<Stock> {

   List<StockBean> queryList(@Param("paramCondition") StockBean paramCondition);

   Page<StockBean> queryPageList(@Param("page") Page page, @Param("paramCondition") StockBean paramCondition);

}
