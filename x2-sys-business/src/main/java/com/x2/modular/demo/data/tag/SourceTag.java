package com.x2.modular.demo.data.tag;


import com.x2.core.tag.ClassSelectOptionVO;
import com.x2.core.tag.IClassTagSelect;

import java.util.ArrayList;
import java.util.List;


public class SourceTag implements IClassTagSelect {


    /**
     * 取得对象
     *
     * @return
     */
    public List<ClassSelectOptionVO> getOptionList() {
        try {
            List<ClassSelectOptionVO> dataLists = new ArrayList<>();
            dataLists.add(new ClassSelectOptionVO("111", "111"));
            dataLists.add(new ClassSelectOptionVO("222", "222"));
            dataLists.add(new ClassSelectOptionVO("333", "333"));
            return dataLists;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
