package com.x2.modular.demo.data.model;


import java.util.HashMap;
import java.util.List;

/**
 * 保存任务
 */
public class DataBean {

    public static HashMap convert(HashMap data) {
        return data;
    }
    public static List<HashMap> convert(List<HashMap> dataList) {
        if (dataList != null) {
            for (HashMap line : dataList) {
                convert(line);
            }
        }
        return dataList;
    }

}
