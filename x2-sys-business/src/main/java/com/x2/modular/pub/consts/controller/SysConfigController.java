package com.x2.modular.pub.consts.controller;


import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.core.base.controller.BaseController;
import com.x2.core.util.ToolUtil;
import com.x2.kernel.model.response.ResponseData;
import com.x2.modular.pub.consts.entity.SysConfig;
import com.x2.modular.pub.consts.model.params.SysConfigParam;
import com.x2.modular.pub.consts.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 参数配置控制器
 * @author stylefeng
 * @date 2019-06-20 14:32:21
 */
@Controller
@RequestMapping("/sysConfig")
public class SysConfigController extends BaseController {

    private final static String PREFIX = "/pub/sysConfig";
    private SysConfigService sysConfigService;

    @Autowired
    public SysConfigController(SysConfigService sysConfigService) {
        this.sysConfigService = sysConfigService;
    }

    /**
     * 跳转到主页面
     * @return 页面地址
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "/sysConfig.html";
    }

    /**
     * 新增页面
     * @return 页面地址
     */
    @RequestMapping("/add")
    public String add() {
        return PREFIX + "/sysConfig_add.html";
    }

    /**
     * 编辑页面
     * @return 页面地址
     */
    @RequestMapping("/edit")
    public String edit() {
        return PREFIX + "/sysConfig_edit.html";
    }

    /**
     * 新增接口
     * @param sysConfigParam: 参数
     * @return 结果
     */
    @RequestMapping("/addItem")
    @ResponseBody
    public ResponseData addItem(SysConfigParam sysConfigParam) {
        this.sysConfigService.add(sysConfigParam);
        return ResponseData.success();
    }

    /**
     * 编辑接口
     * @param sysConfigParam: 参数
     * @return 结果
     */
    @RequestMapping("/editItem")
    @ResponseBody
    public ResponseData editItem(SysConfigParam sysConfigParam) {
        this.sysConfigService.update(sysConfigParam);
        return ResponseData.success();
    }

    /**
     * 删除接口
     * @param sysConfigParam: 参数
     * @return 结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResponseData delete(SysConfigParam sysConfigParam) {
        this.sysConfigService.delete(sysConfigParam);
        return ResponseData.success();
    }

    /**
     * 查看详情接口
     * @param sysConfigParam: 参数
     * @return 结果
     */
    @RequestMapping("/detail")
    @ResponseBody
    public ResponseData detail(SysConfigParam sysConfigParam) {
        SysConfig detail = this.sysConfigService.getById(sysConfigParam.getId());
        return ResponseData.success(detail);
    }

    /**
     * 查询列表
     * @param condition: 参数
     * @return 结果
     */
    @ResponseBody
    @RequestMapping("/list")
    public LayuiPageInfo list(@RequestParam(value = "condition", required = false) String condition) {
        SysConfigParam sysConfigParam = new SysConfigParam();
        sysConfigParam.setCode(condition);
        return this.sysConfigService.findPageBySpec(sysConfigParam);
    }

}


