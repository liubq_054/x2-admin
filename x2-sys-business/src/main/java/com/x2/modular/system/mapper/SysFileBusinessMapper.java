package com.x2.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.x2.modular.system.entity.SysFileBusiness;
import com.x2.modular.system.model.params.SysFileBusinessParam;
import com.x2.modular.system.model.result.SysFileBusinessResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 附件业务关系 Mapper 接口
 * @author changpeng
 * @date 2021-02-03
 */
public interface SysFileBusinessMapper extends BaseMapper<SysFileBusiness> {

    /**
     * 获取列表
     * @param paramCondition: 参数
     * @return 结果列表
     */
    List<SysFileBusinessResult> customList(@Param("paramCondition") SysFileBusinessParam paramCondition);

    /**
     * 获取map列表
     * @param paramCondition：参数
     * @return 结果列表
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") SysFileBusinessParam paramCondition);

    /**
     * 获取分页实体列表
     * @param page: 分页条件
     * @param paramCondition：查询条件
     * @return 分页结果
     */
    Page<SysFileBusinessResult> customPageList(@Param("page") Page page, @Param("paramCondition") SysFileBusinessParam paramCondition);

    /**
     * 获取分页map列表
     * @param page: 分页条件
     * @param paramCondition：查询条件
     * @return 分页结果
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") SysFileBusinessParam paramCondition);

}
