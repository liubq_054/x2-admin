package com.x2.modular.system.service;

import com.x2.base.pojo.node.ZTreeNode;
import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.core.constant.SysTreeNode;
import com.x2.modular.system.entity.UserRole;
import com.x2.modular.system.model.params.UserRoleParam;
import com.x2.modular.system.model.result.UserRoleResult;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户和角色关联表 服务类
 * </p>
 *
 * @author liuhq
 * @since 2019-07-02
 */
public interface UserRoleService extends IService<UserRole> {

    /**
     * 新增
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    void add(UserRoleParam param);

    /**
     * 删除
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    void delete(UserRoleParam param);

    /**
     * 更新
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    void update(UserRoleParam param);

    /**
     * 查询单条数据，Specification模式
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    UserRoleResult findBySpec(UserRoleParam param);

    /**
     * 查询列表，Specification模式
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    List<UserRoleResult> findListBySpec(UserRoleParam param);

    /**
     * 查询分页数据，Specification模式
     *
     * @author liuhq
     * @Date 2019-07-02
     */
     LayuiPageInfo findPageBySpec(UserRoleParam param);

    void setUserRoleAuthority(Long roleId, String userIds);
    void setRoleUserAuthority(Long userId, String roleIds);
    List<SysTreeNode> getSelectTreeData(Long roleId);
    List<SysTreeNode> getSelectTreeDataAllowLogin(Long roleId);
    List<Long> getRoleList(Long userId);

    void deleteByRoleId(Long roleId);
    void deleteByUserId(Long userId);

    List<ZTreeNode> roleZTreeListByUserId(Long userId);
}
