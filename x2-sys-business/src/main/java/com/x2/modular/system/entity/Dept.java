package com.x2.modular.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author stylefeng
 * @since 2019-04-01
 */
@Data
@TableName("sys_dept")
public class Dept implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "dept_id", type = IdType.ASSIGN_ID)
    private Long deptId;
    /**
     * 父部门id
     */
    @TableField("pid")
    private Long pid;
    /**
     * 父级ids
     */
    @TableField("pids")
    private String pids;
    //所属区域
    @TableField("dept_region")
    private Long deptRegion;
    //所属区域
    @TableField("dept_region_name")
    private String deptRegionName;
    /**
     * 简称
     */
    @TableField("simple_name")
    private String simpleName;
    /**
     * 全称
     */
    @TableField("full_name")
    private String fullName;
    /**
     * 部门负责人
     */
    @TableField("leader_id")
    private Long leaderId;
    /**
     * 部门类型
     */
    @TableField("dept_type")
    private String deptType;
    /**
     * 描述
     */
    @TableField("description")
    private String description;
    /**
     * 版本（乐观锁保留字段）
     */
    @TableField("version")
    private Integer version;
    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;
    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;
    /**
     * 创建人
     */
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private Long createUser;
    /**
     * 修改人
     */
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private Long updateUser;


    @Override
    public String toString() {
        return "deptId=" + deptId + ", simpleName=" + simpleName;

    }
}
