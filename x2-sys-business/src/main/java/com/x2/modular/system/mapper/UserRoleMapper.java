package com.x2.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.x2.modular.system.entity.UserRole;
import com.x2.modular.system.model.params.UserRoleParam;
import com.x2.modular.system.model.result.UserRoleResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户和角色关联表 Mapper 接口
 * </p>
 *
 * @author liuhq
 * @since 2019-07-02
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 获取列表
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    List<UserRoleResult> customList(@Param("paramCondition") UserRoleParam paramCondition);

    /**
     * 获取map列表
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") UserRoleParam paramCondition);

    /**
     * 获取分页实体列表
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    Page<UserRoleResult> customPageList(@Param("page") Page page, @Param("paramCondition") UserRoleParam paramCondition);

    /**
     * 获取分页map列表
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") UserRoleParam paramCondition);

}
