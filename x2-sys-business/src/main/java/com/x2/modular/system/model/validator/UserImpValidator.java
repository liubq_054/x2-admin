
/**
 * @CopyRight Changchun Jiacheng Information Technology Co., Ltd.
 **/

package com.x2.modular.system.model.validator;

import cn.hutool.core.util.StrUtil;
import com.x2.core.util.ToolUtil;
import com.x2.modular.system.model.params.UserImpParam;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *@ClassName
 *@Description
 *@author Administrator -Technical Management Architecture
 *@date 2019-04-22 15:15
 *@Version 3.0
 *
 **/
public class UserImpValidator implements Validator {

    private static final String JC_SYS_011 = "JC_SYS_011";

    /**
     * @description 检验类能够校验的类
     * @param arg0
     *            校验的类型
     * @return 是否支持校验
     * @author
     * @version 2014-03-18
     */
    @Override
    public boolean supports(Class<?> arg0) {
        return UserImpParam.class.equals(arg0);
    }

    /**
     * @description 检验具体实现方法
     * @param arg0
     *            当前的实体类
     * @param arg1
     *            错误的信息
     * @author
     * @version 2014-03-18
     */
    @Override
    public void validate(Object arg0, Errors arg1) {
        UserImpParam v = (UserImpParam) arg0;
        if (StrUtil.isEmpty(v.getEmployeeNumber())) {
            arg1.reject("employeeNumber", "员工工号为空");
        }
        if (v.getEmployeeNumber() != null && v.getEmployeeNumber().length() > 20) {
            arg1.reject("employeeNumber", "员工工号长度大于20");
        }

        if (StrUtil.isEmpty(v.getName())) {
            arg1.reject("name", "姓名为空");
        }
        if (v.getName() != null && v.getName().length() > 20) {
            arg1.reject("name", "姓名长度大于20");
        }

        if (StrUtil.isEmpty(v.getDeptName())) {
            arg1.reject("deptName", "部门为空");
        }
        if (ToolUtil.isEmpty(v.getDeptId())) {
            arg1.reject("deptId", "填写部门不存在");
        }

        if (StrUtil.isEmpty(v.getSex())) {
            arg1.reject("sex", "性别为空");
        }

        if (StrUtil.isEmpty(v.getOnJobState())) {
            arg1.reject("onJobState", "在岗状态为空");
        }
    }
}
