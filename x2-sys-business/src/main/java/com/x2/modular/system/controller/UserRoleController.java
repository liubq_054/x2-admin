package com.x2.modular.system.controller;

import com.x2.base.pojo.node.ZTreeNode;
import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.core.base.controller.BaseController;
import com.x2.core.exception.enums.BizExceptionEnum;
import com.x2.core.util.ToolUtil;
import com.x2.kernel.model.exception.ServiceException;
import com.x2.kernel.model.response.ResponseData;
import com.x2.kernel.model.response.SuccessResponseData;
import com.x2.modular.system.entity.UserRole;
import com.x2.modular.system.model.params.UserRoleParam;
import com.x2.modular.system.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * 用户和角色关联表控制器
 *
 * @author liuhq
 * @Date 2019-07-02 13:48:59
 */
@Controller
@RequestMapping("/userRole")
public class UserRoleController extends BaseController {

    private String PREFIX = "/system/userRole";

    @Autowired
    private UserRoleService userRoleService;

    /**
     * 跳转到主页面
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "/userRole.html";
    }

    /**
     * 新增页面
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    @RequestMapping("/add")
    public String add() {
        return PREFIX + "/userRole_add.html";
    }

    /**
     * 编辑页面
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    @RequestMapping("/edit")
    public String edit() {
        return PREFIX + "/userRole_edit.html";
    }

    /**
     * 新增接口
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    @RequestMapping("/addItem")
    @ResponseBody
    public ResponseData addItem(UserRoleParam userRoleParam) {
        this.userRoleService.add(userRoleParam);
        return ResponseData.success();
    }

    /**
     * 编辑接口
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    @RequestMapping("/editItem")
    @ResponseBody
    public ResponseData editItem(UserRoleParam userRoleParam) {
        this.userRoleService.update(userRoleParam);
        return ResponseData.success();
    }

    /**
     * 删除接口
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResponseData delete(UserRoleParam userRoleParam) {
        this.userRoleService.delete(userRoleParam);
        return ResponseData.success();
    }

    /**
     * 查看详情接口
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    @RequestMapping("/detail")
    @ResponseBody
    public ResponseData detail(UserRoleParam userRoleParam) {
        UserRole detail = this.userRoleService.getById(userRoleParam.getRelationId());
        return ResponseData.success(detail);
    }

    /**
     * 查询列表
     *
     * @author liuhq
     * @Date 2019-07-02
     */
    @ResponseBody
    @RequestMapping("/list")
    public LayuiPageInfo list(UserRoleParam userRoleParam) {
        return this.userRoleService.findPageBySpec(userRoleParam);
    }

    @RequestMapping(value = "/userRoleAuthority")
    @ResponseBody
    public ResponseData roleUserAssign(@RequestParam("roleId") Long roleId, @RequestParam("userIds") String userIds) {
        if (ToolUtil.isEmpty(roleId)) {
            throw new ServiceException(BizExceptionEnum.REQUEST_NULL);
        }
        this.userRoleService.setUserRoleAuthority(roleId, userIds);
        return SUCCESS_TIP;
    }

    @RequestMapping("/getUserRoleTree/{roleId}")
    @ResponseBody
    public SuccessResponseData getUserRoleTree(@PathVariable("roleId") Long roleId) {
//        this.userService.getSelectTreeData();
        return new SuccessResponseData(this.userRoleService.getSelectTreeData(roleId));
    }

    @RequestMapping("/getUserRoleTreeAllowLogin/{roleId}")
    @ResponseBody
    public SuccessResponseData getUserRoleTreeAllowLogin(@PathVariable("roleId") Long roleId) {
//        this.userService.getSelectTreeData();
        return new SuccessResponseData(this.userRoleService.getSelectTreeDataAllowLogin(roleId));
    }

    /**
     * 获取角色列表，通过用户id
     *
     * @author x2
     * @Date 2018/12/23 6:31 PM
     */
    @RequestMapping(value = "/roleZTreeListByUserId/{userId}")
    @ResponseBody
    public List<ZTreeNode> roleZTreeListByUserId(@PathVariable Long userId) {
        return this.userRoleService.roleZTreeListByUserId(userId);

    }
}


