package com.x2.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.x2.modular.system.entity.DictType;
import com.x2.modular.system.model.params.DictTypeParam;
import com.x2.modular.system.model.result.DictTypeResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 字典类型表 Mapper 接口
 *
 * @author stylefeng
 * @since 2019-03-13
 */
public interface DictTypeMapper extends BaseMapper<DictType> {
    /**
     * 列表查询
     *
     * @param paramCondition: 查询条件
     * @return 结果
     */
    List<DictTypeResult> customList(@Param("paramCondition") DictTypeParam paramCondition);
}
