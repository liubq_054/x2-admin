package com.x2.modular.system.model;

import com.x2.modular.system.entity.Dept;
import lombok.Data;

import java.io.Serializable;

/**
 * 字典信息
 *
 * @author x2
 * @Date 2018/12/8 18:16
 */
@Data
public class DeptDto extends Dept implements Serializable {
    /**
     * 父部门名称
     */
    private String pName;

    /**
     * 部门负责人
     */
    private String leaderName;
}
