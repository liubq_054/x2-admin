package com.x2.modular.system.model.params;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 附件业务关系
 * @author changpeng
 * @date 2021-02-03
 */
@Data
public class SysFileBusinessParam implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    private Long id;

    /**
     * 业务标识
     */
    private String busSign;

    /**
     * 业务id
     */
    private Long busId;

    /**
     * 附件id
     */
    private String fileId;

    /**
     * 删除标识，默认 0 逻辑删除 1
     */
    private Integer deleteFlag;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建用户
     */
    private Long createUser;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 更新用户
     */
    private Long updateUser;

    private String[] attachIds;

}
