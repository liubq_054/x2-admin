package com.x2.modular.system.model.params;

import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 用户和角色关联表
 * </p>
 *
 * @author liuhq
 * @since 2019-07-02
 */
@Data
public class UserRoleParam implements Serializable  {

    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    private Long relationId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 角色id
     */
    private Long roleId;

}
