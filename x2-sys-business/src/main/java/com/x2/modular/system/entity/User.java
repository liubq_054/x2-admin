package com.x2.modular.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.x2.core.constant.UserCardTypeEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户表
 *
 * @author liubq
 * @date 2021-01-08
 */
@Data
@TableName("sys_user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "user_id", type = IdType.ASSIGN_ID)
    private Long userId;
    /**
     * 主键id
     */
    @TableField(value = "user_type")
    private String userType;
    /**
     * 头像
     */
    @TableField("avatar")
    private String avatar;
    /**
     * 账号
     */
    @TableField("account")
    private String account;
    /**
     * 密码
     */
    @TableField("password")
    private String password;
    /**
     * md5密码盐
     */
    @TableField("salt")
    private String salt;
    /**
     * 名字
     */
    @TableField("name")
    private String name;

    //证件类型
    @TableField("card_type")
    private String cardType;
    //证件号
    @TableField("card_no")
    private String cardNo;
    /**
     * 电话
     */
    @TableField("phone")
    private String phone;
    /**
     * 部门id(多个逗号隔开)
     */
    @TableField("dept_id")
    private Long deptId;

    /**
     * 状态(字典)
     */
    @TableField("status")
    private String status;
    /**
     * 参与大厅审批
     */
    @TableField("dt")
    private String dt;

    /**
     * 乐观锁
     */
    @TableField("version")
    private Integer version;
    /**
     * 密码修改日期
     */
    @TableField("update_pwd_date")
    private Date updatePwdDate;
    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 创建人
     */
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private Long createUser;
    /**
     * 创建记录时用户所在部门
     */
    @TableField(value = "create_dept", fill = FieldFill.INSERT)
    private Long createDept;
    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;
    /**
     * 更新人
     */
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private Long updateUser;


    public String getCardTypeValue() {
        return UserCardTypeEnum.getByName(this.getCardType());
    }

    @Override
    public String toString() {
        return getName();
    }
}
