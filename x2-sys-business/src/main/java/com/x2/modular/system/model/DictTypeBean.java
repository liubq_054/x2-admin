package com.x2.modular.system.model;

import com.x2.modular.system.entity.DictType;
import lombok.Data;

/**
 * 字典分类bean
 *
 * @author stylefeng
 * @date 2017/5/5 22:40
 */
@Data
public class DictTypeBean extends DictType {

}
