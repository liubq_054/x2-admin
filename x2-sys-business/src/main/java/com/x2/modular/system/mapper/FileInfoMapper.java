package com.x2.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.x2.modular.system.entity.FileInfo;

/**
 * 文件信息表 Mapper 接口
 * @author stylefeng
 * @since 2018-12-07
 */
public interface FileInfoMapper extends BaseMapper<FileInfo> {

}
