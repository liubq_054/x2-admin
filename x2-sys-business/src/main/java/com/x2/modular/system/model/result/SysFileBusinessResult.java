package com.x2.modular.system.model.result;

import com.x2.base.auth.context.LoginContextHolder;
import com.x2.base.auth.model.LoginUser;
import com.x2.core.constant.factory.ConstantFactory;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 附件业务关系
 * @author changpeng
 * @date 2021-02-03
 */
@Data
public class SysFileBusinessResult implements Serializable {

    private static final long serialVersionUID = 1L;

    /**主键*/
    private Long id;
    /**业务标识*/
    private String busSign;
    /**业务id*/
    private Long busId;
    /**附件id*/
    private String fileId;
    /**删除标识，默认 0 逻辑删除 1*/
    private Integer deleteFlag;
    /**创建时间*/
    private Date createTime;
    /**创建用户*/
    private Long createUser;
    /**更新时间*/
    private Date updateTime;
    /**更新用户*/
    private Long updateUser;
    /**文件名*/
    private String fileName;
    /**文件类型*/
    private String fileSuffix;
    /**文件路径*/
    private String filePath;
    /**文件大小*/
    private Long fileSize;

    /**
     * 创建用户名称
     *
     * @return
     */
    public String getCreateUserName(){
        return null;
    }
    /**
     * 创建上传成功的结果
     * @param fileId：文件名
     * @param fileName：附件名
     * @param fileSize：附件大小
     * @param fileSuffix: 文件类型
     * @return 对象
     */
    public static SysFileBusinessResult createUploadResult(String fileId, String fileName, Long fileSize, String fileSuffix) {
        SysFileBusinessResult result = new SysFileBusinessResult();
        result.setFileId(fileId);
        result.setFileName(fileName);
        result.setFileSize(fileSize);
        result.setFileSuffix(fileSuffix);
        result.setCreateTime(new Date());
        LoginUser currentUser  = LoginContextHolder.getContext().getUser();
        if(currentUser!=null){
            result.setCreateUser(currentUser.getId());
        }
        result.setFileSuffix(fileSuffix);
        return result;
    }
}
