package com.x2.modular.system.tag;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.x2.core.constant.factory.ConstantFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class SysDicSelectTag extends SysDicBaseTag {

    @Override
    public void render() {
        initAttr();
        StringBuilder sb = new StringBuilder();
        if (StringUtils.checkValNotNull(dicCode)) {
            sb.append("<select id='" + this.getId() + "' ");
            if (this.getName() != null && this.getName().trim().length() > 0) {
                sb.append(" name='" + this.getName().trim() + "' ");
            }
            if (this.getOnChange() != null) {
                sb.append(" lay-filter='").append(this.getOnChange() == null ? "" : this.getOnChange()).append("' ");
            }
            if (layVerify != null) {
                sb.append(" lay-verify='").append(layVerify == null ? "" : layVerify).append("' ");
            }
            if (workFlowForm != null) {
                sb.append(" workFlowForm='").append(workFlowForm == null ? "" : workFlowForm).append("' ");
            }
            if (itemName != null) {
                sb.append(" itemName='").append(itemName == null ? "" : itemName).append("' ");
            }
//                    +" >");
            sb.append(" >");

            if (headName != null) {
                sb.append("<option value='").append(headValue == null ? "" : headValue).append("' selected>")
                        .append(headName).append("</option>");
            }

            if (headType != null) {
                if ("1".equals(headType)) {
                    sb.append("<option value='").append(headValue == null ? "" : headValue).append("' selected>")
                            .append(" - 全部 - ").append("</option>");
                }
                if ("2".equals(headType)) {
                    sb.append("<option value='").append(headValue == null ? "" : headValue).append("' selected>")
                            .append(" - 请选择 - ").append("</option>");
                }
                if ("3".equals(headType)) {
                    sb.append("<option value='").append(headValue == null ? "" : headValue).append("' selected>")
                            .append(" - 无 - ").append("</option>");
                }
            }

            List<Map<String, Object>> lst = ConstantFactory.me().getDictListByPCode(dicCode);
            String paramValue = this.getDefaultValue();
            int index = 0;
            String exclude = this.getExclude();
            if (exclude == null) {
                exclude = "";
            }
            String code;
            String name;
            for (Map<String, Object> option : lst) {
                code = (String) option.get("code");
                name = (String) option.get("name");
                if (exclude.indexOf(code) >= 0) {
                    continue;
                }
                sb.append("<option value='").append(code).append("'");
                if (paramValue != null) {
                    if (paramValue.equals(code)) {
                        sb.append(" selected ");
                    }
                } else {
                    if (index == 0 && headName == null && headType == null) {
                        sb.append(" selected ");
                    }
                }
                sb.append(">").append(name).append("</option>");
                index++;
                    /*this.binds("<a href='www.baidu.com' />");
                    this.doBodyRender();*/
            }
            sb.append("</select>");

        }

        try {
            this.ctx.byteWriter.writeString(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*String dicCode = this.getAttributeValue("code")==null?"0":this.getAttributeValue("code").toString();
        DictParam dictParam = new DictParam();
        dictParam.setDictTypeId(Long.parseLong(dicCode));
        List<Dict> lst= dictService.findPageBySpec(dictParam).getData();

        for (Dict channel : lst) {
            this.binds("<a href='www.baidu.com' />");
//            this.binds(channel);
            this.doBodyRender();
        }*/
    }
}
