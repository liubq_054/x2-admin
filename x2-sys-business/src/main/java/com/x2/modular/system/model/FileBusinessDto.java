package com.x2.modular.system.model;

import lombok.Data;

/**
 * @author 常鹏
 * @version 1.0
 * @date 2021/2/5 15:21
 */
@Data
public class FileBusinessDto {
    private String fileIds;
    private String deleteFileIds;
    private String busSign;
}
