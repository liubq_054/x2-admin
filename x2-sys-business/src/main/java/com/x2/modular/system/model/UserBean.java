package com.x2.modular.system.model;

import com.x2.modular.system.entity.User;
import lombok.Data;

@Data
public class UserBean extends User {
    private String deptName;
    private String sql;
}
