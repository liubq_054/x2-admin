package com.x2.modular.system.controller;

import com.x2.core.base.controller.BaseController;
import com.x2.base.auth.context.LoginContextHolder;
import com.x2.base.auth.model.LoginUser;
import com.x2.base.pojo.page.LayuiPageFactory;
import com.x2.kernel.model.response.ResponseData;
import com.x2.modular.system.model.UserBean;
import com.x2.modular.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import jakarta.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

/**
 * 系统管理员控制器
 *
 * @author x2
 * @Date 2017年1月11日 下午1:08:17
 */
@Controller
@RequestMapping("/sys_common")
public class UserSelectController extends BaseController {

    private static String PREFIX = "/system/common/";

    @Autowired
    private UserService userService;

    /**
     * 跳转到查看管理员列表的页面
     *
     * @author liuhq
     * @Date 2020/02/09 22:43
     */
    @RequestMapping("/user_select")
    public String userSelect(String apply, Model model,HttpServletRequest request) {
        model.addAttribute("apply", apply);
        model.addAttribute("deptScope", request.getParameter("deptScope"));
        return PREFIX + "user_select.html";
    }

    /**
     * 跳转到查看管理员列表的页面
     *
     * @author liuhq
     * @Date 2020/02/09 22:43
     */
    @RequestMapping("/user_select_radio")
    public String userSelectRadio(String apply, Model model,HttpServletRequest request) {
        model.addAttribute("apply", apply);
        model.addAttribute("deptScope", request.getParameter("deptScope"));
        return PREFIX + "user_select_radio.html";
    }



    /**
     * 查询管理员列表
     *
     * @author liuhq
     * @Date 2020/02/09 22:43
     */
    @RequestMapping("/getNowUser")
    @ResponseBody
    public ResponseData getNowUser(HttpServletRequest request) {
        String userIdStr = request.getParameter("userId");
        Long userId;
        if(userIdStr == null||userIdStr.trim().length()<=0){
            LoginUser loginUser = LoginContextHolder.getContext().getUser();
            userId = loginUser.getId();
        } else {
            userId = Long.valueOf(userIdStr);
        }
        return ResponseData.success(userService.getById(userId));
    }

    /**
     * 用户选择
     * @param UserBean：查询条件
     * @return 结果
     */
    @RequestMapping("/chooseList")
    @ResponseBody
    public List<Map<String, Object>> chooseList(UserBean UserBean) {
        return this.userService.selectChooseList(UserBean);
    }
}
