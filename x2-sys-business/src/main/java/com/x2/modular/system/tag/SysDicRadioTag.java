package com.x2.modular.system.tag;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.x2.core.constant.factory.ConstantFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class SysDicRadioTag extends SysDicBaseTag {
    @Override
    public void render() {
        initAttr();
        StringBuilder sb = new StringBuilder();
        if (StringUtils.checkValNotNull(dicCode)) {
            List<Map<String, Object>> lst = ConstantFactory.me().getDictListByPCode(dicCode);

            String paramValue = this.getDefaultValue();
            int index = 0;
            String code;
            String name;
            for (Map<String, Object> option : lst) {
                code = (String) option.get("code");
                name = (String) option.get("name");
                String id = this.getType() + "_" + this.getName() + "_" + code;
                sb.append(" <input type='radio' id='").append(id).append("'");
                if (this.getName() != null && this.getName().trim().length() > 0) {
                    sb.append(" name='" + this.getName().trim() + "' ");
                }
                sb.append(" title='").append(name).append("'");
                sb.append(" value='").append(code).append("'");
                if (this.getSkin() != null) {
                    sb.append(" lay-skin='").append(this.getSkin()).append("'");
                }
                if (this.getOnChange() != null) {
                    sb.append(" lay-filter='").append(this.getOnChange()).append("'");
                }

                if (this.getLayVerify() != null) {
                    sb.append(" lay-verify='").append(this.getLayVerify()).append("'");
                }
                if (index == 0) {
                    sb.append(" checked ");
                }
                if (paramValue != null && paramValue.equals(code)) {
                    sb.append(" checked ");
                }

                sb.append(" /> ");
                index++;
            }
        }

        try {
            this.ctx.byteWriter.writeString(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
