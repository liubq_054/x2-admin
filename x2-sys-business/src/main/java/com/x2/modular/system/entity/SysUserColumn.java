package com.x2.modular.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * 用户配置的每个列表显示的列
 * @author changpeng
 * @since 2021-01-29
 */
@TableName("sys_user_column")
public class SysUserColumn implements Serializable {

    private static final long serialVersionUID=1L;

    /**主键*/
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    /**用户id*/
    @TableField("user_id")
    private Long userId;
    /**列表地址*/
    @TableField("url")
    private String url;
    /**显示列的字符串*/
    @TableField("content")
    private String content;
    @TableField("delete_flag")
    private Integer deleteFlag;
    /**创建时间*/
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;
    /**创建用户*/
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private Long createUser;
    /**更新时间*/
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;
    /**更新用户*/
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private Long updateUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    @Override
    public String toString() {
        return "SysUserColumn{" +
        "id=" + id +
        ", userId=" + userId +
        ", url=" + url +
        ", content=" + content +
        ", deleteFlag=" + deleteFlag +
        ", createTime=" + createTime +
        ", createUser=" + createUser +
        ", updateTime=" + updateTime +
        ", updateUser=" + updateUser +
        "}";
    }
}
