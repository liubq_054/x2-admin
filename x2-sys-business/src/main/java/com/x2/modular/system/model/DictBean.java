package com.x2.modular.system.model;

import com.x2.modular.system.entity.Dict;
import lombok.Data;

/**
 * 字典Bean
 * @author stylefeng
 * @date 2017/5/5 22:40
 */
@Data
public class DictBean extends Dict {

}
