package com.x2.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.x2.modular.system.entity.User;
import com.x2.modular.system.model.UserBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 管理员表 Mapper 接口
 *
 * @author changpeng
 * @date 2021-01-08
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 修改用户状态
     *
     * @param userId：用户
     * @param status：状态
     * @return 结果
     */
    int setStatus(@Param("userId") Long userId, @Param("status") String status);

    /**
     * 修改密码
     *
     * @param userId：用户
     * @param pwd：密码
     * @return 结果
     */
    int changePwd(@Param("userId") Long userId, @Param("pwd") String pwd);

    /**
     * 设置用户的角色
     *
     * @param userId：用户id
     * @param roleIds：角色id
     * @return 结果
     */
    int setRoles(@Param("userId") Long userId, @Param("roleIds") String roleIds);

    /**
     * 用户查询
     *
     * @param UserBean：调教
     * @return 结果
     */
    List<UserBean> query(@Param("paramCondition") UserBean UserBean);

    /**
     * 用户分页查询
     *
     * @param page：分页参数
     * @param UserBean：条件
     * @return 结果
     */
    Page<UserBean> query(@Param("page") Page page, @Param("paramCondition") UserBean UserBean);

    /**
     * 用户查询，返回map
     *
     * @param UserBean: 条件
     * @return 结果
     */
    List<Map<String, Object>> queryMap(@Param("paramCondition") UserBean UserBean);

    /**
     * 用户查询
     *
     * @param page：分页参数
     * @param UserBean：查询参数
     * @return 结果
     */
    Page<Map<String, Object>> queryMap(@Param("page") Page page, @Param("paramCondition") UserBean UserBean);

    /**
     * 通过账号获取用户
     *
     * @param account： 账号
     * @return 结果
     */
    User getByAccount(@Param("account") String account);

    /**
     * 用户查询
     *
     * @param employeeNumber：编号
     * @return 结果
     */
    List<User> getByEmployeeNumber(@Param("employeeNumber") String employeeNumber);

    /**
     * 选择办理人
     *
     * @param page： 分页参数
     * @return 结果
     */
    IPage<Map<String, Object>> listUserAndRoleExpectAdmin(Page page);


}
