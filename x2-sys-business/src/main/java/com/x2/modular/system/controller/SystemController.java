/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.x2.modular.system.controller;

import cn.hutool.core.bean.BeanUtil;
import com.x2.base.auth.context.LoginContextHolder;
import com.x2.base.auth.model.LoginUser;
import com.x2.base.oshi.SystemHardwareInfo;
import com.x2.core.base.controller.BaseController;
import com.x2.core.constant.DefaultImages;
import com.x2.core.constant.factory.ConstantFactory;
import com.x2.core.util.ToolUtil;
import com.x2.kernel.model.exception.RequestEmptyException;
import com.x2.modular.system.entity.Dept;
import com.x2.modular.system.entity.Notice;
import com.x2.modular.system.entity.User;
import com.x2.modular.system.service.NoticeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * 通用控制器
 *
 * @author x2
 * @Date 2017年2月17日20:27:22
 */
@Controller
@RequestMapping("/system")
@Slf4j
public class SystemController extends BaseController {


    @Autowired
    private NoticeService noticeService;

    /**
     * 控制台页面
     *
     * @author x2
     * @Date 2018/12/24 22:43
     */
    @RequestMapping("/console")
    public String console() {
        return "/system/frame/console.html";
    }

    /**
     * 分析页面
     *
     * @author x2
     * @Date 2018/12/24 22:43
     */
    @RequestMapping("/console2")
    public String console2() {
        return "/system/frame/console2.html";
    }

    /**
     * 跳转到查看用户详情页面
     *
     * @param model: 参数
     * @return 结果
     */
    @RequestMapping("/user_info")
    public String userInfo(Model model) {
        Long userId = LoginContextHolder.getContext().getUserId();
        User user = ConstantFactory.me().getUserById(userId);
        model.addAllAttributes(BeanUtil.beanToMap(user));
        model.addAttribute("roleName", ConstantFactory.me().getRoleNameByUserId(user.getUserId()));
        String deptName = ConstantFactory.me().getDeptName(user.getDeptId());
        model.addAttribute("deptName", deptName);
        model.addAttribute("avatar", DefaultImages.defaultAvatarUrl());
        return "/system/frame/user_info.html";
    }

    /**
     * 系统硬件信息页面
     *
     * @author x2
     * @Date 2018/12/24 22:43
     */
    @RequestMapping("/systemInfo")
    public String systemInfo(Model model) {

        SystemHardwareInfo systemHardwareInfo = new SystemHardwareInfo();
        systemHardwareInfo.copyTo();

        model.addAttribute("server", systemHardwareInfo);

        return "/system/frame/systemInfo.html";
    }

    /**
     * 系统硬件信息页面
     *
     * @author x2
     * @Date 2018/12/24 22:43
     */
    @RequestMapping("/currentUserInfo")
    @ResponseBody
    public User currentUserInfo() {
        LoginUser loginUser = LoginContextHolder.getContext().getUser();
        return ConstantFactory.me().getUserById(loginUser.getId());
    }


    /**
     * 跳转到首页通知
     *
     * @author x2
     * @Date 2018/12/23 6:06 PM
     */
    @RequestMapping("/notice")
    public String hello() {
        List<Notice> notices = noticeService.list();
        super.setAttr("noticeList", notices);
        return "/system/frame/notice.html";
    }

    /**
     * 主页面
     *
     * @author x2
     * @Date 2019/1/24 3:38 PM
     */
    @RequestMapping("/welcome")
    public String welcome() {
        return "/system/frame/welcome.html";
    }

    /**
     * 主题页面
     *
     * @author x2
     * @Date 2019/1/24 3:38 PM
     */
    @RequestMapping("/theme")
    public String theme() {
        return "/system/frame/theme.html";
    }


    /**
     * 个人消息列表
     *
     * @author x2
     * @Date 2018/12/24 22:43
     */
    @RequestMapping("/message")
    public String message() {
        return "/system/frame/message.html";
    }


    /**
     * 通用的树列表选择器
     *
     * @author x2
     * @Date 2018/12/23 6:59 PM
     */
    @RequestMapping("/commonTree")
    public String deptTreeList(@RequestParam("formName") String formName, @RequestParam("formId") String formId,
                               @RequestParam("treeUrl") String treeUrl, @RequestParam(value = "cache", required = false) String cache,
                               Model model) {
        if (ToolUtil.isOneEmpty(formName, formId, treeUrl)) {
            throw new RequestEmptyException("请求数据不完整！");
        }
        try {
            cache = ToolUtil.isEmpty(cache) || !cache.equals("Y") ? "N" : "Y";
            model.addAttribute("formName", URLDecoder.decode(formName, "UTF-8"));
            model.addAttribute("formId", URLDecoder.decode(formId, "UTF-8"));
            model.addAttribute("treeUrl", URLDecoder.decode(treeUrl, "UTF-8"));
            model.addAttribute("cache", cache);
        } catch (UnsupportedEncodingException e) {
            throw new RequestEmptyException("请求数据不完整！");
        }

        return "/common/tree_dlg.html";
    }

    /**
     * 更新头像
     *
     * @author x2
     * @Date 2018/11/9 12:45 PM
     */
    @RequestMapping("/updateAvatar")
    @ResponseBody
    public Object uploadAvatar(@RequestParam("fileId") String fileId) {
        if (ToolUtil.isEmpty(fileId)) {
            throw new RequestEmptyException("请求头像为空");
        }
        return SUCCESS_TIP;
    }

    /**
     * 跳转到修改密码界面
     *
     * @author x2
     * @Date 2018/12/24 22:43
     */
    @RequestMapping("/user_chpwd")
    public String chPwd() {
        return "/system/frame/password.html";
    }


}
