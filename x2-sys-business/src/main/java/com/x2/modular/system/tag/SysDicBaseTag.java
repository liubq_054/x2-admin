package com.x2.modular.system.tag;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.x2.core.util.SpringContextHolder;
import com.x2.modular.system.service.DictService;
import com.x2.modular.system.service.DictTypeService;
import lombok.Data;
import org.beetl.core.tag.GeneralVarTagBinding;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Data
public class SysDicBaseTag extends GeneralVarTagBinding {
    @Autowired
    public DictService dictService = SpringContextHolder.getBean(DictService.class);
    public DictTypeService dictTypeService = SpringContextHolder.getBean(DictTypeService.class);
    public String id;
    public String name;
    public String dicCode;
    public String type;
    public String skin;
    public String onClick;
    public String onChange;
    public String headName;
    public String headValue;
    public String headType;
    public String defaultValue;
    public String disabled;
    public String layVerify;
    public String workFlowForm;
    public String itemName;
    public String exclude;
    public void initAttr(){
        Map<String, Object> attrs = this.getAttributes();
        if(attrs.size()>0){
            if(StringUtils.checkValNotNull(attrs.get("id"))){
                this.setId(attrs.get("id").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("name"))){
                this.setName(attrs.get("name").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("dicCode"))){
                this.setDicCode(attrs.get("dicCode").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("type"))){
                this.setType(attrs.get("type").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("skin"))){
                this.setSkin(attrs.get("skin").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("onClick"))){
                this.setOnClick(attrs.get("onClick").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("onChange"))){
                this.setOnChange(attrs.get("onChange").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("headName"))){
                this.setHeadName(attrs.get("headName").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("headValue"))){
                this.setHeadValue(attrs.get("headValue").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("headType"))){
                this.setHeadType(attrs.get("headType").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("defaultValue"))){
                this.setDefaultValue(attrs.get("defaultValue").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("disabled"))){
                this.setDisabled(attrs.get("disabled").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("layVerify"))){
                this.setLayVerify(attrs.get("layVerify").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("workFlowForm"))){
                this.setWorkFlowForm(attrs.get("workFlowForm").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("itemName"))){
                this.setItemName(attrs.get("itemName").toString());
            }
            if(StringUtils.checkValNotNull(attrs.get("exclude"))){
                this.setExclude(attrs.get("exclude").toString());
            }
        }
    }

    @Override
    public void render() {

    }
}
