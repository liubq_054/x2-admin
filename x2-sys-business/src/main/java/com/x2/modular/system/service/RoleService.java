package com.x2.modular.system.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.x2.base.pojo.node.ZTreeNode;
import com.x2.base.pojo.page.LayuiPageFactory;
import com.x2.core.constant.Const;
import com.x2.core.exception.enums.BizExceptionEnum;
import com.x2.core.util.BeanContextHolder;
import com.x2.core.util.ToolUtil;
import com.x2.kernel.model.exception.RequestEmptyException;
import com.x2.kernel.model.exception.ServiceException;
import com.x2.kernel.model.response.ResponseData;
import com.x2.modular.system.entity.Relation;
import com.x2.modular.system.entity.Role;
import com.x2.modular.system.entity.UserRole;
import com.x2.modular.system.mapper.RelationMapper;
import com.x2.modular.system.mapper.RoleMapper;
import com.x2.modular.system.model.RoleDto;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2018-12-07
 */
@Service
public class RoleService extends ServiceImpl<RoleMapper, Role> {

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private RelationMapper relationMapper;

    @Resource
    private RelationService relationService;

    @Resource
    private UserService userService;

    /**
     * 添加角色
     *
     * @author x2
     * @Date 2018/12/23 6:40 PM
     */
    @Transactional(rollbackFor = Exception.class)
    public void addRole(Role role) {
        if (ToolUtil.isOneEmpty(role, role.getName(), role.getPid(), role.getDescription())) {
            throw new RequestEmptyException();
        }
        role.setRoleId(null);
        this.save(role);
    }

    /**
     * 编辑角色
     *
     * @author x2
     * @Date 2018/12/23 6:40 PM
     */
    @Transactional(rollbackFor = Exception.class)
    public void editRole(RoleDto roleDto) {
        if (ToolUtil.isOneEmpty(roleDto, roleDto.getName(), roleDto.getPid(), roleDto.getDescription())) {
            throw new RequestEmptyException();
        }
        Role old = this.getById(roleDto.getRoleId());
        String oldName = old.getName();
        BeanUtil.copyProperties(roleDto, old);
        this.updateById(old);
        String desc;
        if (old.getName().equals(oldName)) {
            desc = "修改角色，" + old.getName();
        } else {
            desc = "修改角色，新名称：" + old.getName() + ",旧名称：" + oldName;
        }
    }

    /**
     * 设置某个角色的权限
     *
     * @param roleId 角色id
     * @param ids    权限的id
     * @date 2017年2月13日 下午8:26:53
     */
    @Transactional(rollbackFor = Exception.class, timeout = 60)
    public void setAuthority(Long roleId, String ids) {

        // 删除该角色所有的权限
        this.roleMapper.deleteRolesById(roleId);

        Relation relation;
        List<Relation> relationList = new ArrayList<>();
        for (Long id : Convert.toLongArray(ids.split(","))) {
            relation = new Relation();
            relation.setRoleId(roleId);
            relation.setMenuId(id);
            relationList.add(relation);
        }
        this.relationService.saveBatch(relationList);

      /*  // 添加新的权限
        for (Long id : Convert.toLongArray(ids.split(","))) {
            relation = new Relation();
            relation.setRoleId(roleId);
            relation.setMenuId(id);
            this.relationMapper.insert(relation);
        }*/

        // 刷新当前用户的权限
        userService.refreshCurrentUser();

    }

    /**
     * 删除角色
     *
     * @author stylefeng
     * @Date 2017/5/5 22:24
     */
    @Transactional(rollbackFor = Exception.class)
    public ResponseData delRoleById(Long roleId) {
        if (ToolUtil.isEmpty(roleId)) {
            throw new ServiceException(BizExceptionEnum.REQUEST_NULL);
        }
        //不能删除超级管理员角色
        if (roleId.equals(Const.ADMIN_ROLE_ID)) {
            throw new ServiceException(BizExceptionEnum.CANT_DELETE_ADMIN);
        }
        UserRoleService userRoleService = BeanContextHolder.getBean(UserRoleService.class);
        // 验证角色下是否有用户，没有才可以删除
        QueryWrapper<UserRole> query = new QueryWrapper<>();
        query.select("role_id");
        query.eq("role_id", roleId);
        query.isNotNull("user_id");
        Long checkDelete = userRoleService.count(query);
        if (checkDelete > 0) {
            return ResponseData.error("该角色下存在用户，无法删除");
        }
        //删除角色
        this.roleMapper.deleteById(roleId);
        //删除该角色所有的权限
        this.roleMapper.deleteRolesById(roleId);
        //删除角色人员关联
        userRoleService.deleteByRoleId(roleId);

        return ResponseData.success();
    }

    /**
     * 根据条件查询角色列表
     *
     * @return
     * @date 2017年2月12日 下午9:14:34
     */
    public Page<Map<String, Object>> selectRoles(String condition) {
        Page page = LayuiPageFactory.defaultPage();
        return this.baseMapper.selectRoles(page, condition);
    }

    /**
     * 删除某个角色的所有权限
     *
     * @param roleId 角色id
     * @return
     * @date 2017年2月13日 下午7:57:51
     */
    public int deleteRolesById(Long roleId) {
        Role role = this.getById(roleId);
        int res = this.baseMapper.deleteRolesById(roleId);
        return res;
    }

    /**
     * 获取角色列表树
     *
     * @return
     * @date 2017年2月18日 上午10:32:04
     */
    public List<ZTreeNode> roleTreeList() {
        return this.baseMapper.roleTreeList();
    }

    /**
     * 获取角色列表树
     *
     * @return
     * @date 2017年2月18日 上午10:32:04
     */
    public List<ZTreeNode> roleTreeListByRoleId(Long[] roleId) {
        return this.baseMapper.roleTreeListByRoleId(roleId);
    }

    /**
     * 获取角色列表
     *
     * @author x2
     * @Date 2019-08-30 15:35
     */
    public IPage listRole(String name) {
        Page pageContext = LayuiPageFactory.defaultPage();
        return baseMapper.listRole(pageContext, name);
    }

    /**
     * 根据id列表获取第三方id字符串
     *
     * @param roleIds
     * @return
     */
    public String getPluginsIdsByIds(String roleIds) {
        QueryWrapper<Role> query = new QueryWrapper<>();
        query.select("role_id AS roleId , plugins_id AS pluginsId");
        query.in("role_id", roleIds);
        List<Role> roleList = this.list(query);
        String pluginsIds = "";
        if (roleList != null) {
            for (Role role : roleList) {
                if (role.getPluginsId() != null) {
                    pluginsIds += "," + role.getPluginsId();
                }
            }
        }
        if (pluginsIds.trim().length() > 0) {
            return pluginsIds.substring(1);
        }
        return null;
    }

    /**
     * 根据用户id获取用户包含角色的第三方id字符串
     *
     * @param userId
     * @return
     */
    public String getPluginsIdsByUserId(Long userId) {
        UserRoleService userRoleService = BeanContextHolder.getBean(UserRoleService.class);
        QueryWrapper<UserRole> query = new QueryWrapper<>();
        query.select("role_id AS roleId");
        query.eq("user_id", userId);
        List<UserRole> userRoleList = userRoleService.list(query);
        String roleIds = "";
        if (userRoleList != null) {
            for (UserRole userRole : userRoleList) {
                roleIds += "," + userRole.getRoleId();
            }
        }
        if (roleIds.trim().length() > 0) {
            return getPluginsIdsByIds(roleIds.substring(1));
        }
        return null;
    }
}
