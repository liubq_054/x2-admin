package com.x2.modular.system.tag;

import com.x2.core.util.SpringContextHolder;
import com.x2.modular.system.entity.Dict;
import com.x2.modular.system.model.params.DictParam;
import com.x2.modular.system.service.DictService;
import org.beetl.core.tag.GeneralVarTagBinding;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SysDicSelectOptionTag extends GeneralVarTagBinding {
    @Autowired
    private DictService dictService = SpringContextHolder.getBean(DictService.class);

    @Override
    public void render() {
        /*String dicCode = Strings.sNull(this.getAttributeValue("code"));
        Sys_dict dict;
        if (Strings.isNotBlank(dicCode)) {
            dict = sysDictService.fetch(Cnd.where("code", "=", dicCode));
        }else{
            return;
        }

        String parentId= dict.getId();

        List<Sys_dict> lst= sysDictService.query(Cnd.where("parentId","=",parentId).asc("location"));*/
        String dicCode = this.getAttributeValue("code")==null?"0":this.getAttributeValue("code").toString();
        DictParam dictParam = new DictParam();
        dictParam.setDictTypeId(Long.parseLong(dicCode));
        List<Dict> lst= dictService.findPageBySpec(dictParam).getData();

        for (Dict channel : lst) {
            this.binds("<a href='www.baidu.com' />");
//            this.binds(channel);
            this.doBodyRender();
        }
    }
}
