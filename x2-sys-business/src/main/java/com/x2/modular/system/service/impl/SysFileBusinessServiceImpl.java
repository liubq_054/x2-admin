package com.x2.modular.system.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.x2.base.pojo.page.LayuiPageFactory;
import com.x2.core.util.ToolUtil;
import com.x2.kernel.model.response.ResponseData;
import com.x2.modular.system.entity.SysFileBusiness;
import com.x2.modular.system.mapper.SysFileBusinessMapper;
import com.x2.modular.system.model.FileBusinessDto;
import com.x2.modular.system.model.params.SysFileBusinessParam;
import com.x2.modular.system.model.result.SysFileBusinessResult;
import com.x2.modular.system.service.SysFileBusinessService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 附件业务关系 服务实现类
 *
 * @author changpeng
 * @date 2021-02-03
 */
@Service
public class SysFileBusinessServiceImpl extends ServiceImpl<SysFileBusinessMapper, SysFileBusiness> implements SysFileBusinessService {

    @Override
    public ResponseData save(String busSign, Long busId, String fileIds, String deleteFileIds) {
        if (!ToolUtil.isEmpty(deleteFileIds)) {
            SysFileBusinessParam param = new SysFileBusinessParam();
            param.setBusId(busId);
            param.setBusSign(busSign);
            param.setAttachIds(ToolUtil.splitStr(deleteFileIds, ','));
            List<SysFileBusinessResult> resultList = this.baseMapper.customList(param);
            if (resultList != null) {
                for (SysFileBusinessResult result : resultList) {
                    this.delete(result.getId());
                }
            }
        }
        if (!ToolUtil.isEmpty(fileIds)) {
            SysFileBusinessParam param = new SysFileBusinessParam();
            param.setBusId(busId);
            param.setBusSign(busSign);
            param.setAttachIds(ToolUtil.splitStr(fileIds, ','));
            fileIds = "," + fileIds + ",";
            List<SysFileBusinessResult> resultList = this.baseMapper.customList(param);
            if (resultList != null) {
                //排除已经存在的
                for (SysFileBusinessResult result : resultList) {
                    fileIds = ToolUtil.replaceStr(fileIds, "," + result.getFileId() + ",", ",");
                }
            }
            String[] array = ToolUtil.splitStr(fileIds, ',');
            if (array != null) {
                for (String ids : array) {
                    if (ids.trim().length() == 0) {
                        continue;
                    }
                    SysFileBusiness entity = new SysFileBusiness();
                    entity.setBusId(busId);
                    entity.setBusSign(busSign);
                    entity.setFileId(ids);
                    this.save(entity);
                }
            }
        }
        return ResponseData.success();
    }

    @Override
    public ResponseData saveAttach(String attachFile, Long busId) {
        if (ToolUtil.isEmpty(attachFile)) {
            return ResponseData.error("附件不存在");
        }
        if (busId == null) {
            return ResponseData.error("业务id异常");
        }
        try {
            List<FileBusinessDto> attachList = JSONUtil.toList(JSONUtil.parseArray(attachFile), FileBusinessDto.class);
            if (attachList != null) {
                for (FileBusinessDto dto : attachList) {
                    save(dto.getBusSign(), busId, dto.getFileIds(), dto.getDeleteFileIds());
                }
            }
        } catch (Exception ex) {
            return ResponseData.error(ex.getMessage());
        }
        return ResponseData.success();
    }

    @Override
    public ResponseData delete(Long attachId) {
        this.removeById(attachId);
        return ResponseData.success();
    }

    @Override
    public List<SysFileBusinessResult> getAttach(String busSign, Long busId) {
        if (ToolUtil.isEmpty(busSign) && busId != null) {
            return Collections.emptyList();
        }
        SysFileBusinessParam param = new SysFileBusinessParam();
        param.setBusId(busId);
        param.setBusSign(busSign);
        return this.baseMapper.customList(param);
    }

    @Override
    public SysFileBusinessResult getAttach(String busSign, Long busId, String fileId) {
        SysFileBusinessParam param = new SysFileBusinessParam();
        param.setBusId(busId);
        param.setBusSign(busSign);
        param.setFileId(fileId);
        List<SysFileBusinessResult> resultList = this.baseMapper.customList(param);
        if (resultList != null && resultList.size() > 0) {
            return resultList.get(0);
        }
        return null;
    }

    @Override
    public SysFileBusinessResult getAttach(Long id) {
        SysFileBusinessParam param = new SysFileBusinessParam();
        param.setId(id);
        List<SysFileBusinessResult> resultList = this.baseMapper.customList(param);
        if (resultList != null && resultList.size() > 0) {
            return resultList.get(0);
        }
        return null;
    }

    private Serializable getKey(SysFileBusinessParam param) {
        return param.getId();
    }

    private Page getPageContext() {
        return LayuiPageFactory.defaultPage();
    }

    private SysFileBusiness getOldEntity(SysFileBusinessParam param) {
        return this.getById(getKey(param));
    }

    private SysFileBusiness getEntity(SysFileBusinessParam param) {
        SysFileBusiness entity = new SysFileBusiness();
        ToolUtil.copyProperties(param, entity);
        return entity;
    }
}
