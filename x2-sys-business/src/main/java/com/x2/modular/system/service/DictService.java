package com.x2.modular.system.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.x2.base.enums.CommonStatus;
import com.x2.base.pojo.node.ZTreeNode;
import com.x2.base.pojo.page.LayuiPageFactory;
import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.core.constant.factory.ConstantFactory;
import com.x2.core.exception.enums.BizExceptionEnum;
import com.x2.core.util.BeanContextHolder;
import com.x2.core.util.ToolUtil;
import com.x2.kernel.model.exception.RequestEmptyException;
import com.x2.kernel.model.exception.ServiceException;
import com.x2.modular.system.entity.Dict;
import com.x2.modular.system.entity.DictType;
import com.x2.modular.system.mapper.DictMapper;
import com.x2.modular.system.model.params.DictParam;
import com.x2.modular.system.model.result.DictResult;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础字典 服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2019-03-13
 */
@Service
public class DictService extends ServiceImpl<DictMapper, Dict> {


    /**
     * 新增
     *
     * @author stylefeng
     * @Date 2019-03-13
     */
//    @CachePut(value = Cache.CONSTANT, key = "'"+ CacheKey.DIC_NAME+"'+#param.getDictTypeCode()+'_'+#param.getCode()")
    public void add(DictParam param) {

        //判断是否已经存在同编码或同名称字典
        QueryWrapper<Dict> dictQueryWrapper = new QueryWrapper<>();
        dictQueryWrapper
                .and(i -> i.eq("code", param.getCode()).or().eq("name", param.getName()))
                .and(i -> i.eq("dict_type_id", param.getDictTypeId()));
        List<Dict> list = this.list(dictQueryWrapper);
        if (list != null && list.size() > 0) {
            throw new ServiceException(BizExceptionEnum.DICT_EXISTED);
        }

        Dict entity = getEntity(param);

        //设置pids
        dictSetPids(entity);

        //设置状态
        entity.setStatus(CommonStatus.ENABLE.getCode());

        this.save(entity);
        //刷新字典
        ConstantFactory.me().refreshDictNameByPCode(param.getDictTypeCode(), param.getCode(), param.getName());
        ConstantFactory.me().refreshDictCodesByPCode(param.getDictTypeCode(), param.getCode(), param.getName());
        ConstantFactory.me().deleteDictListByPCode(param.getDictTypeCode());
    }

    /**
     * 删除
     *
     * @author stylefeng
     * @Date 2019-03-13
     */
//    @CacheEvict(value = Cache.CONSTANT, key = "'"+ CacheKey.DIC_NAME+"'+#param.dictTypeCode+'_'+#param.code")
    public void delete(DictParam param) {

        //删除字典的所有子级
        List<Long> subIds = getSubIds(param.getDictId());
        if (subIds.size() > 0) {
            for (Long id : subIds) {
                Dict detail = this.getById(id);
                if (detail != null) {
                    ConstantFactory.me().deleteDictsByPCode(param.getDictTypeCode(), detail.getCode(), detail.getName());
                }
            }
            this.removeByIds(subIds);
        }
        Dict detail = this.getById(getKey(param));
        if (detail != null) {
            ConstantFactory.me().deleteDictsByPCode(param.getDictTypeCode(), detail.getCode(), detail.getName());
        }
        this.removeById(getKey(param));
    }

    /**
     * 更新
     *
     * @author stylefeng
     * @Date 2019-03-13
     */
//    @CachePut(value = Cache.CONSTANT, key = "'"+ CacheKey.DIC_NAME+"'+#param.getDictTypeCode()+'_'+#param.getCode()")
    public void update(DictParam param) {
        Dict oldEntity = getOldEntity(param);
        Dict newEntity = getEntity(param);
        ToolUtil.copyProperties(newEntity, oldEntity);

        //判断编码是否重复
        QueryWrapper<Dict> wrapper = new QueryWrapper<Dict>()
                .and(i -> i.eq("code", newEntity.getCode()).or().eq("name", newEntity.getName()))
                .and(i -> i.ne("dict_id", newEntity.getDictId()))
                .and(i -> i.eq("dict_type_id", param.getDictTypeId()));
        Long dicts = this.count(wrapper);
        if (dicts > 0) {
            throw new ServiceException(BizExceptionEnum.DICT_EXISTED);
        }

        //设置pids
        dictSetPids(newEntity);

        this.updateById(newEntity);
        //刷新字典
        ConstantFactory.me().refreshDictNameByPCode(param.getDictTypeCode(), param.getCode(), param.getName());
        ConstantFactory.me().refreshDictCodesByPCode(param.getDictTypeCode(), param.getCode(), param.getName());
        ConstantFactory.me().deleteDictListByPCode(param.getDictTypeCode());
    }

    /**
     * 查询单条数据，Specification模式
     *
     * @author stylefeng
     * @Date 2019-03-13
     */
    public DictResult findBySpec(DictParam param) {
        List<DictResult> resultList = findListBySpec(param);
        return resultList != null && resultList.size() > 0 ? resultList.get(0) : null;
    }

    /**
     * 查询列表，Specification模式
     *
     * @author stylefeng
     * @Date 2019-03-13
     */
    public List<DictResult> findListBySpec(DictParam param) {
        return this.baseMapper.customList(param);
    }

    /**
     * 查询分页数据，Specification模式
     *
     * @author stylefeng
     * @Date 2019-03-13
     */
    public LayuiPageInfo findPageBySpec(DictParam param) {
        QueryWrapper<Dict> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("dict_type_id", param.getDictTypeId());

        if (ToolUtil.isNotEmpty(param.getCondition())) {
            objectQueryWrapper.and(i -> i.eq("code", param.getCondition()).or().eq("name", param.getCondition()));
        }

        objectQueryWrapper.orderByAsc("sort");

        List<Dict> list = this.list(objectQueryWrapper);

        //创建根节点
        Dict dict = new Dict();
        dict.setName("根节点");
        dict.setDictId(0L);
        dict.setParentId(-999L);
        list.add(dict);

        LayuiPageInfo result = new LayuiPageInfo();
        result.setData(list);

        return result;
    }

    /**
     * 获取字典的树形列表（ztree结构）
     *
     * @author x2
     * @Date 2019/3/14 3:40 PM
     */
    public List<ZTreeNode> dictTreeList(Long dictTypeId, Long dictId) {
        if (dictTypeId == null) {
            throw new RequestEmptyException();
        }

        List<ZTreeNode> tree = this.baseMapper.dictTree(dictTypeId);

        //获取dict的所有子节点
        List<Long> subIds = getSubIds(dictId);

        //如果传了dictId，则在返回结果里去掉
        List<ZTreeNode> resultTree = new ArrayList<>();
        for (ZTreeNode zTreeNode : tree) {

            //如果dictId等于树节点的某个id则去除
            if (ToolUtil.isNotEmpty(dictId) && dictId.equals(zTreeNode.getId())) {
                continue;
            }
            if (subIds.contains(zTreeNode.getId())) {
                continue;
            }
            resultTree.add(zTreeNode);
        }

        resultTree.add(ZTreeNode.createParent());

        return resultTree;
    }

    /**
     * 查看dict的详情
     *
     * @author x2
     * @Date 2019/3/14 5:22 PM
     */
    public DictResult dictDetail(Long dictId) {
        if (ToolUtil.isEmpty(dictId)) {
            throw new RequestEmptyException();
        }

        DictResult dictResult = new DictResult();

        //查询字典
        Dict detail = this.getById(dictId);
        if (detail == null) {
            throw new RequestEmptyException();
        }

        //查询父级字典
        if (ToolUtil.isNotEmpty(detail.getParentId())) {
            Long parentId = detail.getParentId();
            Dict dictType = this.getById(parentId);
            if (dictType != null) {
                dictResult.setParentName(dictType.getName());
            } else {
                dictResult.setParentName("无父级");
            }
        }

        ToolUtil.copyProperties(detail, dictResult);

        return dictResult;
    }

    /**
     * 查询字典列表，通过字典类型
     *
     * @author x2
     * @Date 2019-06-20 15:14
     */
    public List<Dict> listDicts(Long dictTypeId) {

        QueryWrapper<Dict> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("dict_type_id", dictTypeId);

        List<Dict> list = this.list(objectQueryWrapper);

        if (list == null) {
            return new ArrayList<>();
        } else {
            return list;
        }

    }

    /**
     * 查询字典列表，通过字典类型code
     *
     * @author x2
     * @Date 2019-06-20 15:14
     */
    public List<Dict> listDictsByCode(String dictTypeCode) {

        QueryWrapper<DictType> wrapper = new QueryWrapper<>();
        wrapper.eq("code", dictTypeCode);
        DictTypeService dictTypeService = BeanContextHolder.getBean(DictTypeService.class);
        DictType one = dictTypeService.getOne(wrapper);
        return listDicts(one.getDictTypeId());
    }

    /**
     * 查询字典列表，通过字典类型code
     *
     * @author x2
     * @Date 2019-06-20 15:14
     */
    public List<Map<String, String>> listByTypeCodes(List<String> typeCodeList) {
        return this.baseMapper.listByTypeCodes(typeCodeList);
    }


    /**
     * 查询字典列表，通过字典类型code
     *
     * @author x2
     * @Date 2019-06-20 15:14
     */
    public List<Map<String, Object>> getDictsByCodes(List<String> dictCodes) {

        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.in("code", dictCodes).orderByAsc("sort");

        ArrayList<Map<String, Object>> results = new ArrayList<>();

        //转成map
        List<Dict> list = this.list(wrapper);
        for (Dict dict : list) {
            Map<String, Object> map = BeanUtil.beanToMap(dict);
            results.add(map);
        }

        return results;
    }

    /**
     * 根据父编码获取 key：name， value：code的map
     *
     * @param parentCode: 父编码
     * @return 结果
     */
    public Map<String, Object> getDictNameToCodeMapByParentCode(String parentCode) {
        Map<String, Object> map = new HashMap<>(0);
        List<Map<String, Object>> dictList = ConstantFactory.me().getDictListByPCode(parentCode);
        if (dictList != null) {
            for (Map<String, Object> stringObjectMap : dictList) {
                map.put((String) stringObjectMap.get("name"), (String) stringObjectMap.get("code"));
            }
        }
        return map;
    }

    private Serializable getKey(DictParam param) {
        return param.getDictId();
    }

    private Page getPageContext() {
        return LayuiPageFactory.defaultPage();
    }

    private Dict getOldEntity(DictParam param) {
        return this.getById(getKey(param));
    }

    private Dict getEntity(DictParam param) {
        Dict entity = new Dict();
        ToolUtil.copyProperties(param, entity);
        return entity;
    }

    private List<Long> getSubIds(Long dictId) {

        ArrayList<Long> longs = new ArrayList<>();

        if (ToolUtil.isEmpty(dictId)) {
            return longs;
        } else {
            List<Dict> list = this.baseMapper.likeParentIds(dictId);
            for (Dict dict : list) {
                longs.add(dict.getDictId());
            }
            return longs;
        }
    }

    private void dictSetPids(Dict param) {
        if (param.getParentId().equals(0L)) {
            param.setParentIds("[0]");
        } else {
            //获取父级的pids
            Long parentId = param.getParentId();
            Dict parent = this.getById(parentId);
            if (parent == null) {
                param.setParentIds("[0]");
            } else {
                param.setParentIds(parent.getParentIds() + "," + "[" + parentId + "]");
            }
        }
    }
}
