package com.x2.modular.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.x2.kernel.model.response.ResponseData;
import com.x2.modular.system.entity.SysFileBusiness;
import com.x2.modular.system.model.result.SysFileBusinessResult;

import java.util.List;

/**
 * 附件业务关系 服务类
 * @author changpeng
 * @date 2021-02-03
 */
public interface SysFileBusinessService extends IService<SysFileBusiness> {

    /**
     * 保存关系
     * @param busSign：业务标识
     * @param busId：业务主键
     * @param fileIds：要保存的文件id
     * @param deleteFileIds：要删除的文件id
     * @return
     */
    ResponseData save(String busSign, Long busId, String fileIds, String deleteFileIds);

    /**
     * 保存关系
     * @param attachFile：附件关系
     * @param busId;
     * @return
     */
    ResponseData saveAttach(String attachFile, Long busId);

    /**
     * 删除
     * @param attachId: 关系id
     * @return 结果
     */
    ResponseData delete(Long attachId);

    /**
     * 获取附件
     * @param busSign：业务标识
     * @param busId: 业务id
     * @return 附件数据列表
     */
    List<SysFileBusinessResult> getAttach(String busSign, Long busId);

    /**
     * 获取附件
     * @param busSign：业务标识
     * @param busId：业务id
     * @param fileId：附件id
     * @return 结果
     */
    SysFileBusinessResult getAttach(String busSign, Long busId, String fileId);

    /**
     * 获取附件
     * @param id: 关系的id
     * @return 结果
     */
    SysFileBusinessResult getAttach(Long id);
}
