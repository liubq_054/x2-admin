package com.x2.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.x2.base.pojo.node.LayuiTreeNode;
import com.x2.base.pojo.node.ZTreeNode;
import com.x2.modular.system.entity.Dept;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2018-12-07
 */
public interface DeptMapper extends BaseMapper<Dept> {

    /**
     * 获取ztree的节点列表
     */
    List<ZTreeNode> tree();

    /**
     * 获取所有部门列表
     */
    Page<Map<String, Object>> list(@Param("page") Page page, @Param("condition") String condition, @Param("deptId") Long deptId);

    /**
     * 获取所有部门
     *
     * @param condition：名称
     * @param deptId：部门id
     * @return 结果
     */
    List<Map<String, Object>> list(@Param("condition") String condition, @Param("deptId") Long deptId);


    /**
     * where pids like ''
     */
    List<Dept> likePids(@Param("deptId") Long deptId);

    /**
     * 获取第一级部门
     *
     * @param condition：名称
     * @param deptId：部门id
     * @return 结果
     */
    List<Map<String, Object>> deptRootList(@Param("condition") String condition, @Param("deptId") Long deptId);
}
