package com.x2.modular.system.utils;

import cn.hutool.core.util.StrUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * @date 2021-08-09 00:00:00
 */
public class UploadUtils {

	public static final String REQUEST_URI = "/content/attach";

	public UploadUtils() {
	}

	private static List<String> imgExt = new ArrayList<>();
	static {
		imgExt.add("png");
		imgExt.add("jpg");
		imgExt.add("bmp");
		imgExt.add("jpeg");
		imgExt.add("jpe");
		imgExt.add("gif");
	}

	/**
	 * 获取文件名后缀
	 * @param fileName: 文件名
	 * @return 结果
	 */
	public static String getFileExt(String fileName){
		String fileExt = null;
		int index = fileName.lastIndexOf(".") ;
		if(index ==-1){
			fileExt = "";
		}
		else{
			fileExt = fileName.substring(index+1)
				.toLowerCase();
		}
		return fileExt;
	}
	
	public static boolean isImage(String fileName){
		return imgExt.contains(getFileExt(fileName));
	}
	
	public static boolean isImageExt(String ext){
		if(StrUtil.isEmpty(ext)){
			return false;
		}
		return imgExt.contains(ext.toLowerCase());
	}
}