package com.x2.modular.system.mapper;

import com.x2.modular.system.entity.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.x2.base.pojo.node.ZTreeNode;
import com.x2.modular.system.model.params.DictParam;
import com.x2.modular.system.model.result.DictResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 基础字典 Mapper 接口
 * @author stylefeng
 * @since 2019-03-13
 */
public interface DictMapper extends BaseMapper<Dict> {

    /**获取ztree的节点列表*/
    List<ZTreeNode> dictTree(@Param("dictTypeId") Long dictTypeId);

    /**where parentIds like ''*/
    List<Dict> likeParentIds(@Param("dictId") Long dictId);

    List<Map<String,String>> listByTypeCodes(@Param("typeCodeList") List<String> typeCodeList);
    /**
     * 列表查询
     * @param paramCondition: 查询条件
     * @return 结果
     */
    List<DictResult> customList(@Param("paramCondition") DictParam paramCondition);
}
