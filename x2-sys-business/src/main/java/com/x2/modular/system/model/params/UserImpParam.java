/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.x2.modular.system.model.params;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.x2.excel.annotation.ExcelField;
import com.x2.excel.converter.Dic2FieldConverter;
import com.x2.excel.converter.Field2DicConverter;
import lombok.Data;

import java.lang.reflect.Field;
import java.util.Date;

/**
 * 用户传输bean
 *
 * @author stylefeng
 * @Date 2017/5/5 22:40
 */
@Data
public class UserImpParam {

    @ExcelField(title = "员工工号", order = 1)
    private String employeeNumber;

    @ExcelField(title = "姓名", order = 2)
    private String name;

    @ExcelField(title = "身份证号码", order = 3)
    private String idCard;

    @ExcelField(title = "部门名称", order = 4)
    private String deptName;
    private Long deptId;

    @ExcelField(title = "性别", order = 5, parentCode = "SEX", readConverter = Dic2FieldConverter.class, writeConverter = Field2DicConverter.class)
    private String sex;

    @ExcelField(title = "出生日期", order = 6)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date birthday;

    @ExcelField(title = "民族", order = 7, parentCode = "NATION", readConverter = Dic2FieldConverter.class, writeConverter = Field2DicConverter.class)
    private String nation;

    @ExcelField(title = "毕业学校", order = 8)
    private String graduateSchool;

    @ExcelField(title = "毕业时间", order = 9)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date graduateTime;

    @ExcelField(title = "所学专业", order = 10)
    private String major;

    @ExcelField(title = "学历", order = 11, parentCode = "education", readConverter = Dic2FieldConverter.class, writeConverter = Field2DicConverter.class)
    private String education;

    @ExcelField(title = "手机号码", order = 12)
    private String mobile;

    @ExcelField(title = "婚姻状况", order = 13, parentCode = "marital_status", readConverter = Dic2FieldConverter.class, writeConverter = Field2DicConverter.class)
    private String maritalStatus;

    @ExcelField(title = "QQ", order = 14)
    private String qqNumber;

    @ExcelField(title = "电话", order = 15)
    private String phone;

    @ExcelField(title = "E-mail", order = 16)
    private String email;

    @ExcelField(title = "MSN", order = 17)
    private String msnNumber;

    @ExcelField(title = "在岗状态", order = 18, parentCode = "ONJOBSTATE", readConverter = Dic2FieldConverter.class, writeConverter = Field2DicConverter.class)
    private String onJobState;


    @ExcelField(title = "职务/职称", order = 19)
    private String positionInfo;

    @ExcelField(title = "入职时间", order = 20)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date entryTime;

    @ExcelField(title = "编制情况", order = 21, parentCode = "identity_category", readConverter = Dic2FieldConverter.class, writeConverter = Field2DicConverter.class)
    private String identityCategory;

    @ExcelField(title = "省份", order = 22)
    private String province;

    @ExcelField(title = "城市", order = 23)
    private String city;

    @ExcelField(title = "兴趣爱好", order = 24)
    private String hobby;

    @ExcelField(title = "邮政编码", order = 25)
    private String postCode;

    @ExcelField(title = "地址/邮政地址", order = 26)
    private String address;


    /**
     * 重置取消空格
     */
    public UserImpParam trim() {
        for (Field f : UserImpParam.class.getDeclaredFields()) {
            f.setAccessible(true);
            try {
                if (f.getType().equals(String.class)) {
                    Object value = f.get(this);
                    if (value != null) {
                        f.set(this, value.toString().trim());
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return this;
    }
}
