package com.x2.modular.system.utils;

import com.x2.core.constant.factory.ConstantFactory;
import com.x2.modular.system.entity.User;

public class UserListener {


    /**
     * 用户变更事件
     *
     * @param newUser
     */
    public static void fireEvent(User newUser) {
        if (newUser == null) {
            return;
        }
        fireEvent(newUser.getUserId());
    }

    /**
     * 用户变更事件
     *
     * @param userId
     */
    public static void fireEvent(Long userId) {
        if (userId == null) {
            return;
        }
        ConstantFactory.me().removeUserById(userId);
        ConstantFactory.me().removeUserNameById(userId);
        ConstantFactory.me().removeUserAccountById(userId);
    }
}
