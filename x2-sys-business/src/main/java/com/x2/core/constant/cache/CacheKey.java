package com.x2.core.constant.cache;

/**
 * 缓存标识前缀集合,常用在ConstantFactory类中
 *
 * @date 2017-04-25 9:37
 */
public interface CacheKey {

    /**用户名称*/
    String USER_NAME = "local:user:name:";
    /**用户信息*/
    String USER_INFO = "local:user:info:";
    /**用户账号*/
    String USER_ACCOUNT = "local:user:account:";
    /**用户工号*/
    String USER_EMPLOYEE_NUMBER = "local:user:employee_number:";
    /**角色名称(多个)*/
    String ROLES_NAME= "local:role:name:ids:";
    /**角色名称(多个)*/
    String ROLES_NAME_LIST = "local:role:name:list:";
    /**角色名称(多个)*/
    String ROLES_NAME_BY_USER_ID = "local:role:name:user:id:";
    /**角色名称(单个)*/
    String SINGLE_ROLE_NAME = "local:role:name:single:";
    /**角色数据权限*/
    String ROLE_AUTH = "local:role:auth:";
    /**角色数据权限*/
    String ROLE_CODE = "local:role:code:";
    /**角色英文名称*/
    String SINGLE_ROLE_TIP = "local:role:tip:single:";
    /**部门名称*/
    String DEPT_NAME = "local:dept:name:";
    /**部门对象*/
    String DEPT_OBJECT = "local:dept:object:";
    /**部门ID*/
    String DEPT_ID = "local:dept:id:";
    /**字典名称*/
    String DIC_NAME = "local:dict:name:";
    /**字典类型*/
    String DICT_TYPE = "local:dict:type:";
    /**字典对象*/
    String DIC_LIST_BY_PCODE = "local:dict:list:";
    /**字典编码*/
    String DIC_CODE = "local:dict:code:";
    /**职位名称*/
    String POSITION_NAME = "local:position:name:";
    /**职位ID*/
    String POSITION_ID = "local:position:id:";
    /**错误编码*/
    String ERROR_CODE = "local:error:code";
    /**行政区缓存key*/
    String REGION_CODE = "local:region:code:";
    /**行政区名称缓存*/
    String REGION__NAME = "local:region:name:";
    /**自然人*/
    String PERSON_KEY = "local:person:info:";
    /**自然人证件和证件类型作为key的缓存*/
    String PERSON_CARD_KEY = "local:person:card:";
    /**数据中心部门缓存key*/
    String CENTER_DEPT = "local:center:dept:";
    /**区县名称缓存*/
    String REGION_NAME = "local:region:region:name:";
    /**区县下级缓存*/
    String REGION_NEXT = "local:region:next:name:";
    /**消息模板缓存key*/
    String MESSAGE_TEMPLATE = "local:message:template:";
}
