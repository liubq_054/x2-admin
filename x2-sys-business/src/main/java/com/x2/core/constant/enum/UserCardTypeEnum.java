package com.x2.core.constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum UserCardTypeEnum {
    /***/
    ID_CARD("身份证"), POLICE_CARD("警官证");

    private String displayName;

    UserCardTypeEnum(String name) {
        this.displayName = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static UserCardTypeEnum getByCode(String code) {
        for (UserCardTypeEnum e : UserCardTypeEnum.values()) {
            if (e.name().equals(code)) {
                return e;
            }
        }
        return UserCardTypeEnum.ID_CARD;
    }

    public static String getByName(String code) {
        UserCardTypeEnum e = getByCode(code);
        if (e == null) {
            return "";
        }
        return e.getDisplayName();
    }

    public static List<Map<String, Object>> collectToMap() {
        UserCardTypeEnum[] array = UserCardTypeEnum.values();
        List<Map<String, Object>> result = new ArrayList<>();
        for (UserCardTypeEnum e : array) {
            Map<String, Object> map = new HashMap<>(2);
            map.put("code", e.toString());
            map.put("name", e.getDisplayName());
            result.add(map);
        }
        return result;
    }

}
