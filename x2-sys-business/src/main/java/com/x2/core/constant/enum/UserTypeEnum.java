package com.x2.core.constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum UserTypeEnum {
    company("公司"), police("公安"), person("自然人"), standard("标准");

    private String displayName;

    UserTypeEnum(String name) {
        this.displayName = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static UserTypeEnum getByCode(String code) {
        for (UserTypeEnum e : UserTypeEnum.values()) {
            if (e.name().equals(code)) {
                return e;
            }
        }
        return UserTypeEnum.standard;
    }

    public static String getByName(String code) {
        UserTypeEnum e = getByCode(code);
        if (e == null) {
            return "";
        }
        return e.getDisplayName();
    }

    public static List<Map<String, Object>> collectToMap() {
        List<Map<String, Object>> result = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>(2);
        map1.put("code", UserTypeEnum.person.toString());
        map1.put("name", UserTypeEnum.person.getDisplayName());
        result.add(map1);
        Map<String, Object> map2 = new HashMap<>(2);
        map2.put("code", UserTypeEnum.company.toString());
        map2.put("name", UserTypeEnum.company.getDisplayName());
        result.add(map2);
        Map<String, Object> map3 = new HashMap<>(2);
        map3.put("code", UserTypeEnum.police.toString());
        map3.put("name", UserTypeEnum.police.getDisplayName());
        result.add(map3);
        return result;
    }

}
