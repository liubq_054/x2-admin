/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.x2.core.constant;

import com.x2.base.auth.context.LoginContextHolder;
import com.x2.core.listener.ConfigListener;

/**
 * 系统常量
 *
 * @author x2
 * @date 2017年2月12日 下午9:42:53
 */
public interface Const {

    /**
     * 管理员角色的名字
     */
    String ADMIN_NAME = "administrator";
    /**
     * 人事管理员角色的名字
     */
    String HR_ADMIN_NAME = "hr_admin";

    /**
     * 管理员id
     */
    Long ADMIN_ID = 1L;

    /**
     * 超级管理员角色id
     */
    Long ADMIN_ROLE_ID = 1L;

    //返回成功
    String RESULT_SUCCESS = "success";
    //返回错误信息
    String RESULT_ERRORMESSAGE = "errorMessage";
    //返回成功信息
    String RESULT_SUCCESSMESSAGE = "successMessage";



    /**
     * 默认的登录页面背景
     *
     * @author x2
     * @Date 2018/10/30 5:51 PM
     */
    public static String loginBg() {
        return ConfigListener.getConf().get("contextPath") + "/assets/common/images/login-register.jpg";
    }


    /**
     * 默认的404错误页面背景
     *
     * @author x2
     * @Date 2018/10/30 5:51 PM
     */
    public static String error404() {
        return ConfigListener.getConf().get("contextPath") + "/assets/common/images/error-bg.jpg";
    }
}
