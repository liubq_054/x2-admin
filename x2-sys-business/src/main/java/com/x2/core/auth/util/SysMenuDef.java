package com.x2.core.auth.util;

import com.x2.base.auth.context.LoginContextHolder;
import com.x2.base.auth.model.LoginUser;
import com.x2.base.pojo.node.MenuNode;
import com.x2.core.util.SpringContextHolder;
import com.x2.modular.system.service.MenuService;
import com.x2.modular.system.service.UserRoleService;

import java.util.ArrayList;
import java.util.List;

public class SysMenuDef {

    /**
     * 所有菜单
     *
     * @return
     */
    public static List<MenuNode> getManageMenus() {
        LoginUser loginUser = LoginContextHolder.getContext().getUser();
        MenuService menuService = SpringContextHolder.getBean(MenuService.class);
        UserRoleService userRoleService = SpringContextHolder.getBean(UserRoleService.class);
        List<Long> roleList;
        if (loginUser.isAdmin()) {
            roleList = new ArrayList<>();
            roleList.add(1L);
        } else {
            roleList = userRoleService.getRoleList(loginUser.getId());
        }
        if (roleList == null || roleList.size() == 0) {
            return new ArrayList<>();
        }
        List<MenuNode> menus = menuService.getMenusByRoleIds(roleList);
        return menus;
    }
}
