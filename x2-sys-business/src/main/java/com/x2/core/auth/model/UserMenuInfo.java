package com.x2.core.auth.model;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class UserMenuInfo {
    private List<Map<String, Object>> types;
    private List<Map<String, Object>> menus;
    private Boolean hasSub;

    public UserMenuInfo(List<Map<String, Object>> types, List<Map<String, Object>> menus, boolean hasSub) {
        this.types = types;
        this.menus = menus;
        this.hasSub = hasSub;
    }
}
