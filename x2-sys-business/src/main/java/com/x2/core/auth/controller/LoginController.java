/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.x2.core.auth.controller;

import com.x2.base.auth.context.LoginContextHolder;
import com.x2.base.auth.service.AuthService;
import com.x2.base.consts.ConstantsContext;
import com.x2.core.auth.util.SysMenuUtil;
import com.x2.core.base.controller.BaseController;
import com.x2.core.constant.KaptchaConst;
import com.x2.core.exception.InvalidKaptchaException;
import com.x2.core.util.ToolUtil;
import com.x2.kernel.model.response.ResponseData;
import com.x2.kernel.model.response.SuccessResponseData;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 登录控制器
 *
 * @author x2
 * @Date 2017年1月10日 下午8:25:24
 */
@Controller
public class LoginController extends BaseController {

    @Autowired
    private AuthService authService;

    /**
     * 跳转到主页
     *
     * @author x2
     * @Date 2018/12/23 5:41 PM
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String loginGet(Model model) {

        if (LoginContextHolder.getContext().hasLogin()) {
            Map<String, Object> userIndexInfo = SysMenuUtil.getUserIndexInfo();
            model.addAllAttributes(userIndexInfo);
            if (userIndexInfo == null) {
                model.addAttribute("tips", "该用户没有角色，无法登陆");
                return "/login.html";
            } else {
                model.addAllAttributes(userIndexInfo);
                return "/index.html";
            }
        } else {
            return "/login.html";
        }
    }

    /**
     * 跳转到登录页面
     *
     * @author x2
     * @Date 2018/12/23 5:41 PM
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPost(Model model) {
        if (LoginContextHolder.getContext().hasLogin()) {
            return REDIRECT + "/";
        } else {
            return "/login.html";
        }
    }

    /**
     * 点击登录执行的动作
     *
     * @param request:    请求
     * @param response：相应
     * @return 结果
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseData loginValidate(HttpServletRequest request, HttpServletResponse response) {
        String username = super.getPara("username");
        String password = super.getPara("password");
        //验证验证码是否正确
        if (ConstantsContext.getKaptchaOpen()) {
            String kaptcha = super.getPara(KaptchaConst.KAPTCHA_SESSION_KEY).trim();
            String code = (String) super.getSession().getAttribute(KaptchaConst.KAPTCHA_SESSION_KEY);
            if (ToolUtil.isEmpty(kaptcha) || !kaptcha.equalsIgnoreCase(code)) {
                throw new InvalidKaptchaException();
            }
        }
        String token;
        try {
            token = authService.login(username, password);
        } catch (Exception e) {
            return ResponseData.error(e.getMessage());
        }
        return new SuccessResponseData(token);
    }

    /**
     * 退出登录
     *
     * @return 结果
     */
    @RequestMapping(value = "/logout")
    @ResponseBody
    public ResponseData logOut() {
        authService.logout();
        return new SuccessResponseData();
    }

    /**
     * 跳转到登录页面
     *
     * @author x2
     * @Date 2018/12/23 5:41 PM
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Model model) {
        return "/home.html";
    }
}