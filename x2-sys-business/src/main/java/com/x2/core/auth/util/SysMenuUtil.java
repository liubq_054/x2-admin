package com.x2.core.auth.util;

import com.x2.base.auth.context.LoginContextHolder;
import com.x2.base.auth.model.LoginUser;
import com.x2.base.pojo.node.MenuNode;
import com.x2.core.auth.model.UserMenuInfo;

import java.util.*;

public class SysMenuUtil {


    /**
     * 获取用户首页信息
     *
     * @author x2
     * @Date 2019/10/17 16:18
     */
    public static Map<String, Object> getUserIndexInfo() {
        //获取当前用户角色列表
        LoginUser user = LoginContextHolder.getContext().getUser();
        UserMenuInfo info = getUserMenuNodes();
        HashMap<String, Object> result = new HashMap<>(3);
        result.put("menus", info.getMenus());
        result.put("systemTypes", info.getTypes());
        result.put("hasSubMenu", info.getHasSub() ? "Y" : null);
        result.put("name", user.getName());
        return result;
    }


    /**
     * 获取用户菜单列表
     *
     * @return 菜单列表
     */
    private static UserMenuInfo getUserMenuNodes() {
        //定义不同系统分类的菜单集合
        ArrayList<Map<String, Object>> lists = new ArrayList<>();
        //取得固定菜单
        List<MenuNode> menus = SysMenuDef.getManageMenus();
        //头部分类
        List<MenuNode> topMenus = getTopMenus(menus);
        Map<String, Object> item;
        List<Map<String, Object>> topMenuList = new ArrayList<>();
        for (MenuNode node : topMenus) {
            item = new HashMap<>();
            item.put("code", String.valueOf(node.getId()));
            item.put("name", node.getName());
            item.put("sort", node.getNum());
            item.put("icon", "");
            topMenuList.add(item);
        }

        //底部分类
        Map<Long, Long> idTreeMap = getMenuTreeIdMap(menus);
        String sysType;
        String allMenuSystemType = "";
        for (Map<String, Object> topeMenu : topMenuList) {
            //获取当前系统分类下菜单集合
            ArrayList<MenuNode> originSystemTypeMenus = new ArrayList<>();
            Long parentId;
            sysType = (String) topeMenu.get("code");
            for (MenuNode menu : menus) {
                //不分组
                if (allMenuSystemType.length() > 0) {
                    originSystemTypeMenus.add(menu);
                    sysType = allMenuSystemType;
                }
                //分组
                else {
                    //分组内的根跳过
                    if (menu.getParentId().equals(Long.valueOf(0L))) {
                        continue;
                    }
                    parentId = getRootId(idTreeMap, menu.getId());
                    if (parentId != null && String.valueOf(parentId).equals(sysType)) {
                        originSystemTypeMenus.add(menu);
                    }
                }

            }
            //拼接存放key为系统分类编码，value为该分类下菜单集合的map
            HashMap<String, Object> map = new HashMap<>();
            List<MenuNode> treeSystemTypeMenus = MenuNode.buildTitle(originSystemTypeMenus);
            map.put("systemType", sysType);
            map.put("menus", treeSystemTypeMenus);
            lists.add(map);
        }
        return new UserMenuInfo(topMenuList, lists, true);
    }


    /**
     * 根据角色获取菜单
     *
     * @return
     * @date 2017年2月19日 下午10:35:40
     */
    private static List<MenuNode> getTopMenus(List<MenuNode> menus) {


        //给所有的菜单url加上ctxPath
        List<MenuNode> menuList = new ArrayList<>();
        if (menuList != null) {
            for (MenuNode menuItem : menus) {
                if (menuItem.getParentId() > 0) {
                    continue;
                }
                menuList.add(menuItem);
            }
        }
        Collections.sort(menuList, new Comparator<MenuNode>() {
            @Override
            public int compare(MenuNode t0, MenuNode t1) {
                int sort0 = t0.getNum() == null ? 0 : t0.getNum();
                int sort1 = t1.getNum() == null ? 0 : t1.getNum();
                return sort0 - sort1;
            }
        });
        return menuList;
    }


    /**
     * 根据子父关系
     *
     * @return
     * @date 2017年2月19日 下午10:35:40
     */
    private static Map<Long, Long> getMenuTreeIdMap(List<MenuNode> menus) {


        //给所有的菜单url加上ctxPath
        Map<Long, Long> menuList = new HashMap<Long, Long>();
        if (menuList != null) {
            for (MenuNode menuItem : menus) {
                menuList.put(menuItem.getId(), menuItem.getParentId());
            }
        }
        return menuList;
    }

    /**
     * 根据子父关系
     *
     * @return
     * @date 2017年2月19日 下午10:35:40
     */
    private static Long getRootId(Map<Long, Long> map, Long id) {
        Long queryId = id;
        if (queryId == null) {
            return null;
        }
        if (queryId.intValue() == 0) {
            return queryId;
        }
        Long parentId;
        while (map.containsKey(queryId)) {
            parentId = map.get(queryId);
            if (parentId.intValue() == 0) {
                return queryId;
            }
            queryId = parentId;
        }
        return null;
    }

}
