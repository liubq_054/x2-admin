package com.x2.core.auth;

import com.x2.base.auth.model.LoginUser;

public class LoginUserExtendProxy {
    public static LoginUserExtendProxy build(LoginUser loginUser) {
        return new LoginUserExtendProxy(loginUser);

    }

    private LoginUserExtendProxy(LoginUser loginUser) {
        this.loginUser = loginUser;
    }

    private LoginUser loginUser;


    public Long getId() {
        return loginUser != null ? loginUser.getId() : null;
    }

    public void setId(Long id) {
        if (loginUser != null) {
            loginUser.setId(id);
        }
    }

    public String getAccount() {
        return loginUser != null ? loginUser.getAccount() : null;
    }

    public void setAccount(String account) {
        if (loginUser != null) {
            loginUser.setAccount(account);
        }
    }

    public String getName() {
        return loginUser != null ? loginUser.getName() : null;
    }

    public void setName(String name) {
        if (loginUser != null) {
            loginUser.setName(name);
        }
    }


    public Long getDeptId() {
        return toLong("deptId");
    }

    public void setDeptId(Long deptId) {
        put("deptId", deptId);
    }

    public String getDeptName() {
        return toString("deptName");
    }

    public void setDeptName(String deptName) {
        put("deptName", deptName);
    }

    public String getUserType() {
        return toString("userType");
    }

    public void setUserType(String userType) {
        put("userType", userType);
    }

    public String getAvatar() {
        return toString("avatar");
    }

    public void setAvatar(String avatar) {
        put("avatar", avatar);
    }

    private Long toLong(String key) {
        if (loginUser != null) {
            Object v = loginUser.getExtendMap().get(key);
            if (v == null) {
                return null;
            }
            if (v instanceof Long) {
                return (Long) v;
            } else {
                try {
                    return Long.valueOf(v.toString());
                } catch (Exception ex) {
                    return null;
                }
            }
        }
        return null;
    }

    private String toString(String key) {
        if (loginUser != null) {
            Object v = loginUser.getExtendMap().get(key);
            if (v == null) {
                return null;
            }
            if (v instanceof String) {
                return (String) v;
            } else {
                return v.toString();
            }
        }
        return null;
    }

    /**
     * 发值
     *
     * @param key
     * @param value
     */
    private void put(String key, Object value) {
        if (loginUser != null) {
            loginUser.getExtendMap().put(key, value);
        }
    }

    /**
     * 取得用户信息
     *
     * @return
     */
    public LoginUser getLoginUser() {
        return loginUser;
    }
}
