package com.x2.excel.utils;

import com.x2.core.util.SpringContextHolder;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018-1-16.
 */
public class DicUtils {

    private static Object constantFactory;

    public static Object getConstantFactory() {
        if(constantFactory==null){
            constantFactory = SpringContextHolder.getBean("constantFactory");
        }
        return constantFactory;
    }
    public static String getDictCodesByPCode(String parentCode, String dicName){
        try {
            Method getDicCode = ReflectionUtils.findMethod(getConstantFactory().getClass(), "getDictCodesByPCode", new Class[]{String.class, String.class});
            String dicCode = (String) ReflectionUtils.invokeMethod(getDicCode, getConstantFactory(), parentCode, dicName);
            return dicCode;
        } catch (Exception e) {
            return "";
        }
    }

    public static String getDictNameByPCode(String parentCode, String dicCode){
        try {
            Method getDicName = ReflectionUtils.findMethod(getConstantFactory().getClass(), "getDictNameByPCode", new Class[]{String.class, String.class});
            String dicName = (String) ReflectionUtils.invokeMethod(getDicName, getConstantFactory(), parentCode, dicCode);
            return dicName;
        } catch (Exception e) {
            return "";
        }
    }

    public static List<Map<String, Object>> getDictListByPCode(String parentCode){
        try {
            Method getDictList = ReflectionUtils.findMethod(getConstantFactory().getClass(), "getDictListByPCode", new Class[]{String.class});
            Object dicListObj = ReflectionUtils.invokeMethod(getDictList, getConstantFactory(), parentCode);
            if(dicListObj==null){
                return new ArrayList<>();
            }
            List<Map<String, Object>> dicList = (List<Map<String, Object>>)dicListObj;
            return dicList;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
    private static  Map<String, Map<String,Object>> dicMap;

    public static Map<String, Map<String,Object>> getDicMap(){
        if(dicMap == null || dicMap.size()<1){
            try {
                Object cacheClientService = SpringContextUtil.getBean("cacheClientServiceLocalImpl");
                Method getDicMap = ReflectionUtils.findMethod(cacheClientService.getClass(), "getCache", new Class[]{String.class});
                String dicMapStr = (String) ReflectionUtils.invokeMethod(getDicMap, cacheClientService, "dicMap");

                Class<?> jsonUtil = Class.forName("com.jc.foundation.util.JsonUtil");
                Method method = jsonUtil.getMethod("json2Java", String.class,Class.class);
                dicMap = (Map) method.invoke(null, dicMapStr, Map.class);

                return dicMap;
            } catch (Exception e) {
                return new HashMap<String, Map<String,Object>>();
            }
        }else{
            return dicMap;
        }
    }

}
