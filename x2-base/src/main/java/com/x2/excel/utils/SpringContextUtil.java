package com.x2.excel.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * 获取spring容器，以访问容器中定义的其他bean
 */
@Component
@Lazy(false)
public class SpringContextUtil implements ApplicationContextAware,
        DisposableBean {

    private static ApplicationContext applicationContext = null;

    private static Logger logger = LoggerFactory
            .getLogger(SpringContextUtil.class);

    /**
     * @description 取得存储在静态变量中的ApplicationContext
     * @return 返回ApplicationContext
     * @author
     * @version 2014-03-24
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * @description 从静态变量applicationContext中取得Bean, 自动转型为所赋值对象的类型
     * @param name
     *            bean的名字
     * @return 返回取得的bean
     * @author
     * @version 2014-03-24
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBean(String name) {
        return (T) applicationContext.getBean(name);
    }

    /**
     * @description 静态变量applicationContext中取得Bean, 自动转型为所赋值对象的类型
     * @param requiredType
     *            bean的类型
     * @return 返回取得的bean
     * @author
     * @version 2014-03-24
     */
    public static <T> T getBean(Class<T> requiredType) {
        return applicationContext.getBean(requiredType);
    }

    /**
     * @description 清除SpringContextHolder中的ApplicationContext为Null
     * @author
     * @version 2014-03-24
     */
    public static void clearHolder() {
        logger.debug("清除SpringContextHolder中的ApplicationContext:"
                + applicationContext);
        applicationContext = null;
    }

    /**
     * @description 实现ApplicationContextAware接口, 注入Context到静态变量中
     * @param applicationContext
     *            注入的Context
     * @author
     * @version 2014-03-24
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        if (SpringContextUtil.applicationContext != null) {
            logger.warn("SpringContextHolder中的ApplicationContext被覆盖, 原有ApplicationContext为:"
                    + SpringContextUtil.applicationContext);
        }
        SpringContextUtil.applicationContext = applicationContext; // NOSONAR
    }

    /**
     * @description 实现DisposableBean接口, 在Context关闭时清理静态变量.
     * @throws Exception
     * @author
     * @version 2014-03-24
     */
    @Override
    public void destroy() throws Exception {
        SpringContextUtil.clearHolder();
    }

}
