package com.x2.excel.converter;

/**
 * Created by Administrator on 2017-12-28.
 */
public class yesOrNotExcelConverter implements WriteConvertible {

    public yesOrNotExcelConverter(){

    }


    @Override
    public Object execWrite(Object object) {

        if(object != null){
           if("Y".equals(object)){
               return "是";
           }else if("N".equals(object)){
               return "否";
           }
        }

        return "未知" ;
    }
}
