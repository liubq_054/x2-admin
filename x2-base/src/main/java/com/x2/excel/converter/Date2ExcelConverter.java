package com.x2.excel.converter;

import com.x2.core.util.DateUtils;
import com.x2.core.util.EmptyUtil;

import java.util.Date;

/**
 * Created by Administrator on 2017-12-28.
 */
public class Date2ExcelConverter implements WriteConvertible {

    public Date2ExcelConverter() {

    }

    public Date2ExcelConverter(String formatStr) {
        this.formatStr = formatStr;
    }

    private String formatStr;

    public String getFormatStr() {
        return formatStr;
    }

    public void setFormatStr(String formatStr) {
        this.formatStr = formatStr;
    }

    @Override
    public Object execWrite(Object object) {

        if (EmptyUtil.isEmpty(formatStr)) {
            formatStr = DateUtils.DATE_FORMAT_SEC;
        }
        if (object == null) {
            return "";
        }
        Date date = (Date) object;
        return DateUtils.date2Str(date, formatStr);
    }
}
