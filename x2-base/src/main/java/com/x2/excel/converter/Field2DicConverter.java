package com.x2.excel.converter;

import cn.hutool.core.util.StrUtil;
import com.x2.excel.utils.DicUtils;
import com.x2.excel.utils.SpringContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2017-12-28.
 */
@Component
public class Field2DicConverter implements WriteConvertible {

    @Autowired
    private SpringContextUtil springContextUtil;

    public Field2DicConverter(){

    }

    public Field2DicConverter(String dicName, String parentCode){
        this.dicName = dicName;
        this.parentCode = parentCode;
    }

    /**
     * 字段的字典名
     */
    private String dicName;

    /**
     * 字段的字典父类代码
     */
    private String parentCode;

    public String getDicName() {
        return dicName;
    }

    public void setDicName(String dicName) {
        this.dicName = dicName;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    @Override
    public Object execWrite(Object object) {

        String strVal = null;
        try {
            if(object!=null&& StrUtil.isNotEmpty(parentCode)){
                String dicStr = object.toString();
                if(dicStr.indexOf(",")>0){
                    String[] dics = dicStr.split(",");
                    if(dics.length>0){
                        StringBuffer dicValSB = new StringBuffer();
                        for(String dic :dics){
                            String dicVal =DicUtils.getDictNameByPCode(parentCode, dic);
                            if(StrUtil.isNotEmpty(dicVal)){
                                dicValSB.append(dicVal).append(",");
                            }
                        }
                        if(StrUtil.isNotEmpty(dicValSB)){
                            strVal = StrUtil.removeSuffix(dicValSB.toString(), ", ");
                        }
                    }
                }else{
                    String dicVal = DicUtils.getDictNameByPCode(parentCode, dicStr);
                    if(StrUtil.isNotEmpty(dicVal)){
                        strVal = dicVal;
                    }
                }

            }

        }catch (Exception e){
            return object;
        }
        return  strVal== null ? object : strVal;
    }
}
