package com.x2.excel.converter;

import com.x2.core.util.DateUtils;
import com.x2.core.util.EmptyUtil;

import java.text.ParseException;

/**
 * Created by Administrator on 2017-12-28.
 */
public class Excel2DateConverter implements ReadConvertible {


    public Excel2DateConverter() {

    }

    public Excel2DateConverter(String formatStr) {
        this.formatStr = formatStr;
    }

    private String formatStr;

    public String getFormatStr() {
        return formatStr;
    }

    public void setFormatStr(String formatStr) {
        this.formatStr = formatStr;
    }

    @Override
    public Object execRead(String object) throws ParseException {
        if (EmptyUtil.empty(formatStr)) {
            formatStr = DateUtils.DATE_FORMAT_SEC;
        }
        if (EmptyUtil.isEmpty(object)) {
            return null;
        }
        return DateUtils.str2Date(object, formatStr);
    }
}
