package com.x2.excel.vo;

import lombok.Data;

/**
 * 合并单元格属性
 * @author 常鹏
 * @version 1.0
 * @date 2020/11/23 13:59
 */
@Data
public class BusinessMergedCell {
    private int rowStart;
    private int rowEnd;
    private int cellStart;
    private int cellEnd;

    public static BusinessMergedCell create(int rowStart, int rowEnd, int cellStart, int cellEnd) {
        BusinessMergedCell result = new BusinessMergedCell();
        result.setRowStart(rowStart);
        result.setRowEnd(rowEnd);
        result.setCellStart(cellStart);
        result.setCellEnd(cellEnd);
        return result;
    }
}
