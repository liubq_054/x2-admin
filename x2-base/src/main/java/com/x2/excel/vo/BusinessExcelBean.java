package com.x2.excel.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 常鹏
 * @version 1.0
 * @date 2020/11/23 11:28
 */
@Data
public class BusinessExcelBean {
    private String key;
    private String value;
    private List<BusinessExcelBean> childList;

    private void addChild(String key, String value){
        this.childList.add(BusinessExcelBean.create(key, value));
    }

    public static BusinessExcelBean create(String key, String value) {
        BusinessExcelBean entity = new BusinessExcelBean();
        entity.setChildList(new ArrayList<>());
        entity.setKey(key);
        entity.setValue(value);
        return entity;
    }

    public static BusinessExcelBean createResult(String key, String value) {
        BusinessExcelBean result = BusinessExcelBean.create(key, value);
        result.addChild(key, value);
        return result;
    }
}
