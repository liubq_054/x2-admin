package com.x2.excel.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 常鹏
 * @version 1.0
 * @date 2020/12/3 10:00
 */
@Data
public class BusinessExcelHeader {
    private String text;
    private Integer colspan;
    private int width;
    private List<BusinessExcelHeader> childList;

    /**
     * 创建单行时生成表头
     * @param text: 显示文字
     * @param colspan：合并单元格数量
     * @param width: 单元格宽度
     * @return
     */
    public static BusinessExcelHeader create(String text, Integer colspan, Integer width) {
        BusinessExcelHeader entity = new BusinessExcelHeader();
        entity.setColspan(colspan);
        entity.setText(text);
        entity.setWidth(width);
        entity.addChild(text, (int) width / colspan);
        return entity;
    }

    /**
     * 创建多行时生成表头
     * @param text: 显示文字
     * @param colspan：合并单元格数量
     * @param width: 单元格宽度
     * @return
     */
    public static BusinessExcelHeader createMultiple(String text, Integer colspan, Integer width) {
        BusinessExcelHeader entity = new BusinessExcelHeader();
        entity.setColspan(colspan);
        entity.setText(text);
        entity.setWidth(width);
        return entity;
    }

    public void addChild(String text, Integer width) {
        BusinessExcelHeader entity = new BusinessExcelHeader();
        entity.setText(text);
        entity.setWidth(width);
        if (this.childList == null) {
            this.childList = new ArrayList<>();
        }
        this.childList.add(entity);
    }
}
