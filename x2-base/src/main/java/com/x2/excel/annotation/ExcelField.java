/*
 *
 *                  Copyright 2017 Crab2Died
 *                     All rights reserved.
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Browse for more information ：
 * 1) https://gitee.com/Crab2Died/Excel4J
 * 2) https://github.com/Crab2died/Excel4J
 *
 */

package com.x2.excel.annotation;

import com.x2.excel.converter.DefaultConvertible;
import com.x2.excel.converter.ReadConvertible;
import com.x2.excel.converter.WriteConvertible;

import java.lang.annotation.*;

/**
 * 功能说明: 用来在对象的属性上加入的annotation，通过该annotation说明某个属性所对应的标题
 * @author Qing
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface ExcelField {

    /**
     * 属性的标题名称
     * @return 表头名
     */
    String title();

    String className() default "";
    /**
     * 属性的长度
     * @return 字段长度
     */
    long size() default Long.MAX_VALUE;

    /**
     * 字典名称
     * @return 字典名称
     */
    String dicName() default "";

    /**
     * 字典父类标识
     * @return 字典父类标识
     */
    String parentCode() default "";

    /**
     * 写数据转换器
     *
     * @see WriteConvertible
     * @return 写入Excel数据转换器
     */
    Class<? extends WriteConvertible> writeConverter()
            default DefaultConvertible.class;

    /**
     * 读数据转换器
     *
     * @see ReadConvertible
     * @return 读取Excel数据转换器
     */
    Class<? extends ReadConvertible> readConverter()
            default DefaultConvertible.class;

    /**
     * 在excel的顺序
     *
     * @return 列表顺序
     */
    int order() default Integer.MAX_VALUE;;
}
