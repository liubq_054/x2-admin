package com.x2.base.bean;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

/**
 * 基础bean
 * @author 常鹏
 * @version 1.0
 * @date 2021/3/18 10:08
 */
public class BaseBean implements Serializable {
    /**主键*/
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    /**删除标识，默认 0 逻辑删除 1*/
    @TableField("delete_flag")
    private Integer deleteFlag;
    /**创建时间*/
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;
    /**创建用户*/
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    private Long createUser;
    /**创建记录时用户所在部门*/
    @TableField(value = "create_dept", fill = FieldFill.INSERT)
    private Long createDept;
    /**更新时间*/
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;
    /**更新用户*/
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    private Long updateUser;
    /**使用数据权限时，表的别名，默认为"t"*/
    @TableField(exist = false)
    private String tableAlias;
    /**数据权限类型，默认为USER*/
    @TableField(exist = false)
    private String dataAuthType;
    /**数据权限的用户或部门id，默认为当前登录用户的id或部门id*/
    @TableField(exist = false)
    private Long dataAuthId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getCreateDept() {
        return createDept;
    }

    public void setCreateDept(Long createDept) {
        this.createDept = createDept;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    public String getDataAuthType() {
        return dataAuthType;
    }

    public void setDataAuthType(String dataAuthType) {
        this.dataAuthType = dataAuthType;
    }

    public Long getDataAuthId() {
        return dataAuthId;
    }

    public void setDataAuthId(Long dataAuthId) {
        this.dataAuthId = dataAuthId;
    }
}
