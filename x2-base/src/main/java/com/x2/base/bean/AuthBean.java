package com.x2.base.bean;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * 数据隔离
 * @author 常鹏
 * @version 1.0
 * @date 2023/2/24 15:40
 */
public class AuthBean implements Serializable {
    /**使用数据权限时，表的别名，默认为"t"*/
    @TableField(exist = false)
    private String tableAlias;
    /**数据权限类型，默认为USER*/
    @TableField(exist = false)
    private String dataAuthType;
    /**数据权限的用户或部门id，默认为当前登录用户的id或部门id*/
    @TableField(exist = false)
    private Long dataAuthId;

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    public String getDataAuthType() {
        return dataAuthType;
    }

    public void setDataAuthType(String dataAuthType) {
        this.dataAuthType = dataAuthType;
    }

    public Long getDataAuthId() {
        return dataAuthId;
    }

    public void setDataAuthId(Long dataAuthId) {
        this.dataAuthId = dataAuthId;
    }
}
