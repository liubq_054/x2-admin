package com.x2.base.pojo.node;

import com.x2.core.util.ToolUtil;
import com.x2.kernel.model.tree.Tree;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * layui属性组件节点
 *
 * @author x2
 * @Date 2019-8-26 14:01
 */
@Data
public class LayuiTreeNode implements Tree {

    /**
     * 节点id
     */
    private Long id;

    /**
     * 父级节点id
     */
    private Long pid;
    /**
     * 父级节点ids
     */
    private String pids;

    /**
     * 节点名称
     */
    private String title;

    /**
     * 节点code(用于字典树)
     */
    private String code;

    /**
     * 节点是否初始展开
     */
    private Boolean spread;

    /**
     * 节点是否初始为选中状态
     */
    private Boolean checked;

    /**
     * 节点是否为禁用状态
     */
    private Boolean disabled;

    /**允许的扩展数据*/
    private Map<String, Object> extend;
    /**子节点*/
    private List<LayuiTreeNode> children = new ArrayList<>();

    @Override
    public String getNodeId() {
        if (ToolUtil.isNotEmpty(id)) {
            return String.valueOf(id);
        } else {
            return null;
        }
    }

    @Override
    public String getNodeParentId() {
        if (ToolUtil.isNotEmpty(pid)) {
            return String.valueOf(pid);
        } else {
            return null;
        }
    }

    @Override
    public void setChildrenNodes(List childrenNodes) {
        this.children = childrenNodes;
    }

}
