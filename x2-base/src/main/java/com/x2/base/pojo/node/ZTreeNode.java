package com.x2.base.pojo.node;

import lombok.Data;

@Data
public class ZTreeNode {
    private Long id;
    private Long pId;
    private String name;
    private Boolean open;
    private Boolean checked;
    private String iconSkin;

    public static ZTreeNode createParent() {
        ZTreeNode zTreeNode = new ZTreeNode();
        zTreeNode.setChecked(true);
        zTreeNode.setId(0L);
        zTreeNode.setName("顶级");
        zTreeNode.setOpen(true);
        zTreeNode.setPId(-1L);
        return zTreeNode;
    }


}
