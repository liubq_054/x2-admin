package com.x2.base.auth.context;

import com.x2.core.util.SpringContextHolder;

/**
 * 当前登录用户信息获取的接口
 *
 * @author x2
 * @Date 2019/7/18 22:27
 */
public class LoginContextHolder {

    public static LoginContext getContext() {
        return SpringContextHolder.getBean(LoginContext.class);
    }

}
