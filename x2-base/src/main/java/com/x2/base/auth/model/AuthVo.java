package com.x2.base.auth.model;

import lombok.Data;

/**
 * 登录查询条件bean
 * @author 常鹏
 * @version 1.0
 * @date 2023/2/25 14:23
 */
@Data
public class AuthVo {
    /**用户类型*/
    private String userType;
    /**用户名*/
    private String username;
    /**密码*/
    private String password;
    /**token*/
    private String token;
    /**是否记住密码 Y:是*/
    private String remember;
}
