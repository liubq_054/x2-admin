package com.x2.base.jdbc;

import lombok.Data;

import java.util.List;

@Data
public class JdbcResult {
    private Object pk;
    private String sql;
    private List<Object> paraList;

    public JdbcResult(String sql, List<Object> paraList) {
        this.sql = sql;
        this.paraList = paraList;
    }

    public JdbcResult(Object pk, String sql, List<Object> paraList) {
        this.pk = pk;
        this.sql = sql;
        this.paraList = paraList;
    }

    public Object[] getParas() {
        return paraList.toArray(new Object[0]);
    }
}
