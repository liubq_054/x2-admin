package com.x2.base.jdbc;

import lombok.Data;

@Data
public class FieldMeta {
    private String columnName;
    private String fieldName;
    private String type;

    public FieldMeta(String columnName, String fieldName, String type) {
        this.columnName = columnName;
        this.fieldName = fieldName;
        this.type = type;
    }
}
