package com.x2.config;

import com.x2.core.properties.GlobalProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 项目中的配置
 *
 * @author x2
 * @Date 2019/5/10 22:45
 */
@Configuration
public class PropertiesConfig {


    /**
     * Guns的属性配置
     *
     * @author x2
     * @Date 2019-06-13 08:56
     */
    @Bean
    @ConfigurationProperties(prefix = GlobalProperties.PREFIX)
    public GlobalProperties gunsProperties() {
        return new GlobalProperties();
    }

}
