package com.x2.config.listener;


import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.springframework.web.context.ContextLoaderListener;


/**
 * @title 工具类
 * @description 系统初始化方类
 */
@WebListener
public class BaseListener extends ContextLoaderListener implements ServletContextListener {

    public BaseListener() {
    }

    /**
     * @param event spring的初始化事件
     * @description 系统初始化方法
     * @version 2020-03-06
     */
    @Override
    public void contextInitialized(ServletContextEvent event) {
        this.initial(event);
    }

    /**
     * @param event spring的初始化事件
     * @description 系统关闭时调用的方法
     * @version 2020-03-06
     */
    @Override
    public void contextDestroyed(ServletContextEvent event) {
    }

    /**
     * @description 子系统初始化数据的方法
     */
    protected void initial(ServletContextEvent event) {
    }
}
