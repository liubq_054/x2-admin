package com.x2.config.web;

import cn.hutool.core.date.DateUtil;
import com.x2.core.util.EmptyUtil;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import java.util.Date;

/**
 * 默认的string to date的转化
 *
 * @author x2
 * @Date 2019/2/12 20:09
 */
@Configuration
public class String2DateConfig {

    @Autowired
    private RequestMappingHandlerAdapter handlerAdapter;

    /**
     * 默认时间转化器
     */
    @PostConstruct
    public void addConversionConfig() {
        ConfigurableWebBindingInitializer initializer = (ConfigurableWebBindingInitializer) handlerAdapter.getWebBindingInitializer();
        if ((initializer != null ? initializer.getConversionService() : null) != null) {
            GenericConversionService genericConversionService = (GenericConversionService) initializer.getConversionService();
            genericConversionService.addConverter(new StringToDateConverter());
        }
    }

    /**
     * 时间字符串转date的格式
     *
     * @author x2
     * @Date 2019/10/22 13:42
     */
    public static class StringToDateConverter implements Converter<String, Date> {
        @Override
        public Date convert(String dateString) {
            if (EmptyUtil.isEmpty(dateString)) {
                return null;
            } else {
                return DateUtil.parse(dateString);
            }
        }
    }

}
