package com.x2.core.auth.filter;

import com.x2.base.auth.jwt.JwtTokenUtil;
import com.x2.base.consts.ConstantsContext;
import com.x2.core.auth.cache.SessionManager;
import com.x2.core.util.ToolUtil;
import io.jsonwebtoken.JwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

/**
 * jwt token过滤器
 *
 * @author liubq
 * @date 2019/7/20 21:33
 */

public class JwtAuthorizationTokenFilter extends OncePerRequestFilter {

    private SessionManager sessionManager;

    public JwtAuthorizationTokenFilter(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {

        //权限校验的头部
        String tokenHeader = ConstantsContext.getTokenHeaderName();
        final String requestHeader = request.getHeader(tokenHeader);

        String username = null;
        String authToken = null;

        // 1.从cookie中获取token
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (tokenHeader.equals(cookie.getName())) {
                    authToken = cookie.getValue();
                }
            }
        }

        // 2.如果cookie中没有token，则从header中获取token
        if (ToolUtil.isEmpty(authToken)) {
            if (requestHeader != null && requestHeader.startsWith("Bearer ")) {
                authToken = requestHeader.substring(7);
            }
        }

        // 3.通过token获取用户名
        if (ToolUtil.isNotEmpty(authToken)) {
            try {
                username = JwtTokenUtil.getJwtPayLoad(authToken).getAccount();
            } catch (IllegalArgumentException | JwtException e) {
                //请求token为空或者token不正确，忽略，并不是所有接口都要鉴权
            }
        }

        // 4.如果账号不为空，并且没有设置security上下文，就设置上下文
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            //从缓存中拿userDetails
            UserDetails userDetails = sessionManager.getSession(authToken);
            if (userDetails == null) {

                //删除cookies
                Cookie[] tempCookies = request.getCookies();
                for (Cookie cookie : tempCookies) {
                    if (tokenHeader.equals(cookie.getName())) {
                        Cookie temp = new Cookie(cookie.getName(), "");
                        temp.setMaxAge(0);
                        temp.setPath("/");
                        response.addCookie(temp);
                    }
                }

                //跳转到登录超时
                response.setHeader("Guns-Session-Timeout", "true");
                chain.doFilter(request, response);
                return;
            }

            //创建当前登录上下文
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                    userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        chain.doFilter(request, response);
    }
}
