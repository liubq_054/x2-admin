package com.x2.core.auth.cache;


import com.x2.base.auth.model.LoginUser;

/**
 * 会话管理
 * @author fengshuonan
 * @date 2019-09-28-14:43
 */
public interface SessionManager {

    /**缓存前缀*/
    String SESSION_PREFIX = "LOGIN_USER_";

    /**
     * 创建会话
     * @param token: tokens
     * @param loginUser: 登录对象
     */
    void createSession(String token, LoginUser loginUser);

    /**
     * 获取会话
     * @author fengshuonan
     * @param token: token
     * @return 登录对象
     */
    LoginUser getSession(String token);

    /**
     * 删除会话
     * @param token: token
     */
    void removeSession(String token);

    /**
     * 是否已经登陆
     * @param token：token
     * @return 判断结果
     */
    boolean haveSession(String token);

}
