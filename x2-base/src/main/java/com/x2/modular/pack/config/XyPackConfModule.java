package com.x2.modular.pack.config;

/**
 * 模块名称
 */
public class XyPackConfModule {
    //代码文件
    private String codeFile;
    //导出目录
    private String outDir;

    public XyPackConfModule(String codeFile, String outDir) {
        this.codeFile = codeFile;
        this.outDir = outDir;

    }

    public String getCodeFile() {
        return codeFile;
    }


    public String getOutDir() {
        return outDir;
    }


}
