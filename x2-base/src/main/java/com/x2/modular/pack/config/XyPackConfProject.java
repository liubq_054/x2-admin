package com.x2.modular.pack.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 工程配置
 */
public class XyPackConfProject {
    //基础目录
    private String outBaseDir;
    //代码目录
    private String codeBaseDir;
    //保留jar名称
    private String prefixJarName;
    //模块列表
    private List<XyPackConfModule> moduleList = new ArrayList<>();


    public XyPackConfProject() {

        try {
            File file = new File(".");
            codeBaseDir = file.getCanonicalPath();
            System.out.println(file.getCanonicalPath());
        } catch (Exception ex) {

        }
    }

    public String getPrefixJarName() {
        return prefixJarName;
    }

    public XyPackConfProject setPrefixJarName(String prefixJarName) {
        this.prefixJarName = prefixJarName;
        return this;
    }

    public String getOutBaseDir() {
        return outBaseDir;
    }

    public XyPackConfProject setOutBaseDir(String outBaseDir) {
        this.outBaseDir = outBaseDir;
        return this;
    }


    public String getCodeBaseDir() {
        return codeBaseDir;
    }

    public XyPackConfProject setCodeBaseDir(String codeBaseDir) {
        this.codeBaseDir = codeBaseDir;
        return this;
    }

    public XyPackConfProject addModule(XyPackConfModule module) {
        if (module != null) {
            moduleList.add(module);
        }
        return this;
    }

    public List<XyPackConfModule> getModuleList() {
        return moduleList;
    }
}
