package com.x2.modular.common.controller;

import com.x2.core.base.controller.BaseController;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 */
@Controller
@RequestMapping("/system/common")
public class SysCommonController extends BaseController {
    @RequestMapping("/show")
    public String select(HttpServletRequest request) {
        String formUrl = request.getParameter("url");
        return formUrl;
    }
}