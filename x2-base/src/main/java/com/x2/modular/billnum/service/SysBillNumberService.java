package com.x2.modular.billnum.service;

import com.x2.kernel.model.response.ResponseData;
import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.modular.billnum.entity.SysBillNumber;
import com.x2.modular.billnum.model.SysBillNumberVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 编号规则 服务类
 *
 * @author duangy
 * @date 2021-04-15
 */
public interface SysBillNumberService extends IService<SysBillNumber> {

    /**
     * 新增
     *
     * @param param: 参数
     * @return 结果
     */
    ResponseData add(SysBillNumberVo param);

    /**
     * 删除
     *
     * @param param: 参数
     * @return 结果
     */
    ResponseData delete(SysBillNumberVo param);

    /**
     * 更新
     *
     * @param param: 参数
     * @return 结果
     */
    ResponseData update(SysBillNumberVo param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param param: 参数
     * @return result对象
     */
    SysBillNumberVo findBySpec(SysBillNumberVo param);

    /**
     * 查询列表，Specification模式
     *
     * @param param: 参数
     * @return resultList对象
     */
    List<SysBillNumberVo> findListBySpec(SysBillNumberVo param);

    /**
     * 查询分页数据，Specification模式
     *
     * @param param: 参数
     * @return 分页结果
     */
    LayuiPageInfo findPageBySpec(SysBillNumberVo param);

    void deleteOldBillNumber2(SysBillNumberVo paramCondition);

}
