package com.x2.modular.billnum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.x2.modular.billnum.entity.SysBillNumber;
import com.x2.modular.billnum.model.SysBillNumberVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 编号规则 Mapper 接口
 * @author duangy
 * @date 2021-04-15
 */
public interface SysBillNumberMapper extends BaseMapper<SysBillNumber> {

    /**
     * 获取列表
     * @param paramCondition: 参数
     * @return 结果列表
     */
    List<SysBillNumberVo> customList(@Param("paramCondition") SysBillNumberVo paramCondition);

    /**
     * 获取map列表
     * @param paramCondition：参数
     * @return 结果列表
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") SysBillNumberVo paramCondition);

    /**
     * 获取分页实体列表
     * @param page: 分页条件
     * @param paramCondition：查询条件
     * @return 分页结果
     */
    Page<SysBillNumberVo> customPageList(@Param("page") Page page, @Param("paramCondition") SysBillNumberVo paramCondition);

    /**
     * 获取分页map列表
     * @param page: 分页条件
     * @param paramCondition：查询条件
     * @return 分页结果
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") SysBillNumberVo paramCondition);

    void deleteOldBillNumber(@Param("paramCondition") SysBillNumberVo paramCondition);

    void deleteOldBillNumber2(@Param("paramCondition") SysBillNumberVo paramCondition);
}
