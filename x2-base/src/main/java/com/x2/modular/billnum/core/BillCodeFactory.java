package com.x2.modular.billnum.core;

import com.x2.core.util.SpringContextHolder;
import com.x2.modular.billnum.service.SysBillNumberGetService;


/**
 * 编码生成器
 *
 * @author duangy
 * @生成规则 编号类型+8位时间戳+N位流水号
 * Date:20210415
 */
public class BillCodeFactory {

    //单例
    private SysBillNumberGetService billNumberService;

    /**
     * 取得服务
     *
     * @return
     */
    public SysBillNumberGetService getBillNumberService() {
        if (billNumberService == null) {
            billNumberService = SpringContextHolder.getBean(SysBillNumberGetService.class);
        }
        return billNumberService;
    }

    /**
     * 单据号
     *
     * @param numType
     * @return
     */
    public String getBillNum(String numType) {
        return getBillNumberService().billNumber(numType, "yyyyMMdd", 20);
    }

    /**
     * 单据号
     *
     * @param numType
     * @return
     */
    public String billNumberOnMonth(String numType) {
        return getBillNumberService().billNumber(numType, "yyyyMM", 20);
    }


    /**
     * 单据号
     *
     * @param numType
     * @return
     */
    public String billNumberOnMonthOnCache(String numType) {
        return getBillNumberService().billNumber(numType, "yyyyMM", 1);
    }


    /**
     * 单据号
     *
     * @param numType    类型
     * @param dateFormat yyyyMM 或 yyyyMMdd
     * @param batchNum   批次大小 >0
     * @return
     */
    public String getBill(String numType, String dateFormat, int batchNum) {
        return getBillNumberService().billNumber(numType, dateFormat, batchNum);
    }

    /**
     * 单据号
     *
     * @param numType    类型
     * @param dateFormat yyyyMM 或 yyyyMMdd
     * @param batchNum   批次大小 >0
     * @param numDigit 默认流水号位数
     * @return
     */
    public String getBill(String numType, String dateFormat, int batchNum, int numDigit) {
        return getBillNumberService().billNumber(numType, dateFormat, batchNum,numDigit);
    }


    //类初始化时，不初始化这个对象(延时加载，真正用的时候再创建)
    private static BillCodeFactory instance;

    //构造器私有化
    private BillCodeFactory() {
    }

    //方法同步，调用效率低
    public static synchronized BillCodeFactory getInstance() {
        if (instance == null) {
            instance = new BillCodeFactory();
        }
        return instance;
    }

}