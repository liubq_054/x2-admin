package com.x2.modular.billnum.controller;

import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.modular.billnum.entity.SysBillNumber;
import com.x2.modular.billnum.model.SysBillNumberVo;
import com.x2.modular.billnum.service.SysBillNumberService;
import com.x2.core.base.controller.BaseController;
import com.x2.kernel.model.response.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 编号规则控制器
 * @author duangy
 * @date 2021-04-15 10:38:22
 */
@Controller
@RequestMapping("/billnum")
public class SysBillNumberController extends BaseController {
    private String urlPrefix = "/billnum";
    private SysBillNumberService sysBillNumberService;

    @Autowired
    public SysBillNumberController(SysBillNumberService sysBillNumberService) {
        this.sysBillNumberService = sysBillNumberService;
    }

    /**
     * 跳转到主页面
     * @return 页面地址
     */
    @RequestMapping("")
    public String index() {
        return urlPrefix + "/billNumberList.html";
    }

    /**
     * 新增或修改页面
     * @return 页面地址
     */
    @RequestMapping("/form")
    public String form() {
        return urlPrefix + "/billNumberForm.html";
    }

    /**
     * 新增页面
     * @return 页面地址
     */
    @RequestMapping("/add")
    public String add() {
        return urlPrefix + "/billNumberAdd.html";
    }

    /**
     * 编辑页面
     * @return 页面地址
     */
    @RequestMapping("/edit")
    public String edit() {
        return urlPrefix + "/billNumberEdit.html";
    }

    /**
     * 新增接口
     * @return 操作结果
     */
    @RequestMapping("/addItem")
    @ResponseBody
    public ResponseData addItem(SysBillNumberVo param) {
        return this.sysBillNumberService.add(param);
    }

    /**
     * 编辑接口
     * @return 操作结果
     */
    @RequestMapping("/editItem")
    @ResponseBody
    public ResponseData editItem(SysBillNumberVo param) {
        return this.sysBillNumberService.update(param);
    }

    /**
     * 删除接口
     * @return 操作结果
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResponseData delete(SysBillNumberVo param) {
        return this.sysBillNumberService.delete(param);
    }

    /**
     * 查看详情接口
     * @return 详细数据
     */
    @RequestMapping("/detail")
    @ResponseBody
    public ResponseData detail(SysBillNumberVo param) {
        SysBillNumber detail = this.sysBillNumberService.getById(param.getId());
        return ResponseData.success(detail);
    }

    /**
     * 查询列表
     * @return 列表数据
     */
    @ResponseBody
    @RequestMapping("/list")
    public LayuiPageInfo list(SysBillNumberVo param) {
        return this.sysBillNumberService.findPageBySpec(param);
    }
}