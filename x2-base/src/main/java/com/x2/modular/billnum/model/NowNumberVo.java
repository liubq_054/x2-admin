package com.x2.modular.billnum.model;

import lombok.Data;

/**
 * 编号规则
 * @author duangy
 * @date 2021-04-15
 */
@Data
public class NowNumberVo   {
    private Long detailId;
    private String nowDate;
    private Integer numNow;

}