package com.x2.modular.billnum.model;

import com.x2.modular.billnum.entity.SysBillNumber;
import lombok.Data;
import com.x2.kernel.model.validator.BaseValidatingParam;
import java.io.Serializable;

/**
 * 编号规则
 * @author duangy
 * @date 2021-04-15
 */
@Data
public class SysBillNumberVo extends SysBillNumber implements Serializable, BaseValidatingParam {

    private static final long serialVersionUID = 1L;

    @Override
    public String checkParam() {
        return null;
    }
}