package com.x2.modular.billnum.service;

/**
 * 编号规则 服务类
 * @author duangy
 * @date 2021-04-15
 */
public interface SysBillNumberGetService {

    String billNumber(String numType, String scope, int batchNum);

    String billNumber(String numType, String scope, int batchNum,int numDigit );
}
