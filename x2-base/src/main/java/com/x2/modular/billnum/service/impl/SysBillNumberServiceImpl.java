package com.x2.modular.billnum.service.impl;

import com.x2.core.util.ToolUtil;
import com.x2.kernel.model.response.ResponseData;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.x2.base.pojo.page.LayuiPageFactory;
import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.modular.billnum.entity.SysBillNumber;
import com.x2.modular.billnum.mapper.SysBillNumberMapper;
import com.x2.modular.billnum.model.SysBillNumberVo;
import com.x2.modular.billnum.service.SysBillNumberService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * 编号规则 服务实现类
 *
 * @author duangy
 * @date 2021-04-15
 */
@Service
public class SysBillNumberServiceImpl extends ServiceImpl<SysBillNumberMapper, SysBillNumber> implements SysBillNumberService {

    @Override
    public ResponseData add(SysBillNumberVo param) {
        String checkParamResult = param.checkParam();
        if (checkParamResult != null) {
            return ResponseData.error(checkParamResult);
        }
        SysBillNumber entity = getEntity(param);
//        entity.setCreateDept(YxSysContext.getLoginDeptId());
        this.save(entity);
        return ResponseData.success();
    }

    @Override
    public ResponseData delete(SysBillNumberVo param) {
        this.removeById(getKey(param));
        return ResponseData.success();
    }

    @Override
    public ResponseData update(SysBillNumberVo param) {
        String checkParamResult = param.checkParam();
        if (checkParamResult != null) {
            return ResponseData.error(checkParamResult);
        }
        SysBillNumber oldEntity = getOldEntity(param);
        SysBillNumber newEntity = getEntity(param);
        ToolUtil.copyProperties(newEntity, oldEntity);
        this.updateById(newEntity);
        return ResponseData.success();
    }

    @Override
    public SysBillNumberVo findBySpec(SysBillNumberVo param) {
        List<SysBillNumberVo> resultList = findListBySpec(param);
        return resultList != null && resultList.size() > 0 ? resultList.get(0) : null;
    }

    @Override
    public List<SysBillNumberVo> findListBySpec(SysBillNumberVo param) {
        return this.getBaseMapper().customList(param);
    }

    @Override
    public LayuiPageInfo findPageBySpec(SysBillNumberVo param) {
        Page pageContext = getPageContext();
        IPage page = this.baseMapper.customPageList(pageContext, param);
        return LayuiPageFactory.createPageInfo(page);
    }

    private Serializable getKey(SysBillNumberVo param) {
        return param.getId();
    }

    private Page getPageContext() {
        return LayuiPageFactory.defaultPage();
    }

    private SysBillNumber getOldEntity(SysBillNumberVo param) {
        return this.getById(getKey(param));
    }

    private SysBillNumber getEntity(SysBillNumberVo param) {
        SysBillNumber entity = new SysBillNumber();
        ToolUtil.copyProperties(param, entity);
        return entity;
    }


    public void deleteOldBillNumber2(SysBillNumberVo paramCondition){
        baseMapper.deleteOldBillNumber2(paramCondition);
    }
}
