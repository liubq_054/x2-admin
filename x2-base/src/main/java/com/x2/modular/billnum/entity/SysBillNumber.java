package com.x2.modular.billnum.entity;

import java.math.BigDecimal;

import com.x2.base.bean.BaseBean;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;

/**
 * 编号规则
 * @author duangy
 * @since 2021-04-15
 */
@TableName("sys_bill_number")
public class SysBillNumber extends BaseBean {

    private static final long serialVersionUID=1L;

    /**编号名称*/
    @TableField("num_name")
    private String numName;
    /**编号类型*/
    @TableField("num_type")
    private String numType;
    /**编号流水日期*/
    @TableField("num_date")
    private String numDate;
    /**补齐位数*/
    @TableField("num_digit")
    private Integer numDigit;
    /**当前流水号*/
    @TableField("num_now")
    private Integer numNow;
    /**预留时间1*/
    @TableField("reserved_time1")
    private Date reservedTime1;
    /**预留时间2*/
    @TableField("reserved_time2")
    private Date reservedTime2;
    /**预留数字1*/
    @TableField("reserved_num1")
    private BigDecimal reservedNum1;
    /**预留数字2*/
    @TableField("reserved_num2")
    private BigDecimal reservedNum2;
    /**预留字符1*/
    @TableField("reserved1")
    private String reserved1;
    /**预留字符2*/
    @TableField("reserved2")
    private String reserved2;
    /**预留字符3*/
    @TableField("reserved3")
    private String reserved3;
    /**预留字符4*/
    @TableField("reserved4")
    private String reserved4;
    /**预留字符5*/
    @TableField("reserved5")
    private String reserved5;

    public String getNumName() {
        return numName;
    }
    public void setNumName(String numName) {
        this.numName = numName;
    }
    public String getNumType() {
        return numType;
    }
    public void setNumType(String numType) {
        this.numType = numType;
    }
    public String getNumDate() {
        return numDate;
    }
    public void setNumDate(String numDate) {
        this.numDate = numDate;
    }
    public Integer getNumDigit() {
        return numDigit;
    }
    public void setNumDigit(Integer numDigit) {
        this.numDigit = numDigit;
    }
    public Integer getNumNow() {
        return numNow;
    }
    public void setNumNow(Integer numNow) {
        this.numNow = numNow;
    }
    public Date getReservedTime1() {
        return reservedTime1;
    }
    public void setReservedTime1(Date reservedTime1) {
        this.reservedTime1 = reservedTime1;
    }
    public Date getReservedTime2() {
        return reservedTime2;
    }
    public void setReservedTime2(Date reservedTime2) {
        this.reservedTime2 = reservedTime2;
    }
    public BigDecimal getReservedNum1() {
        return reservedNum1;
    }
    public void setReservedNum1(BigDecimal reservedNum1) {
        this.reservedNum1 = reservedNum1;
    }
    public BigDecimal getReservedNum2() {
        return reservedNum2;
    }
    public void setReservedNum2(BigDecimal reservedNum2) {
        this.reservedNum2 = reservedNum2;
    }
    public String getReserved1() {
        return reserved1;
    }
    public void setReserved1(String reserved1) {
        this.reserved1 = reserved1;
    }
    public String getReserved2() {
        return reserved2;
    }
    public void setReserved2(String reserved2) {
        this.reserved2 = reserved2;
    }
    public String getReserved3() {
        return reserved3;
    }
    public void setReserved3(String reserved3) {
        this.reserved3 = reserved3;
    }
    public String getReserved4() {
        return reserved4;
    }
    public void setReserved4(String reserved4) {
        this.reserved4 = reserved4;
    }
    public String getReserved5() {
        return reserved5;
    }
    public void setReserved5(String reserved5) {
        this.reserved5 = reserved5;
    }
}