package com.x2.modular.export.kit;

import cn.hutool.core.util.NumberUtil;
import com.x2.modular.export.util.ExcelBuilder;
import com.x2.modular.export.util.XyExcelUtil;
import com.x2.modular.export.vo.XyExportExcelItem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;

import java.math.BigDecimal;
import java.util.Date;

public class DataUtil {
    /**
     * 取得属性值
     *
     * @param dataItem
     * @param value
     * @return
     */
    public static Object changeValue(XyExportExcelItem dataItem, Object value) {
        if (dataItem == null) {
            return "";
        }
        if (value == null || value.toString().length() == 0) {
            if (dataItem.getDefaultValue() == null || dataItem.getDefaultValue().trim().length() <= 0) {
                return "";
            }
            if (dataItem.getDefaultType() == null) {
                return dataItem.getDefaultValue();
            } else {
                if ("Long".equalsIgnoreCase(dataItem.getDefaultType()) || "Int".equalsIgnoreCase(dataItem.getDefaultType())) {
                    return Long.valueOf(dataItem.getDefaultValue());
                } else if ("Float".equalsIgnoreCase(dataItem.getDefaultType()) || "Float".equalsIgnoreCase(dataItem.getDefaultType()) || "BigDecimal".equalsIgnoreCase(dataItem.getDefaultType())) {
                    return NumberUtil.round(BigDecimal.valueOf(Double.valueOf(dataItem.getDefaultValue().trim())), 2);
                } else {
                    return dataItem.getDefaultValue();
                }
            }
        } else if (value instanceof Date) {
            if (dataItem.getFormat() != null && dataItem.getFormat().trim().length() > 0) {
                return XyExcelUtil.nvlDate((Date) value, dataItem.getFormat());
            } else {
                return XyExcelUtil.nvlDate((Date) value);
            }
        } else if (value instanceof BigDecimal || value instanceof Double || value instanceof Float) {
            if (value instanceof Double) {
                return NumberUtil.round((Double) value, 2);
            } else if (value instanceof Float) {
                return NumberUtil.round(Double.valueOf((Float) value), 2);
            } else {
                return NumberUtil.round((BigDecimal) value, 2);
            }
        } else if (value instanceof Integer || value instanceof Long) {
            return Long.valueOf(value.toString());
        } else {
            return value.toString();
        }
    }


    /**
     * 取得单元格
     *
     * @param dataItem
     * @param cell
     * @param inValue
     * @return
     * @throws Exception
     */
    public static void setCellValue(ExcelBuilder builder, XSSFCell cell, XyExportExcelItem dataItem, Object inValue) throws Exception {
        Object value = changeValue(dataItem, inValue);
        if (value == null || value.toString().length() == 0) {
            cell.setCellValue("");
        } else if (value instanceof BigDecimal) {
            XSSFCellStyle style = cell.getCellStyle();
            style.setDataFormat(builder.getWb().createDataFormat().getFormat("0.00"));
            cell.setCellStyle(style);
            cell.setCellValue(Double.valueOf(((BigDecimal) value).doubleValue()));
        } else if (value instanceof Long) {
            XSSFCellStyle style = cell.getCellStyle();
            style.setDataFormat(builder.getWb().createDataFormat().getFormat("0"));
            cell.setCellStyle(style);
            cell.setCellValue(Long.valueOf(value.toString()));
        } else {
            cell.setCellValue(value.toString());
        }
    }


}
