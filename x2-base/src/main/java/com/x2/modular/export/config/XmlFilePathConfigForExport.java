package com.x2.modular.export.config;

import com.x2.core.util.SpringContextHolder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "export.excel")
public class XmlFilePathConfigForExport {

    public String xmlFilePath;

    public XmlFilePathConfigForExport() {
    }

    public XmlFilePathConfigForExport(String xmlFilePath) {
        this.xmlFilePath = xmlFilePath;
    }

    public String getXmlFilePath() {
        return xmlFilePath;
    }

    public void setXmlFilePath(String xmlFilePath) {
        this.xmlFilePath = xmlFilePath;
    }


    public static XmlFilePathConfigForExport getInstance(){
        return SpringContextHolder.getBean(XmlFilePathConfigForExport.class);
    }

}
