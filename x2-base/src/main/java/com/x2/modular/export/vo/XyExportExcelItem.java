package com.x2.modular.export.vo;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class XyExportExcelItem {

    public static final String SCOPE_MAIN = "main";
    public static final String SCOPE_BODY = "body";
    //pk
    @XmlAttribute
    private String code;
    //前台默认选中
    @XmlAttribute
    private String checked;
    //下面内容需要复制
    @XmlAttribute
    private String name;
    @XmlAttribute
    private String type;

    @XmlAttribute
    private String format;
    @XmlAttribute
    private String align;
    @XmlAttribute
    private Long width;
    @XmlAttribute
    private Integer rowspan = 1;
    @XmlAttribute
    private Integer colspan = 1;
    @XmlAttribute
    private String scope = SCOPE_MAIN;
    @XmlAttribute
    private String defaultValue;
    @XmlAttribute
    private String defaultType;
    public XyExportExcelItem clone() {
        XyExportExcelItem item = new XyExportExcelItem();
        item.setCode(this.getCode());
        item.setName(this.getName());
        item.setType(this.getType());
        item.setFormat(this.getFormat());
        item.setAlign(this.getAlign());
        item.setChecked(this.getChecked());
        item.setWidth(this.getWidth());
        item.setRowspan(this.getRowspan());
        item.setColspan(this.getColspan());
        item.setScope(this.getScope());
        item.setDefaultValue(this.getDefaultValue());
        item.setDefaultType(this.getDefaultType());
        return item;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public Long getWidth() {
        return width;
    }

    public Long getWidthInExcel() {
        if(width>250){
            return 250L/10;
        }
        return width/12;
    }


    public void setWidth(Long width) {
        this.width = width;
    }

    public Integer getRowspan() {
        return rowspan;
    }

    public void setRowspan(Integer rowspan) {
        this.rowspan = rowspan;
    }

    public Integer getColspan() {
        return colspan;
    }

    public void setColspan(Integer colspan) {
        this.colspan = colspan;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultType() {
        return defaultType;
    }

    public void setDefaultType(String defaultType) {
        this.defaultType = defaultType;
    }

    public boolean isMainItem() {
        if (SCOPE_BODY.equalsIgnoreCase(scope)) {
            return false;
        }
        return true;
    }
}
