package com.x2.modular.export.kit;

/**
 * 取值
 */
public interface IFetch {

    /**
     * 取值
     * @param nowData
     * @return
     */
    public Object getValue(Object nowData);
}
