package com.x2.modular.export.action;

import java.lang.annotation.*;

/**
 * 本来不想所有方法都拦截，为了不影响性能，可是具体使用人员总忘记，还是废弃这配置，省的麻烦
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface XyExportExcel {
}