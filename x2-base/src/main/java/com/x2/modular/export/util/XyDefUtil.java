package com.x2.modular.export.util;


import cn.afterturn.easypoi.cache.manager.FileLoaderImpl;
import cn.afterturn.easypoi.cache.manager.IFileLoader;
import com.x2.modular.export.vo.XMLUtil;
import com.x2.modular.export.vo.XyExcelDefVO;

/**
 * 取得定义
 */
public class XyDefUtil {

    /**
     * 读取文件
     */
    private static IFileLoader fileLoader = new FileLoaderImpl();

    /**
     * 读取文件
     *
     * @param subPath /export/excel/purchase
     * @param name    testexport.xml
     * @return
     * @throws Exception
     */
    public static String read(String subPath, String name) throws Exception {
        try {
            String txt = XyResourceFileReader.read2("/excel/" + subPath, name + ".xml");
//            System.out.println("*********************************************");
//            System.out.println(txt);
//            System.out.println("*********************************************");
            return txt;
        } catch (Exception var2) {
            var2.printStackTrace();
            return "";
        }
    }

    /**
     * 取得地址
     *
     * @param subPath
     * @param name
     * @return
     * @throws Exception
     */
    public static XyExcelDefVO getXml(String subPath, String name) throws Exception {
        XyExcelDefVO defVO = (XyExcelDefVO) XMLUtil.convertXmlStrToObject(XyExcelDefVO.class, read(subPath, name));
        if (defVO == null) {
            return null;
        }
        return defVO;
    }
}
