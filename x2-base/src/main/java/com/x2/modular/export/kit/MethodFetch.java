package com.x2.modular.export.kit;

import com.x2.modular.export.util.XyExcelUtil;

import java.lang.reflect.Method;

/**
 * 方法反射取值
 */
public class MethodFetch implements IFetch {
    private Method nowMethod;

    public MethodFetch(Object inData, String key) {
        try {
            Method m = inData.getClass().getMethod(XyExcelUtil.getMethodName(key));
            nowMethod = m;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public Object getValue(Object nowData) {
        if (nowData == null) {
            return "";
        }
        if (nowMethod == null) {
            return "";
        }
        try {
            Object value = nowMethod.invoke(nowData);
            if (value == null) {
                return "";
            }
            return value;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


}
