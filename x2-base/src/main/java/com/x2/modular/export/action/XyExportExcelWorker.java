package com.x2.modular.export.action;

import com.x2.core.util.HttpContext;
import com.x2.modular.export.vo.XyExportVO;

import jakarta.servlet.http.HttpServletRequest;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class XyExportExcelWorker {
    private static ExecutorService service = Executors.newFixedThreadPool(10);

    public static final String export_key = "exportExcelFuture";

    public static void submit(XyExportRun task) {
        Future<XyExportVO> future = service.submit(task);
        HttpServletRequest request = HttpContext.getRequest();
        request.getSession().setAttribute(export_key, future);
    }

    public static boolean isDone(HttpServletRequest request) {
        Future<XyExportVO> future = (Future<XyExportVO>) request.getSession().getAttribute(export_key);
        if (future == null) {
            return true;
        }
        return future.isDone();
    }

    public static XyExportVO getResult(HttpServletRequest request) throws Exception {
        Future<XyExportVO> future = (Future<XyExportVO>) request.getSession().getAttribute(export_key);
        request.getSession().removeAttribute("exportExcelFuture");
        if (future == null) {
            return null;
        }
        return future.get();
    }

}
