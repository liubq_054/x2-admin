package com.x2.modular.export.vo;

import java.util.HashMap;
import java.util.Map;

public class XyExportVO {

    private Map<String,Object> extInfo = new HashMap<>();
    private String module;
    private String fileName;
    private String fileTitle;
    private byte[] fileData;

    private XyExportExcelTitle title;

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public Map<String,Object> getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(Map<String,Object> extInfo) {
        this.extInfo = extInfo;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] data) {
        this.fileData = data;
    }

    public XyExportExcelTitle getTitle() {
        return title;
    }

    public void setTitle(XyExportExcelTitle title) {
        this.title = title;
    }
}
