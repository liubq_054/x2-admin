package com.x2.modular.export.vo;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class XyExportExcelTitle {
    @XmlAttribute
    private Integer titleIndex = 0;
    @XmlAttribute
    private Integer dataIndex = -1;
    @XmlElement(name = "row")
    private List<XyExportExcelRow> rowList = new ArrayList<>();


    public List<XyExportExcelItem> getMainTitleList() {
        List<XyExportExcelItem> itemDisList = new ArrayList<>();
        for (XyExportExcelItem item : rowList.get(this.getTitleIndex()).getItemList()) {
            if (item.isMainItem()) {
                itemDisList.add(item);
            }
        }
        return itemDisList;

    }

    public List<XyExportExcelItem> getBodyTitleList() {
        List<XyExportExcelItem> itemDisList = new ArrayList<>();
        for (XyExportExcelItem item : rowList.get(this.getTitleIndex()).getItemList()) {
            if (!item.isMainItem()) {
                itemDisList.add(item);
            }
        }
        return itemDisList;
    }


    public List<XyExportExcelRow> getRowList() {
        return rowList;
    }

    public void setRowList(List<XyExportExcelRow> rowList) {
        this.rowList = rowList;
    }

    public Integer getTitleIndex() {
        return titleIndex;
    }

    public void setTitleIndex(Integer titleIndex) {
        this.titleIndex = titleIndex;
    }

    public Integer getDataIndex() {
        return dataIndex;
    }

    public void setDataIndex(Integer dataIndex) {
        this.dataIndex = dataIndex;
    }
}
