package com.x2.modular.export.controller;


import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONUtil;
import com.x2.core.base.controller.BaseController;
import com.x2.core.util.ToolUtil;
import com.x2.kernel.model.response.ResponseData;
import com.x2.modular.export.util.XyDefUtil;
import com.x2.modular.export.action.XyExportExcelWorker;
import com.x2.modular.export.vo.XyExcelDefVO;
import com.x2.modular.export.vo.XyExportExcelBody;
import com.x2.modular.export.vo.XyExportVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.io.PrintWriter;

/**
 * 控制器
 *
 * @author
 * @Date 2020-09-07 10:25:00
 */
@Controller
@RequestMapping("/exportExcel")
public class ExportExcelController extends BaseController {

    @RequestMapping("/show")
    public String show() {
        return "export/xyexportBill.html";
    }

    @RequestMapping("/show3")
    public String show3() {
        return "export/xyexportBill3.html";
    }


    @RequestMapping(value = "/downloadExportConfig")
    @ResponseBody
    public ResponseData downloadExportConfig(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String module = request.getParameter("module");
        String fileName = request.getParameter("fileName");
        XyExcelDefVO defVO = XyDefUtil.getXml(module, fileName);
        defVO.setToken(IdUtil.fastUUID());
        XyExcelDefVO defNewVO = new XyExcelDefVO();
        ToolUtil.copyProperties(defVO, defNewVO);
        if (defNewVO.getBody() != null) {
            XyExportExcelBody bodyVO = new XyExportExcelBody();
            ToolUtil.copyProperties(defNewVO.getBody(), bodyVO);
            bodyVO.setQueryClass("");
            defNewVO.setBody(bodyVO);
        }
        return ResponseData.success(defNewVO);
    }


    @RequestMapping(value = "/checkExport")
    @ResponseBody
    public ResponseData checkExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (XyExportExcelWorker.isDone(request)) {
            return ResponseData.success();
        } else {
            return ResponseData.error("");
        }
    }

    /**
     * @return String 跳转的路径
     * @throws Exception
     * @description 已办列表
     * @author
     * @version 2020-07-09
     */
    @RequestMapping(value = "/detailExport")
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            XyExportVO config = XyExportExcelWorker.getResult(request);
            if (config == null) {
                buildResponse(response, "导出异常，请检查");
                return;
            }
            response.reset();
            response.setContentType("application/octet-stream");
            if(config.getFileTitle() == null || config.getFileTitle().trim().length() <= 0){
                config.setFileTitle(System.currentTimeMillis()+"");
            }
            response.setHeader("Content-Disposition", "attachment;filename=" + new String(config.getFileTitle().getBytes(), "ISO8859-1"));
            OutputStream fileOut = response.getOutputStream();
            fileOut.write(config.getFileData());
            fileOut.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
            buildResponse(response, "导出异常，" + ex.getMessage());
            return;
        }
    }

    /**
     * 错误返回结果
     *
     * @param response
     * @param s
     * @throws IOException
     */
    private void buildResponse(HttpServletResponse response, String s) throws IOException {
        Map<String, String> resMap = new HashMap<>();
        resMap.put("code", "405");
        resMap.put("message", s);
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=utf-8");
        PrintWriter writer = response.getWriter();
        writer.write(JSONUtil.toJsonStr(resMap));
        writer.flush();
        writer.close();
    }
}

