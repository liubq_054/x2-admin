package com.x2.modular.export.vo;

import jakarta.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class XyExportExcelRow {

    @XmlElement(name = "item")
    private List<XyExportExcelItem> itemList;

    public List<XyExportExcelItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<XyExportExcelItem> itemList) {
        this.itemList = itemList;
    }

    public XyExportExcelRow clone(){
        XyExportExcelRow row = new XyExportExcelRow();
        row.setItemList(itemList);
        return row;

    }
}
