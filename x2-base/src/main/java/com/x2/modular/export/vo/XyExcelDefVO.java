package com.x2.modular.export.vo;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
// XML文件中的根标识
@XmlRootElement(name = "Excel")
public class XyExcelDefVO {
    private String token;
    private String fileTitle;
    private String headPk = "id";
    private String maxNum = "10000";
    private String showSeq = "Y";
    private byte[] fileByteData;

    private XyExportExcelTitle title;

    private XyExportExcelBody body;

    //前台选择列表
    private List<String> mainItems = new ArrayList<>();

    private List<String> bodyItems = new ArrayList<>();

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public String getMaxNum() {
        return maxNum;
    }

    public void setMaxNum(String maxNum) {
        this.maxNum = maxNum;
    }

    public XyExportExcelTitle getTitle() {
        return title;
    }

    public void setTitle(XyExportExcelTitle title) {
        this.title = title;
    }


    public XyExportExcelBody getBody() {
        return body;
    }

    public void setBody(XyExportExcelBody body) {
        this.body = body;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public byte[] getFileByteData() {
        return fileByteData;
    }

    public void setFileByteData(byte[] fileByteData) {
        this.fileByteData = fileByteData;
    }

    public String getHeadPk() {
        return headPk;
    }

    public void setHeadPk(String headPk) {
        this.headPk = headPk;
    }


    public void setShowSeq(String showSeq) {
        this.showSeq = showSeq;
    }


    public List<String> getMainItems() {
        return mainItems;
    }

    public void setMainItems(List<String> mainItems) {
        this.mainItems = mainItems;
    }


    public List<String> getBodyItems() {
        return bodyItems;
    }

    public void setBodyItems(List<String> bodyItems) {
        this.bodyItems = bodyItems;
    }

}
