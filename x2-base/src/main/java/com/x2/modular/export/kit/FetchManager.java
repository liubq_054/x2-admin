package com.x2.modular.export.kit;

import java.util.Map;

/**
 * 取得数据管理类
 */
public class FetchManager {
    //类型
    private int type = 0;
    //对象
    private Object itmeObj;

    /**
     * 构造
     * @param obj
     */
    public FetchManager(Object obj) {
        itmeObj = obj;
        if (obj instanceof Map) {
            type = 1;
        }
    }

    /**
     * 取值方法
     * @param key
     * @return
     */
    public IFetch method(String key) {
        if (type == 1) {
            return new MapFetch(key);
        }
        return new MethodFetch(itmeObj, key);
    }

}
