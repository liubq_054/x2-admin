package com.x2.modular.export.util;

import cn.hutool.core.io.IORuntimeException;
import org.apache.poi.util.IOUtils;

import java.io.*;

/**
 * 资源文件读取
 */
public class XyResourceFileReader {


    /**
     * 读取文件
     *
     * @param path /export/excel/purchase
     * @param name testexport.xml
     * @return
     * @throws Exception
     */
    public static String read(String path, String name) throws Exception {

        //"/export/excel/purchase/testexport.xml"
        String filePath = path.trim();
        filePath = filePath.replace("\\", "/");
        /*if (!filePath.startsWith("/")) {
            filePath = "/" + filePath;
        }*/
        if (filePath.endsWith("/")) {
            filePath = filePath + name;
        } else {
            filePath = filePath + "/" + name;
        }
        System.out.println("读取文件：" + filePath);
       /* File file = new File(filePath);
        FileInputStream fileis = new FileInputStream(file);*/
//        InputStream in = XyResourceFileReader.class.getResourceAsStream(filePath);
        ByteArrayInputStream byteArray = new ByteArrayInputStream(getFile(filePath));


        Reader f = new InputStreamReader(byteArray);
        BufferedReader fb = new BufferedReader(f);
        StringBuffer sb = new StringBuffer("");
        String s = "";
        while ((s = fb.readLine()) != null) {
            sb = sb.append(s);
        }
        return sb.toString();
    }

    public static byte[] getFile(String url) {
        FileInputStream fileis = null;
        ByteArrayOutputStream baos = null;


        try {
            File file = new File(url);
            fileis = new FileInputStream(file);
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fileis.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();
            byte[] var6 = baos.toByteArray();
            return var6;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            IOUtils.closeQuietly(fileis);
            IOUtils.closeQuietly(baos);
        }
    }

    /**
     * 读取文件
     *
     * @param path /export/excel/purchase
     * @param name testexport.xml
     * @return
     * @throws Exception
     */
    public static String read2(String path, String name) throws Exception {
        //"/export/excel/purchase/testexport.xml"
        String filePath = path.trim();
        filePath = filePath.replace("\\", "/");
        /*if (!filePath.startsWith("/")) {
            filePath = "/" + filePath;
        }*/
        if (filePath.endsWith("/")) {
            filePath = filePath + name;
        } else {
            filePath = filePath + "/" + name;
        }
        if(!filePath.startsWith("/")){
            filePath = "/"+filePath;
        }
        System.out.println("*********************读取Xml文件：********************************" + filePath);
        InputStream stream = null;
        try {
//            System.out.println("begin");
            stream = XyResourceFileReader.class.getResourceAsStream(filePath);
            ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len = -1;
            while ((len = stream.read(buffer)) != -1) {
                outSteam.write(buffer, 0, len);
            }
            outSteam.close();
            stream.close();
            String txt =  new String(outSteam.toByteArray(),"UTF-8");
            return txt;
        } catch (Exception var10) {
            throw new IORuntimeException(var10);
        } finally {
            if (stream != null) {
                IOUtils.closeQuietly(stream);
            }
        }
    }


}
