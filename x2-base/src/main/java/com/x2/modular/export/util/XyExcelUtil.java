package com.x2.modular.export.util;

import com.x2.core.util.DateUtils;
import com.x2.core.util.NumberUtil;

import java.math.BigDecimal;
import java.util.Date;

public class XyExcelUtil {


    /**
     * 取得方法名称
     *
     * @param itemName
     * @return
     */
    public static String getMethodName(String itemName) {
        StringBuilder s = new StringBuilder();
        s.append("get").append(itemName.substring(0, 1).toUpperCase());
        if (itemName.length() > 1) {
            s.append(itemName.substring(1));
        }
        return s.toString();
    }

    public static String nvl(Object value) {
        if (value == null) {
            return "";
        }
        return value.toString();
    }

    public static String nvlDate(Date value) {
        if (value == null) {
            return "";
        }
        return DateUtils.formatDate(value);
    }

    public static String nvlDate(Date value, String format) {
        if (value == null) {
            return "";
        }
        return DateUtils.formatDate(value, format);
    }

    public static String nvlNum2(BigDecimal value) {
        if (value == null) {
            return "";
        }
        return NumberUtil.round(value, 2).toString();
    }


    public static String nvlDateTime(Date value) {
        if (value == null) {
            return "";
        }
        return DateUtils.formatDate(value, "yyyy-MM-dd HH:mm:ss");
    }
}
