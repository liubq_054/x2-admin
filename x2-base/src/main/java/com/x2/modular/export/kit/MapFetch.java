package com.x2.modular.export.kit;

import java.util.Map;

/**
 * map取值
 */
public class MapFetch implements IFetch {

    private String key;

    public MapFetch(String key) {
        this.key = key;
    }

    public Object getValue(Object nowData) {
        if (nowData == null) {
            return "";
        }
        Map data = (Map) nowData;
        Object value = data.get(key);
        if (value == null) {
            return "";
        }
        return value;
    }

}
