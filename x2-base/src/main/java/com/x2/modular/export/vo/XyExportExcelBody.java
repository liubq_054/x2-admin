package com.x2.modular.export.vo;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class XyExportExcelBody {
    @XmlAttribute
    private String queryClass;
    @XmlAttribute
    private String methodName;
    @XmlAttribute
    private String bodyFk = "headId";
    @XmlAttribute
    private String mergeMain = "Y";


    public String getQueryClass() {
        return queryClass;
    }

    public void setQueryClass(String queryClass) {
        this.queryClass = queryClass;
    }

    public String getBodyFk() {
        return bodyFk;
    }

    public void setBodyFk(String bodyFk) {
        this.bodyFk = bodyFk;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }


    public String getMergeMain() {
        return mergeMain;
    }

    public void setMergeMain(String mergeMain) {
        this.mergeMain = mergeMain;
    }

}
