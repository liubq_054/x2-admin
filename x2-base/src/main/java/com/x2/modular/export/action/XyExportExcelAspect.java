package com.x2.modular.export.action;


import cn.hutool.json.JSONUtil;
import com.x2.core.util.HttpContext;
import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.core.util.EmptyUtil;
import com.x2.modular.export.vo.XyExcelDefVO;
import com.x2.modular.export.vo.XyExportVO;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;

@Aspect
@Component
public class XyExportExcelAspect {
    private static Logger log = LoggerFactory.getLogger(XyExportExcelAspect.class);

    public static final String TYPE_0 = "0";
    public static final String TYPE_1 = "1";
    public static final String TYPE_2 = "2";
    public static final String TYPE_3 = "3";
    private static final String EXPORT_PARA = "xyExportExcel20161012Info";

    @Pointcut(value = "@annotation(com.x2.modular.export.action.XyExportExcel)")
    public void pageQuery() {
    }


    @Around("pageQuery()")
    public Object Interceptor(ProceedingJoinPoint pjp) throws Throwable {
        HttpServletRequest request = HttpContext.getRequest();
        String exportParam = request.getParameter(EXPORT_PARA);
        //调用目标方法，获取目标方法返回值
        Object returnValue = pjp.proceed();
        //非导出调用，直接返回
        if (exportParam == null || exportParam.length() == 0 || exportParam.trim().length() <= 10) {
            return returnValue;
        }
        //导出
        return afterQueryToExport(returnValue, exportParam);
    }

    /**
     * 导出
     *
     * @param returnValue
     * @param exportParam
     * @return
     */
    private Object afterQueryToExport(Object returnValue, String exportParam) {
        try {
            if (returnValue == null) {
                LayuiPageInfo returnInfo = new LayuiPageInfo();
                returnInfo.setCode(500);
                returnInfo.setMsg("导出数据为空，请检查!");
                return returnInfo;
            }
            if (!(returnValue instanceof LayuiPageInfo)) {
                LayuiPageInfo returnInfo = new LayuiPageInfo();
                returnInfo.setCode(500);
                returnInfo.setMsg("导出列表方法的返回值不是对象(LayuiPageInfo)，暂时支持！");
                return returnInfo;
            }
            LayuiPageInfo returnPage = (LayuiPageInfo) returnValue;
            if (returnPage.getData() == null || returnPage.getData().size() == 0) {
                LayuiPageInfo returnInfo = new LayuiPageInfo();
                returnInfo.setCode(500);
                returnInfo.setMsg("导出数据为空，请检查!");
                return returnInfo;
            }
            String type = exportParam.substring(0, 1);
            String actParam = exportParam.substring(2);
            //支持EasyPOI
            if (TYPE_3.equalsIgnoreCase(type)) {
                //上传的导出信息
                XyExportVO exportVO = toBean(actParam, XyExportVO.class);
                //方式选择
                XyExportExcelWorker.submit(new XyExportRun3((LayuiPageInfo) returnValue, exportVO));
            } else {
                //上传的导出信息
                XyExcelDefVO config = toBean(actParam, XyExcelDefVO.class);
                //待删除
                if (EmptyUtil.empty(config.getMainItems()) && EmptyUtil.empty(config.getBodyItems())) {
                    throw new Exception("没有选择导出列！");
                }
                XyExportExcelWorker.submit(new XyExportRun0((LayuiPageInfo) returnValue, config));
            }
            //返回空数据
            LayuiPageInfo returnInfo = new LayuiPageInfo();
            returnInfo.setCode(0);
            return returnInfo;
        } catch (Throwable e) {
            log.error("出现异常" + e);
            LayuiPageInfo returnInfo = new LayuiPageInfo();
            returnInfo.setCode(500);
            returnInfo.setMsg(e.getMessage());
            return returnInfo;
        }
    }

    /**
     * 转换为对象
     *
     * @param json
     * @param clasz
     * @param <T>
     * @return
     * @throws Exception
     */
    private <T> T toBean(String json, Class<T> clasz) throws Exception {
        T config;
        try {
            config = JSONUtil.toBean(json, clasz);
            if (config == null) {
                throw new Exception("上传配置信息Json化后不正确！");
            }
            return config;
        } catch (Exception ex) {
            throw new Exception("上传配置信息Json化后不正确！");
        }
    }
}
