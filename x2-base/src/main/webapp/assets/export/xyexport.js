var xyexport = {};
xyexport.$;
xyexport.$ax;
xyexport.tableObj;
xyexport.defVO;
xyexport.fileTitle;
xyexport.exportConfig;
xyexport.sendTitleObj;
xyexport.type;
xyexport.headPk;
/**
 * 后台Excel文件方式
 * @param layui
 * @param tableObj
 * @param config
 */
xyexport.do3 = function (layui, tableObj, exportConfig, fileName) {
    xyexport.$ = layui.$;
    xyexport.$ax = layui.ax;
    xyexport.tableObj = tableObj;
    xyexport.exportConfig = exportConfig;
    xyexport.fileTitle = fileName;
    xyexport.type = 3;
    //构造选择列
    var allCols = tableObj.config.cols;
    if (exportConfig && exportConfig.cols) {
        allCols = exportConfig.cols;
    }

    //构建行
    var rowList = xyexport.buildRowList(allCols);
    xyexport.sendTitleObj = {"rowList": rowList};
    xyexport.openSelectDlg3();
};

/**
 * 后台配置文件方式
 * @param layui
 * @param tableObj
 * @param config
 */
xyexport.do2 = function (layui, tableObj, exportConfig, fileName) {
    xyexport.$ = layui.$;
    xyexport.$ax = layui.ax;
    xyexport.tableObj = tableObj;
    xyexport.exportConfig = exportConfig;
    xyexport.fileTitle = fileName;
    xyexport.type = 2;
    var defVO = xyexport.downloadConfig(exportConfig);
    if (!defVO) {
        Feng.alert("没有导出配置信息，请检查！");
        return;
    }
    //弹出框
    xyexport.openSelectDlg(defVO);
};

/**
 * 前台表单方式
 * @param layui
 * @param tableObj
 * @param exportConfig
 */
xyexport.do = function (layui, tableObj, exportConfig, fileName) {
    xyexport.$ = layui.$;
    xyexport.$ax = layui.ax;
    xyexport.tableObj = tableObj;
    xyexport.exportConfig = exportConfig;
    xyexport.fileTitle = fileName;
    xyexport.type = 1;
    if (!tableObj || !tableObj.config) {
        Feng.alert("没有传入table对象");
        return;
    }
    if (!tableObj.config.queryParams) {
        Feng.alert("没有配置取得查询条件方法（queryParams）");
        return;
    }
    //构造选择列
    var allCols = tableObj.config.cols;
    var titleIndex = 0;
    if (exportConfig ) {
        if (exportConfig.headPk) {
            xyexport.headPk = exportConfig.headPk;
        }
        if (exportConfig.cols) {
            allCols = exportConfig.cols;
        }
        if (exportConfig.titleIndex) {
            titleIndex = exportConfig.titleIndex;
        }
    }
    //构建行
    var rowList = xyexport.buildRowList(allCols);
    xyexport.sendTitleObj = {"rowList": rowList};
    var defVO = {};
    defVO['fileName'] = "" + new Date().getTime() + ".xlsx";
    defVO['mainItemList'] = rowList[titleIndex].itemList;
    //弹出框
    xyexport.openSelectDlg(defVO);
};
//构建行列表
xyexport.buildRowList = function (allCols) {
    //构造参数
    var rowList = [];
    var nowItem;
    for (var idx = 0; idx < allCols.length; idx++) {
        var nowCols = allCols[idx];
        var newCols = [];
        for (var idx1 = 0; idx1 < nowCols.length; idx1++) {
            nowItem = nowCols[idx1];
            if (nowItem.hide) {
                continue;
            }
            if (!nowItem.title) {
                continue;
            }
            if (!nowItem.field) {
                continue;
            }
            var itemObj = {};
            itemObj['code'] = nowItem.field;
            itemObj['name'] = nowItem.title;
            if (nowItem.checked) {
                itemObj['checked'] = nowItem.checked;
            } else {
                itemObj['checked'] = "Y";
            }
            if (nowItem.format) {
                itemObj['format'] = nowItem.format;
            }
            if (nowItem.width) {
                itemObj['width'] = nowItem.width;
            }
            if (nowItem.rowspan) {
                itemObj['rowspan'] = nowItem.rowspan;
            }
            if (nowItem.colspan) {
                itemObj['colspan'] = nowItem.colspan;
            }
            newCols.push(itemObj);
        }
        rowList.push({"itemList": newCols})
    }
    return rowList;

};
//导出数据
xyexport.action = function (param) {
    Feng.closeLoading();
    Feng.loading();
    var tableObj = xyexport.tableObj;
    var url = tableObj.config.url;

    var cond = tableObj.config.queryParams();
    if (!cond) {
        cond = {};
    }
    if (xyexport.exportConfig) {
        for (var name in xyexport.exportConfig) {
            param[name] = xyexport.exportConfig[name];
        }
    }
    if (xyexport.fileTitle) {
        param['fileTitle'] = xyexport.fileTitle;
    } else {
        param['fileTitle'] = new Date().getTime() + ".xlsx";
    }
    if (param['dataScope'] != 'allData') {
        var curr = tableObj.config.page.curr;
        var limit = tableObj.config.page.limit;
        cond['page'] = curr;
        cond['limit'] = limit;
    } else {
        cond['page'] = 1;
        cond['limit'] = 10000;
    }
    if (xyexport.sendTitleObj) {
        param['title'] = xyexport.sendTitleObj;
    }
    if (xyexport.headPk) {
        param['headPk'] = xyexport.headPk;
    }
    cond['xyExportExcel20161012Info'] = xyexport.type + "=" + JSON.stringify(param);
    //获取详情信息，填充表单
    var ajax1 = new xyexport.$ax(url);
    ajax1.set(cond);
    var result1 = ajax1.start();
    if (result1.code != '0') {
        Feng.closeLoading();
        if (result1.msg) {
            Feng.alert(result1.msg);
        } else {
            Feng.alert("导出数据为空，请检查！");
        }
        return;
    }
    //启动定时器，定时检查并下载
    //Feng.closeLoading();
    xyexport.checkAndDownFile();
};
//是否处理完成
xyexport.checkAndDownFile = function () {
    var ajax1 = new xyexport.$ax(Feng.ctxPath + "/exportExcel/checkExport");
    var result1 = ajax1.start();
    if (result1.success) {
        window.setTimeout(function () {
            Feng.closeLoading();
        }, 3000);
        xyexport.getFile();
    } else {
        window.setTimeout(xyexport.checkAndDownFile, 1000);
    }
};
//取得文件
xyexport.getFile = function () {
    window.open(Feng.ctxPath + "/exportExcel/detailExport");
};

//下载配置文件
xyexport.downloadConfig = function (config) {
    var downloadUrl = Feng.ctxPath + "/exportExcel/downloadExportConfig?fileName=" + config.fileName + "&module=" + config.module + "&n_=" + new Date().getTime();
    downloadUrl = encodeURI(downloadUrl);
    var ajax1 = new xyexport.$ax(downloadUrl);
    var result = ajax1.start();
    var defVO = result.data;
    if (defVO && defVO.title) {
        defVO['mainItemList'] = defVO.title.mainTitleList;
        if (defVO.body) {
            if (!defVO.body['bodyItemList']) {
                defVO.body['bodyItemList'] = defVO.title.bodyTitleList;
            }
        }
        return defVO;
    } else {
        return null;
    }
};
//弹出选中对话话框
xyexport.openSelectDlg = function (defVO) {
    xyexport.defVO = defVO;

    layer.open({
        type: 2, title: "导出选项",
        maxmin: false,
        move: false,
        shadeClose: true,
        skin: 'layui-layer-admin',
        area: ["900px", "460px"],
        closeBtn: 1,
        btn: ['导出', '取消'],
        content: Feng.ctxPath + "/exportExcel/show",
        success: function (layero, index) {
            var myIframe = window[layero.find('iframe')[0]['name']];
            myIframe.selectModule.fillPage(defVO);
        },
        yes: function (index, layero) {
            var myIframe = window[layero.find('iframe')[0]['name']];
            var selectVO = myIframe.selectModule.getSelectVO(defVO);
            if (selectVO) {
                if (selectVO.mainItems.length == 0) {
                    if (selectVO.bodyItems.length == 0) {
                        Feng.alert("请选择导出属性");
                        return;
                    }
                }
                //导出
                xyexport.action(selectVO);
                layer.close(index);
            }
        }
    });
};

//弹出选中对话话框
xyexport.openSelectDlg3 = function () {
    layer.open({
        type: 2, title: "导出选项",
        maxmin: false,
        move: false,
        shadeClose: true,
        skin: 'layui-layer-admin',
        area: ["300px", "300px"],
        closeBtn: 1,
        btn: ['导出', '取消'],
        content: Feng.ctxPath + "/exportExcel/show3",
        success: function (layero, index) {
            var myIframe = window[layero.find('iframe')[0]['name']];
            myIframe.selectModule.fillPage();
        },
        yes: function (index, layero) {
            var myIframe = window[layero.find('iframe')[0]['name']];
            var selectVO = myIframe.selectModule.getSelectVO({});
            //导出
            xyexport.action(selectVO);
            layer.close(index);
        }
    });
};

