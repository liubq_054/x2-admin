var selectModule = {};
selectModule.$;
selectModule.form;
selectModule.fillPage = function (defVO) {
    layui.use(['form', 'admin'], function () {
        selectModule.$ = layui.$;
        selectModule.form = layui.form;
        selectModule.form .render();
    });
};

//取得对象
selectModule.getSelectVO = function () {
    var $ = selectModule.$;
    var dataScope = "nowPage";
    //数据范围
    $("input[groupname='exportDataScope']").each(function (scopeIndex, scopeObj) {
        if ($(scopeObj).prop("checked")) {
            dataScope = $(scopeObj).attr("value");
        }
    });
    var param = {};
    param['dataScope'] = dataScope;
    param['token'] = "token";
    return param;
};