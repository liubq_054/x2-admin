var selectModule = {};
selectModule.$;
selectModule.form;
selectModule.selectAllItem = function () {
    selectModule.selectOnchange(true);
};
selectModule.unselectAllItem = function () {
    selectModule.selectOnchange(false);
};
selectModule.selectOnchange = function (checked) {
    selectModule.$("#itemListDiv").find("input[type='checkbox']").each(function (itemIndex, itemObj) {
        if (checked) {
            selectModule.$(itemObj).prop("checked", true);
        } else {
            selectModule.$(itemObj).prop("checked", false);
        }
    });
    selectModule.form .render();
};
selectModule.fillPage = function (defVO) {
    layui.use(['form', 'admin'], function () {
        selectModule.$ = layui.$;
        selectModule.form = layui.form;
        //从文档上复制的好像没有这句
        selectModule.fillPageDoAction(defVO);
        selectModule.form .render();
    });
};
selectModule.fillPageDoAction = function (defVO) {
    var $ = selectModule.$
    var html = "";
    html += '';
    var mainItem;
    //表头
    for (var mainIndex = 0; mainIndex < defVO.mainItemList.length; mainIndex++) {
        mainItem = defVO.mainItemList[mainIndex];
        html += selectModule.itemHtml(mainItem, false) + "&nbsp;"
    }
    //表体
    if (defVO.body && defVO.body.bodyItemList) {
        var bodyItem;
        for (var bodyIndex = 0; bodyIndex < defVO.body.bodyItemList.length; bodyIndex++) {
            bodyItem = defVO.body.bodyItemList[bodyIndex];
            html += selectModule.itemHtml(bodyItem, true) + "&nbsp;"
        }
    }
    $("#itemListDiv").html(html);
};
//取得对象
selectModule.itemHtml = function (mainItem, isBody) {
    var html = '<';
    html += 'input type="checkbox" title="' + mainItem.name + '" ';
    html += ' lay-skin="primary" exportCode="' + mainItem.code + '"';
    if ("Y" == mainItem.checked || "y" == mainItem.checked) {
        html += ' class="xy-item-checkbox" checked';
    } else {
        html += ' class="xy-item-checkbox" ';
    }
    if (isBody) {
        html += ' itemScope="body" />';
    } else {
        html += ' itemScope="main" />';
    }
    return html;
};
//取得对象
selectModule.getSelectVO = function (defVO) {
    var $ = selectModule.$;
    var dataScope = "nowPage";
    //数据范围
    $("input[groupname='exportDataScope']").each(function (scopeIndex, scopeObj) {
        if ($(scopeObj).prop("checked")) {
            dataScope = $(scopeObj).attr("value");
        }
    });
    var newMainItemList = [];
    var newBodyItemList = [];

    selectModule.$("#itemListDiv").find("input[type='checkbox']").each(function (itemIndex, itemObj) {

        if ($(itemObj).prop("checked")) {
            var itemScope = $(itemObj).attr("itemScope");
            var code = $(itemObj).attr("exportCode");
            if(itemScope == 'body'){
                newBodyItemList.push(code)
            } else {
                newMainItemList.push(code)
            }
        }
    });
    var param = {};
    param['dataScope'] = dataScope;
    if (newMainItemList.length > 0) {
        param['mainItems'] = newMainItemList;
    } else {
        param['mainItems'] = "";
    }
    if (newBodyItemList.length > 0) {
        param['bodyItems'] = newBodyItemList;
    } else {
        param['bodyItems'] = [];
    }
    if (defVO.token) {
        param['token'] = defVO.token;
    } else {
        param['token'] = "token";
    }
    return param;
};