layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;

    /**编号规则管理*/
    var SysBillNumber = {
        table: null,
        tableId: "sysBillNumberTable",
        tableUrl: Feng.ctxPath + '/billnum/list',
        condition: ['condition']
    };

    /**初始化表格的列*/
    SysBillNumber.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'id', hide: true, title: '主键'},
            {field: 'numName', sort: true, title: '编号名称'},
            {field: 'numType', sort: true, title: '编号类型'},
            {field: 'numDate', sort: true, title: '编号流水日期'},
            {field: 'numDigit', sort: true, title: '补齐位数'},
            {field: 'numNow', sort: true, title: '当前流水号'},
            {align: 'center', toolbar: '#tableBar', title: '操作'}
        ]];
    };

    /**生成查询条件*/
    SysBillNumber.queryParams = function() {
        var queryData = {n: new Date().getTime()};
        var numName = $("#numName").val();
        if (numName.length > 0) {
            queryData['numName'] = numName;
        }
        return queryData;
    };

    /**点击查询按钮*/
    SysBillNumber.search = function () {
        table.reload(SysBillNumber.tableId, {page: {curr: 1}});
    };

    // 渲染表格
    SysBillNumber.table = table.render({
        elem: '#' + SysBillNumber.tableId,
        url: SysBillNumber.tableUrl,
        queryParams: SysBillNumber.queryParams,
        page: true,
        height: "full-98",
        limit: Feng.pageLimit(),
        cellMinWidth: 100,
        cols: SysBillNumber.initColumn()
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        SysBillNumber.search();
    });

    // 重置
    $('#btnReset').click(function () {
        $("#numName").val("");
    });

    // 导出excel
    $('#btnExp').click(function () {
        var checkRows = table.checkStatus(SysBillNumber.tableId);
        if (checkRows.data.length === 0) {
            Feng.error("请选择要导出的数据");
        } else {
            table.exportFile(tableResult.config.id, checkRows.data, 'xls');
        }
    });

    /**新增或修改*/
    SysBillNumber.openFormDlg = function (data) {
        var url = Feng.ctxPath + '/billnum/form';
        var title = '添加编号规则';
        if (data) {
            url += '?id=' + data.id;
            title = '修改编号规则';
        }
        func.open({title: title, content: url, tableId: SysBillNumber.tableId});
    };

    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        SysBillNumber.openFormDlg();
    });

    // 工具条点击事件
    table.on('tool(' + SysBillNumber.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'edit') {
            SysBillNumber.openFormDlg(data);
        } else if (layEvent === 'delete') {
            Feng.confirm("是否删除?", function () {
                var ajax = new $ax(Feng.ctxPath + "/billnum/delete", function (result) {
                    if (result.code != 200) {
                        Feng.error(result.message);
                        return;
                    }
                    Feng.success("删除成功!");
                    table.reload(SysBillNumber.tableId);
                }, function (data) { Feng.error("删除失败!" + data.responseJSON.message + "!"); });
                ajax.set("id", data.id);
                ajax.start();
            });
        }
    });
});
