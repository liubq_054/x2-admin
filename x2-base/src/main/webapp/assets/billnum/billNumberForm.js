layui.use(['form', 'admin', 'ax', 'laydate'], function () {
    var $ = layui.jquery;
    var $ax = layui.ax;
    var form = layui.form;
    var admin = layui.admin;
    var laydate = layui.laydate;

    laydate.render({elem: '#numDate',format:'yyyyMMdd'});

    var SysBillNumberFormModule = {
        formId: 'sysBillNumberForm',
        id: Feng.getUrlParam("id"),
        detailUrl: Feng.ctxPath + "/billnum/detail",
        formUrl: Feng.ctxPath + "/billnum/addItem"
    };

    /**初始化方法*/
    SysBillNumberFormModule.init = function() {
        if (!SysBillNumberFormModule.isAdd()) {
            SysBillNumberFormModule.get();
        }
    };

    /**判断是否为新增*/
    SysBillNumberFormModule.isAdd = function() {
        var isAdd = Feng.common.checkNull(SysBillNumberFormModule.id);
        if (!isAdd) {
            SysBillNumberFormModule.formUrl = Feng.ctxPath + "/billnum/editItem";
        }
        return isAdd;
    };

    /**获取详情信息，填充表单*/
    SysBillNumberFormModule.get = function() {
        var ajax = new $ax(SysBillNumberFormModule.detailUrl + "?id=" + SysBillNumberFormModule.id);
        var result = ajax.start();
        form.val(SysBillNumberFormModule.formId, result.data);
    };

    //表单提交事件
    form.on('submit(btnSubmit)', function (data) {
        var ajax = new $ax(SysBillNumberFormModule.formUrl, function (result) {
            if (result.code != 200) {
                Feng.error(result.message);
                return;
            }
            Feng.success("更新成功！");
            admin.putTempData('formOk', true);
            admin.closeThisDialog();
        }, function (data) { Feng.error("更新失败！" + data.responseJSON.message) });
        ajax.set(data.field);
        ajax.start();
        return false;
    });

    SysBillNumberFormModule.init();
});