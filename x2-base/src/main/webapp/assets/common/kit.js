//工具类，不涉及到后台**********************************************************************************************************************
//工具类，不涉及到后台
var kitFun = {};
//判空
kitFun.isNull = function (value) {
    if (typeof (value) === "undefined" || typeof (value) == undefined ) {
        return true;
    }
    if (value === undefined || value === null || value === '') {
        return true;
    }
    return false;
};
//转换未浮点数
kitFun.toFloat = function (inValue, inDefaultValue) {
    if (inValue && kitFun.isNumber(inValue)) {
        return parseFloat(inValue);
    }
    return inDefaultValue;
};
//转换未整数
kitFun.toInt = function (inValue, inDefaultValue) {
    if (inValue && kitFun.isNumber(inValue)) {
        return parseInt(inValue);
    }
    return inDefaultValue;
};
//固定长度
kitFun.toFixed = function (inValue, len) {
    return parseFloat(inValue).toFixed(len);
};
//固定长度
kitFun.toFix = function (inValue) {
    return parseFloat(inValue).toFixed(2);
};
//固定长度
kitFun.toFix2 = function (inValue) {
    return kitFun.toFloat(inValue, 0).toFixed(2);
};
//是不是数字
kitFun.isNumber = function (inValue) {
    var reg = new RegExp(/(-?\d+)(\.\d+)?$/);
    return reg.test(inValue);
};
//浮点数相加
kitFun.toFloatAdd = function () {
    var len=arguments.length;
    var sum = 0;
    for (var idx = 0; idx < len; idx++) {
        sum += kitFun.toFloat(arguments[idx], 0);
    }
    return kitFun.toFix2(sum);
};
//当前时间字符串
kitFun.getNowDate = function () {
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    return year + seperator1 + month + seperator1 + strDate;
};

//当前时间字符串
kitFun.getNowTime = function () {
    var date = new Date();
    var seperator1 = "-";
    var seperator2 = ":";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();

    var hour = date.getHours()
    var minute = date.getMinutes()
    var second = date.getSeconds()

    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    if (hour >= 0 && hour <= 9) {
        hour = "0" + hour;
    }
    if (minute >= 0 && minute <= 9) {
        minute = "0" + minute;
    }
    if (second >= 0 && second <= 9) {
        second = "0" + second;
    }
    var nowStr =  year + seperator1 + month + seperator1 + strDate;
    nowStr +=" "+ hour + seperator2 + minute + seperator2 + second;
    return nowStr;
};

//当前时间字符串 yyyy-MM-dd HH:mm
kitFun.getNowTime1 = function () {
    var date = new Date();
    var seperator1 = "-";
    var seperator2 = ":";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();

    var hour = date.getHours()
    var minute = date.getMinutes()

    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    if (hour >= 0 && hour <= 9) {
        hour = "0" + hour;
    }
    if (minute >= 0 && minute <= 9) {
        minute = "0" + minute;
    }
    var nowStr =  year + seperator1 + month + seperator1 + strDate;
    nowStr +=" "+ hour + seperator2 + minute;
    return nowStr;
};

//当前月份
kitFun.getMonthBeginDate = function () {
    var date = new Date();
    date.setDate(1);
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    return year + seperator1 + month + seperator1 + strDate;
};

//当前月份
kitFun.getMonth = function () {
    var date = new Date();
    date.setDate(1);
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    return year + seperator1 + month;
};


//本月
kitFun.getCurrentMonthDate = function () {
    //2020-11-01 - 2020-11-20
    var begin = kitFun.getMonthBeginDate();
    var end = kitFun.getNowDate();
    return begin+" - "+end;
};

//本月
kitFun.getCurrentMonth = function () {
    //2020-11-01 - 2020-11-20
    var begin = kitFun.getMonth();
    return begin+" - "+begin;
};
//打开新页
kitFun.openTab = function (param) {
    top.layui.index.openTab(param);
};
//更新当前页
kitFun.refreshTab = function (param) {
    top.layui.index.refreshTab(param);
};
//更新当前页
kitFun.closeTab = function () {
    top.layui.index.closeNowTab();
};

//返回指定页面
kitFun.goBack = function(returnUrl) {
    var  oriUrl = returnUrl;
    if (oriUrl) {
        var title = "";
        var nowUrl = oriUrl;
        var titleIndex = oriUrl.indexOf("$$$");
        if (titleIndex > 0) {
            title = oriUrl.substring(0, titleIndex);
            nowUrl = oriUrl.substring(titleIndex + 3);
        }
        var returnUrl = Feng.ctxPath + nowUrl;
        kitFun.refreshTab({title: title, url: returnUrl});
    }
};

//form赋值
kitFun.fillForm = function (formId, data) {
    if (!data) {
        return;
    }
    var $ = layui.$;
    for (var name in data) {
        $("#" + formId + " #" + name).val(data[name]);
        if (data[name] == "Y") {
            $("#" + formId + " #" + name).attr("checked", true);
        }
        if (data[name] == "on") {
            $("#" + formId + " #" + name).attr("checked", true);
        }
    }
    //checkbox特殊处理
    $("#" + formId).find("input[type=checkbox]").each(function (checkboxIndex, checkboxObj) {
        var name = checkboxObj.id;
        var nowValue = data[name];
        if (nowValue == "Y" || nowValue == "ON" || nowValue == "on" || nowValue == "y") {
            $("#" + formId + " #" + name).attr("checked", true);
        } else {
            $("#" + formId + " #" + name).attr("checked", false);
        }
    });
    //checkbox特殊处理
    $("#" + formId).find("input[type=radio]").each(function (radioIndex, radioObj) {
        var name = $(radioObj).attr("name");
        var nowValue = data[name];
        var pageValue = $(radioObj).attr("value");
        if (pageValue == nowValue) {
            $(radioObj).attr("checked", true);
        }
    });
    if (layui && layui.form) {
        layui.form.render();
    }
};
//form取值
kitFun.getForm = function (formId) {
    var $ = layui.$;
    var formDataArr = $("#" + formId).serializeArray();
    var formData = {};
    if (formDataArr) {
        var nowField;
        for (var itemIndex = 0; itemIndex < formDataArr.length; itemIndex++) {
            nowField = formDataArr[itemIndex];
            if (nowField.value || nowField.value == '') {
                formData[nowField.name] = nowField.value;
            }
        }
    }
    //checkbox特殊处理
    $("#" + formId).find("input[type=checkbox]").each(function (checkboxIndex, checkboxObj) {
        if (checkboxObj.checked) {
            formData[checkboxObj.id] = "Y";
        } else {
            formData[checkboxObj.id] = "N";
        }
    });
    return formData;
};
//异步取得配置
kitFun.ax = function (url, callbackFun) {
    var $ = layui.$;
    $.ajax({
        url: url,
        type: 'POST',
        cache: false,
        success: function (resData) {
            if (typeof callbackFun == 'function') {
                callbackFun(resData);
            }
        }
    });
};


//弹出对话框
kitFun.show  = function (selectVO, callback, selectedInit) {
    if (!selectVO['height']) {
        selectVO['height'] = 640;
    }
    if (!selectVO['width']) {
        selectVO['width'] = 800;
    }
    layui.use(['func'], function () {
        layui.func.openEx({
            height: selectVO.height,
            width: selectVO.width,
            title: selectVO.title,
            content: Feng.ctxPath + '/system/common/show?url=' + selectVO.formUrl+"&n_="+new Date().getTime(),
            success: function (layero, tempSelectIndex) {
                var iframeWin = parent.window[layero.find('iframe')[0]['name']];
                iframeWin.SimpleSelect.initSelect(selectVO, function (selectData) {
                    var msg = callback(selectData);
                    if (msg) {
                        Feng.error(msg);
                        return;
                    } else {
                        //关掉对话框
                        parent.layer.close(tempSelectIndex);
                    }
                },selectedInit);

            }
        });
    });
};