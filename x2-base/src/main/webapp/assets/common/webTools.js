!(function () {
    var WebTools = {};

    function CustomTools() {}
    CustomTools.prototype = {
        /**
         * 验证数字格式
         * @param param: 要验证的数字和验证类型
         * format: {format: '要验证的格式', value: '要验证的值'}
         * 格式说明： (+/-)正负数验证，没有不验证。+：包含0
         *  ''： 不进行格式验证
         *  '#'： 整数
         *  '#.': 不限制位数的小数
         *  '#.#': 一位小数，多个继续往后加'#'
         * @return {*}
         */
        validateNumber: function (param) {
            param.value = this.isEmpty(param.value) ? param.value : '';
            if (param.value === '' || isNaN(param.value)) {
                return {result: false, msg: '请输入正确的数字'};
            }
            param.format = param.format || "#.";
            var firstChar =  param.format.substr(0, 1);
            if(firstChar === '+' && parseFloat(param.value) < parseFloat(0)){
                return {result: false, msg: '请输入大于等于0的数字'};
            }else if(firstChar === '-' && parseFloat(param.value) >= parseFloat(0)){
                return {result: false, msg: '请输入小于0的数字'};
            }
            var paramValue = param.value.replace('-', '');
            var valueArray = paramValue.split('.');
            var formatArray = param.format.split('.');
            if (formatArray.length === 1) {
                if (paramValue.indexOf('0') === 0) {/**说明是整数，判断首位不为0*/
                    return {result: false, msg: '首位不能为0'};
                }
                if (valueArray.length > 1) {
                    return {result: false, msg: '请输入整数'};
                }
            } else {
                /**
                 * 1、验证整数位是否为null。
                 * 2、验证小数位是否为null。
                 * 2、整数位长度大于1，并且第一位是0
                 */
                if (valueArray[0] === '' || (valueArray.length > 1 && valueArray[1] === '') || (valueArray[0].length > 1 && valueArray[0].indexOf('0') === 0)) {
                    return {result: false, msg: '请输入正确的小数'};
                }
                if(formatArray[1] !== ''){
                    /**需要验证小数的位数长度*/
                    if (valueArray.length > 1 && valueArray[1].length > formatArray[1].length) {
                        return {result: false, msg: '小数位长度不能大于' + formatArray[1].length};
                    }
                }
            }
            return {result: true, msg: ''};
        },

        /**
         * 指定毫秒数后的日期及指定格式
         * @param millisecond: 毫秒数，支持负数, 不传递默认当前时间
         * @param format: 转换格式，如果不传递默认转换成时间类型,传递为字符格式
         * @return {string/date}
         */
        dateFormat: function (millisecond, format) {
            millisecond = millisecond || 0;
            millisecond = new Date().getTime() + millisecond;
            var resultDate = new Date(millisecond);
            if(format){
                var dateJson = {
                    "y+": resultDate.getFullYear(),
                    "M+": resultDate.getMonth() + 1,
                    "d+": resultDate.getDate(),
                    "H+": resultDate.getHours(),
                    "m+": resultDate.getMinutes(),
                    "s+": resultDate.getSeconds(),
                };
                for(var key in dateJson){
                    if(new RegExp("(" + key + ")").test(format)){
                        format = format.replace(RegExp.$1, (dateJson[key] + "").length < 2 ? "0" + dateJson[key] : dateJson[key]);
                    }
                }
                return format;
            }
            return resultDate;
        },

        /**
         * 比较两个日期的大小
         * @param sdate: 开始日期
         * @param edate：结束日期
         * @return {boolean} true：结束日期>开始日期 false: 结束日期<开始日期
         */
        dateCompare: function (sdate, edate) {
            var stime = new Date(sdate);
            var etime = new Date(edate);
            if(etime.getTime() > stime.getTime()){
                return true;
            } else {
                return false;
            }
        },

        /**
         * 验证各类证件号码
         * @param param {type: '证件的编号', value: '要验证的值'}
         * 类型可以选:
         *      idcard(身份证号)
         * @return {result: boolean, msg: string}
         */
        validateCardNo: function (param) {
            if (param.type === 'idcard') {
                var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
                if (!reg.test(param.value)) {
                    return {result: false, msg: '请输入正确的身份证号'};
                }
                return {result: true, msg: ''};
            }
            return {result: false, msg: '方法完善中'};
        },

        /**验证是否为null */
        isEmpty: function (obj) {
            if (obj == undefined || obj == null || obj == "") {
                return false;
            }
            return true;
        },

        /**
         * 去掉左右空格
         * @param value
         */
        trim: function (value) {
            if (!this.isEmpty(value)) {
                return value;
            }
            value = value.replace( /^\s*/, "");
            value = value.replace( /\s*$/, "");
            return value;
        },

        /**
         * 获取地址参数值
         * @param value：参数名字
         */
        getUrlValue: function (value) {
            var reg = new RegExp("(^|&)" + value + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {return unescape(r[2])}; return null;
        },

        /**
         * 验证是否为数字
         * @param value: 值
         * @returns {boolean}
         */
        isNumber: function (value) {
            var reg = new RegExp(/(-?\d+)(\.\d+)?$/);
            return reg.test(value);
        },

        /**
         * 转换为浮点型
         * @param value: 要转换的值
         * @param defaultValue：默认值
         * @returns {*}
         */
        toFloat: function (value, defaultValue) {
            if (value && this.isNumber(value)) {
                return parseFloat(value);
            }
            return defaultValue;
        },

        /**
         * 转换为整型
         * @param value: 要转换的值
         * @param defaultValue：默认值
         * @returns {*}
         */
        toInt: function (value, defaultValue) {
            if (value && this.isNumber(value)) {
                return parseInt(value);
            }
            return defaultValue;
        },




    };

    // 工具类
    WebTools.util = function() {
        return new CustomTools();
    };

    /**
     * 根据id获取元素对象
     * @param id：元素id
     * @returns {HTMLElement}
     */
    WebTools.$c = function(id) {
        return document.getElementById(id);
    };

    /**
     * 根据id决定是set innerHTML 还是 get innerHTML
     * @param id：元素id
     * @param content：要写入innerHTML的内容，如果不传为获取innerHTML
     * @returns {string}
     */
    WebTools.$html = function(id, content) {
        if (content) {
            this.$c(id).innerHTML = content;
        } else {
            return this.$c(id).innerHTML;
        }
    };

    /**
     * 根据id获取内容
     * @param id： 元素id
     * @returns {*}
     */
    WebTools.$val = function(id) {
        return this.$c(id).value;
    };

    /**
     * 原生AJAX请求
     * @param options：{
     *     url: 请求地址,
     *     type: POST/GET,
     *     param: {} 参数,
     *     success: function(data) {} 成功的回调方法,
     *     error: function(status) {} 失败的回调方法
     * }
     */
    WebTools.ajax = function (options) {
        options.type = options.type || "GET";
        var paramStr = "";
        var formData = [];
        if (options.param != undefined) {
            for (var key in options.param) {
                formData.push(encodeURIComponent(key) + "=" + encodeURIComponent(options.param[key]));
            }
            if (formData.length > 0) {
                paramStr = formData.join('&');
            }
        }
        var xhr = new XMLHttpRequest();
        if (options.type == 'GET') {
            var url = options.url;
            if (paramStr.length > 0) {
                url = url + "?" + paramStr;
            }
            xhr.open("GET", url, true);
            xhr.send(null);
        } else if (options.type == 'POST') {
            xhr.open("POST", options.url, true);
            xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xhr.send(paramStr);
        }
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                var status = xhr.status;
                if (status >= 200 && status < 300 || status == 304) {
                    options.success && options.success(JSON.parse(xhr.responseText));
                } else {
                    options.error && options.error(status);
                }
            }
        }
    };

    /**
     * 根据className获取对象
     * @param className: class的名字
     * @param tagName：获取的标签名，如div、 span等
     * @returns {*}
     */
    WebTools.$class = function(className, tagName) {
        if (document.getElementsByClassName)    {
            return document.getElementsByClassName(className)
        } else {
            var nodes = document.getElementsByTagName(tagName), ret = [];
            for (var i = 0; i < nodes.length; i++) {
                if (hasClass(nodes[i], className)) {
                    ret.push(nodes[i]);
                }
            }
            return ret;
        }
        function hasClass(tagStr, className){
            var arr = tagStr.className.split(/\s+/);
            for ( var i = 0; i < arr.length; i++) {
                if (arr[i] == className) {
                    return true;
                }
            }
            return false;
        };
    };

    /**
     * 动态创建下拉框
     * @param option: {
     *     elem: '元素id',
     *     url: '数据源',
     *     value: '数据源中option value的属性',
     *     text: '数据源中option 显示的属性',
     *     head: '初始化显示的值',
     *     defaultValue: '默认值'
     * }
     */
    WebTools.createSelect = function(option) {
        if (!option['elem'] || !option['value'] || !option['text']) {
            return;
        }
        option['head'] = option['head'] || '请选择';
        option['defaultValue'] = option['defaultValue'] || '';
        WebTools.ajax({
            url: option.url, type: "GET", param: {},
            success: function(result) {
                var select = $('#' + option.elem);
                select.empty();
                select.append('<option value="">' + option.head + '</option>');
                if (result != null) {
                    for (var i = 0; i < result.length; i++) {
                        var selected = '';
                        if (option.defaultValue !== '' && option.defaultValue === result[i]['value']) {
                            selected = "selected";
                        }
                        select.append('<option value="' + result[i][option.value] + '" ' + selected + '>' + result[i][option.text] + '</option>');
                    }
                }
            }
        });
    };

    /**
     * 线程处理费时请求，并检查执行状态
     * @param ctxPath: 项目上下文
     * @param threadSign：实现类标记
     * @param cacheKey：线程使用的缓存名
     * @param param：请求参数，json结构
     * @param timeInterval：检查线程间隔时间，默认500毫秒
     * @param callback：回调函数
     */
    WebTools.process = function(ctxPath, implSign, cacheKey, param, timeInterval, callback) {
        var loadObject = null;
        var timeOut = null;
        var showTips = "showProcessContainer";
        param = param || {};
        param['ck'] = cacheKey;
        param['ps'] = implSign;

        // 目前没有好的弹层，所以暂时先用layui的，以后有好的再说

        function requestFunction() {
            WebTools.ajax({
                url: ctxPath + "/yx/process/startProcess",
                type: "GET",
                param: param,
                success: function(result) {
                    if (result.code !== 200) {
                        alert(result.message);
                        return;
                    }
                    var timeInterval = timeInterval || 500;
                    loadObject = layer.msg('<span id="' + showTips + '" style="font-weight: bold; font-size: 18px;">' + result.data + '</span>', {icon: 16, shade: 0.3, time:0, area:['400px','66px']});
                    timeOut = window.setInterval(function() {
                        checkFunction();
                    }, timeInterval);
                }
            });
        }

        function checkFunction() {
            WebTools.ajax({
                url: ctxPath + "/yx/process/checkProcess",
                type: "GET",
                param: param,
                success: function(result) {
                    WebTools.$html(showTips, result.data);
                    if (result.data === 'finish') {
                        layer.close(loadObject);
                        window.clearInterval(timeOut);
                        timeOut = null;
                        loadObject = null;
                        if (callback !== undefined) {
                            callback();
                        }
                    }
                }
            });
        }

        requestFunction();
    };

    window.WebTools = WebTools;
})();