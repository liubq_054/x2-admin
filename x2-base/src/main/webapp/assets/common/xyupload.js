var xyUploadModule = {};
xyUploadModule.index = 0;
xyUploadModule.map = [];
xyUploadModule.delete = function (objectKey, fileIndex) {
    xyUploadModule.map[objectKey].delete(fileIndex);
};
xyUploadModule.createXyupload = function () {
    xyUploadModule.index++;
    var xyuploadObject = new Object();
    xyuploadObject.layui;
    xyuploadObject.objectKey = "obj_" + xyUploadModule.index;
    xyUploadModule.map[xyuploadObject.objectKey] = xyuploadObject;
    xyuploadObject.$;
    xyuploadObject.upload;
    xyuploadObject.fileId;
    xyuploadObject.fileSelectBtn;
    xyuploadObject.fileAttachList;
    xyuploadObject.index = 0;
    //初始化
    xyuploadObject.init = function (layui, config) {
        xyuploadObject.layui = layui;
        xyuploadObject.$ = layui.$;
        xyuploadObject.upload = layui.upload;
        xyWfVar.init(layui);
        if (!xyuploadObject.upload) {
            Feng.alert("layui没有加载upload模块，不能上传附件！")
            return;
        }
        var accept = "file";
        var exts;
        if (config) {
            xyuploadObject.fileId = config.idName;
            xyuploadObject.fileAttachList = config.listName;
            xyuploadObject.fileSelectBtn = config.fileBtn;
            xyuploadObject.workFlowItem = config.workFlowItem;
            if (config.accept) {
                accept = config.accept;
            }
            if (config.exts) {
                exts = config.exts;
            }
        }
        if (!xyuploadObject.fileId) {
            xyuploadObject.fileId = 'fileId';
        }

        if (!xyuploadObject.fileAttachList) {
            xyuploadObject.fileAttachList = 'fileAttachList';
        }
        if (!xyuploadObject.fileSelectBtn) {
            xyuploadObject.fileSelectBtn = 'fileSelectBtn';
        }
        if (!xyuploadObject.workFlowItem) {
            xyuploadObject.workFlowItem = 'bzclitem';
        }
        if (xyuploadObject.isEdit()) {
            var fileSelectBtnObj = xyuploadObject.$("#" + xyuploadObject.fileSelectBtn);
            if (fileSelectBtnObj) {
                //上传文件
                var uploadConfig = {
                    elem: '#' + xyuploadObject.fileSelectBtn,
                    url: Feng.ctxPath + '/system/upload',
                    number: 0,
                    accept: accept,
                    multiple: true,
                    before: function (result) {
                    },
                    done: function (result) {
                        xyuploadObject.afterDone(result.data);
                    },
                    allDone: function (result) {
                        //当文件全部被提交后，才触发
                        Feng.success("上传成功！");
                    },
                    error: function () {
                        Feng.error("上传失败！");
                    }
                };
                if (exts) {
                    uploadConfig['exts'] = exts;
                }
                xyuploadObject.upload.render(uploadConfig);
            }
        } else {
            xyuploadObject.$("#" + xyuploadObject.fileSelectBtn).hide();
        }
        xyuploadObject.initPage();
    };
    //是否只读
    xyuploadObject.isEdit = function () {
        return xyWfVar.isEdit(xyuploadObject.workFlowItem);
    };

    //清空旧数据
    xyuploadObject.beforeDone = function () {
        var fileIdObj = xyuploadObject.$("#" + xyuploadObject.fileId);
        fileIdObj.val("");
    };

    xyuploadObject.afterDone = function (fileItem) {
        var fileIdObj = xyuploadObject.$("#" + xyuploadObject.fileId);
        var nowValue = fileIdObj.val();
        if (nowValue && nowValue.length > 0) {
            fileIdObj.val(nowValue + "," + fileItem.fileId);
        } else {
            fileIdObj.val(fileItem.fileId);
        }
        xyuploadObject.index++;
        var fileInfo = fileItem.info;
        var url = Feng.ctxPath + encodeURI("/system/download?fileName=" + fileInfo.originalFilename + "&filePath=" + fileInfo.fileSavePath);
        var html = "";
        var key = xyuploadObject.objectKey;
        var fineLinkKey = key + "_fileLink_" + xyuploadObject.index;
        var fineDeleteKey = key + "_fileDelete_" + xyuploadObject.index;
        html += "<a id='" + fineLinkKey + "' fileid='" + fileItem.fileId + "' href='" + url + "' target='_blank' style='color:blue !important;'>"
        html += ' <span class="layui-inline" style="padding: 5px 2px 10px 5px;text-decoration: underline;" id="fileName">' + fileInfo.originalFilename;
        html += '</span> ';
        html += '</a>';
        if (xyuploadObject.isEdit()) {
            html += "<a href='#' id='" + fineDeleteKey + "'> <span  onclick='xyUploadModule.delete(\"" + key + "\"," + xyuploadObject.index + ")' style='padding:4px 8px 2px 2px;color:red !important;' title='删除'>X</span></a>";
        }
        xyuploadObject.$("#" + xyuploadObject.fileAttachList).append(html);
    };
    //删除
    xyuploadObject.delete = function (index) {
        var key = xyuploadObject.objectKey;
        var fineLinkKey = key + "_fileLink_" + index;
        var fineDeleteKey = key + "_fileDelete_" + index;
        var delfileId = xyuploadObject.$("#" + fineLinkKey).attr("fileid");
        xyuploadObject.$("#" + fineLinkKey).remove();
        xyuploadObject.$("#" + fineDeleteKey).remove();
        var fileIdObj = xyuploadObject.$("#" + xyuploadObject.fileId);
        var fileIdList = fileIdObj.val();
        if (fileIdList) {
            var ids = fileIdList.split(",");
            var newIds = "";
            for (var idx = 0; idx < ids.length; idx++) {
                if (ids[idx] == delfileId) {
                    continue;
                }
                if (newIds.length > 0) {
                    newIds += "," + ids[idx];
                } else {
                    newIds += ids[idx];
                }
            }
            fileIdObj.val(newIds);
        }

    };
    //添加数据
    xyuploadObject.addList = function (fileList) {
        xyuploadObject.beforeDone();
        for (var idx = 0; idx < fileList.length; idx++) {
            var fileItem = {"fileId": fileList[idx].fileId, "info": fileList[idx]};
            xyuploadObject.afterDone(fileItem);
        }
    };
    //添加数据
    xyuploadObject.checkFileUpload = function (fileList) {
        if (xyuploadObject.isEdit()) {
            var fileIdObj = xyuploadObject.$("#" + xyuploadObject.fileId).val();
            if (!fileIdObj) {
                return "请选择文件上传附件!";
            }
        }
        return "";
    };

    //初始化页面
    xyuploadObject.initPage = function () {
        var fileIdList = xyuploadObject.$("#" + xyuploadObject.fileId).val();
        if (!fileIdList) {
            return;
        }
        var url = Feng.ctxPath + '/system/downloadByIds?fileIds=' + fileIdList;
        jQuery.ajax({
            url: url,
            type: 'POST',
            cache: false,
            success: function (resData) {
                Feng.closeLoading();
                xyuploadObject.addList(resData.data);
            },
            error: function () {
                Feng.closeLoading();
                Feng.error("提交失败")
            }
        });
    };
    //字段*************************************************************************************************************************************************************
    return xyuploadObject;
};
var xyupload = xyUploadModule.createXyupload();