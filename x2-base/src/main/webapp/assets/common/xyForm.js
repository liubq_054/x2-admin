//表格操作
var xyForm = {};
xyForm.$;
//form取值
xyForm.getForm = function (formId) {
    return kitFun.getForm(formId);
};

//各个组件的状态判断
xyForm.type = {};
//只读
xyForm.readOnly = function (layui, formId) {
    //根据类型只读
    xyForm.readOnlyByType(layui, formId, "workFlowForm");
};

//只读
xyForm.readOnlyByType = function (layui, formId, typeName) {
    xyForm.$ = layui.$;
    xyForm.$("#" + formId).find("[" + typeName + "]").each(function () {
        var item = xyForm.$(this);
        var itemType = xyForm.$(this).attr(typeName);
        xyForm.type[itemType].read(item);
    });
    //删除所有比输入标识
    var formObj = xyForm.$("#" + formId);
    xyForm.removeAllStar(formObj);
};

xyForm.type["hidden"] = {
    hide: function (obj) {
    },
    read: function (obj) {
    }
};
xyForm.attachIntervalHook
xyForm.attachIntervalHookCount = 0;
xyForm.attachIntervalKeyList = [];
xyForm.type["attach"] = {
    hide: function (obj) {
        obj.remove();
    },
    read: function (obj) {
        var div = obj.find('div');
        if (div !== undefined && div !== null && div.length > 0) {
            var dataSign = xyForm.$(div)[0].getAttribute('data-sign');
            if (dataSign != undefined && dataSign != null && dataSign !== '') {
                xyForm.attachIntervalKeyList.push(dataSign);
                //启动定时器
                if (xyForm.attachIntervalHookCount == 0) {
                    xyForm.attachIntervalHookCount = 1;
                    xyForm.attachIntervalHook = window.setInterval(function () {
                        for (var idx = 0; idx < xyForm.attachIntervalKeyList.length; idx++) {
                            xyForm.$('#btnUploadChoose' + dataSign + 'Container').hide();
                            xyForm.$('.attachTbodyDeleteClass' + dataSign).hide();
                        }
                        xyForm.attachIntervalHookCount++;
                        if (xyForm.attachIntervalHookCount > 15) {
                            window.clearInterval(xyForm.attachIntervalHook);
                        }
                    }, 200);
                }
            }
        }
    }
};


xyForm.type["textinput"] = {
    hide: function (obj) {
        obj.remove();
    },
    read: function (obj) {

        if (obj.is('input') || obj.is('textarea')) {
            var label = obj.val();
            label = label.replace(/\r\n/g, "<BR>");
            label = label.replace(/\n/g, "<BR>");
            if (!label) {
                label = "&nbsp;"
            }
            if (obj.is('input')) {
                label = "<span class='form-read-input layui-empty-item'>" + label + "</span>";
            } else {
                label = "<span class='form-read-input layui-empty-item layui-textarea'>" + label + "</span>";
            }
            obj.parent().html(label);
        } else {
            var label = obj.find('input').val();
            if (!label) {
                label = "&nbsp;"
            }
            label = "<span class='form-read-input layui-empty-item'>" + label + "</span>";
            obj.html(label);
        }

    }
};

xyForm.type["radio"] = {
    hide: function (obj) {
        obj.remove();
    },
    read: function (obj) {

        var checkedLabel = "";
        var checkEle = obj.find("input:radio:checked");
        if (checkEle.length == 1) {
            checkedLabel = checkEle.attr("label");
        }
        obj.html(checkedLabel);
    }
};

xyForm.type["checkbox"] = {
    hide: function (obj) {
        obj.remove();
    },
    read: function (obj) {

        var checkedLabel = "";
        var checkboxs = obj.find("input[type=checkbox]");
        xyForm.$.each(checkboxs, function (i, checkbox) {
            if (xyForm.$(checkbox).is(":checked")) {
                var value = xyForm.$(checkbox).attr("title");
                if (!value) {
                    checkedLabel += "是，";
                } else {
                    checkedLabel += xyForm.$(checkbox).attr("title") + "，";
                }
            }
        });
        if (checkedLabel.length > 0) {
            checkedLabel = checkedLabel.substring(0, checkedLabel.length - 1);
        }
        checkedLabel = "<span class='form-read-input '>" + checkedLabel + "</span>";
        obj.html(checkedLabel);
    }
};


xyForm.type["select"] = {
    hide: function (obj) {
        obj.remove();
    },
    read: function (obj) {

        var selectLabel = "";
        obj.find("select:not(.noneWorkflow) option:checked").each(function (index, item) {
            if (xyForm.$(item).val() == null || xyForm.$(item).val().length == 0) {
                selectLabel = "";
            } else {
                selectLabel = xyForm.$(item).html();
            }
        });
        if (!selectLabel) {
            selectLabel = "&nbsp;"
        }
        selectLabel = "<span class='form-read-input layui-empty-item'>" + selectLabel + "</span>";
        obj.html(selectLabel);

    }
};

xyForm.type["button"] = {
    hide: function (obj) {
        obj.hide();
    },
    read: function (obj) {
        obj.hide();
    }
};

xyForm.type["closeBtn"] = {
    hide: function (obj) {
        obj.hide();
    },
    read: function (obj) {
        var closeBtnName = obj.attr("closeBtnName");
        if (closeBtnName) {
            obj.val(closeBtnName);
            obj.html(closeBtnName);
        } else {
            obj.val("关 闭");
            obj.html("关 闭");
        }

    }
};

xyForm.type["div"] = {
    hide: function (obj) {
        obj.hide();
    },
    read: function (obj) {
        obj.hide();
    }
};

xyForm.type["self"] = {
    hide: function (obj) {
        obj.hide();
    },
    read: function (obj) {
        var key = obj.attr("xyFormKey");
        var label = xyForm.$("#" + key).val();
        if (!label) {
            label = "&nbsp;"
        }
        if (label == "Y" || label == "on" || label == "y" || label == "ON") {
            label = "是";
        } else if (label == "N" || label == "off" || label == "n" || label == "OFF") {
            label = "否";
        }
        label = "<span class='form-read-input layui-empty-item'>" + label + "</span>";
        obj.html(label);
    }
};

xyForm.removeAllStar = function (obj) {
    obj.find('span').each(function (idx, subObj) {
        if (xyForm.$(subObj).hasClass('required')) {
            xyForm.$(subObj).remove();
        }
    });

    obj.find('label').each(function (idx, subObj) {
        if (xyForm.$(subObj).hasClass('layui-form-required')) {
            xyForm.$(subObj).removeClass('layui-form-required');
        }
    });

};
