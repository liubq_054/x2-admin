var xyListModule = {};
xyListModule.index = 0;
xyListModule.objMap = {};
xyListModule.tTD;
xyListModule.onblurFun = function (objectKey, fieldKey) {
    xyListModule.objMap[objectKey].onblurFun(fieldKey);
};
xyListModule.regEvent = function () {
    //鼠标抬起事件
    document.onmouseup = xyListModule.releaseTablePullEvent;
    //鼠标抬起事件
    document.onmousedown = xyListModule.releaseTablePullEvent;
};
xyListModule.releaseTablePullEvent = function () {
    var tTD = xyListModule.tTD;
    if (tTD) {
        tTD.mouseDown = false;
        tTD.style.cursor = 'default';
    }
};
xyListModule.savePullObject = function (tdObj) {
    xyListModule.tTD = tdObj;
    return tdObj;

};
//去掉多余字符
xyListModule.trimPx = function (width) {
    if (width) {
        var nwidth = width + "";
        if (nwidth.indexOf("px") > 0) {
            return kitFun.toInt(nwidth.substr(0, nwidth.length - 2));
        }
        return kitFun.toInt(nwidth);
    }
    return 0;
};
//是否时数组
xyListModule.isArray = function (o) {
    return Object.prototype.toString.call(o) === '[object Array]';
};

xyListModule.createXyList = function () {
    var xyListObject = new Object();
    xyListModule.index++;
    xyListObject.verify = {};
    xyListObject.eventFun = {};
    xyListObject.nowIndex = (xyListModule.index) * 10000;
    xyListObject.bodyIndex = "body00" + xyListModule.index + "Index";
    xyListModule.objMap[xyListObject.bodyIndex] = xyListObject;
    xyListObject.targetTableId;
    xyListObject.columnInfo;
    xyListObject.pageMode = false;
    xyListObject.columnInfoMap = {};
    xyListObject.layui;
    xyListObject.toInitDateList = [];
    xyListObject.dateObjectMap = {};
    xyListObject.$
    //主题逻辑*************************************************************************************************************************************************************
    //取得单元Key
    xyListObject.getTrKey = function (dataIndex) {
        var key = "line_" + dataIndex;
        return key;
    };
    //取得单元Key
    xyListObject.getFieldKey = function (tarKey, tarDataIndex) {
        var fieldKey = tarKey + "_" + tarDataIndex;
        return fieldKey;
    };
    //初始化
    //{"layui":layui,"targetTableId":targetTableId,"columnInfo":columnInfo,"isReadonly":isReadonly}
    xyListObject.init = function (xySetting) {
        xyListObject.layui = xySetting.layui;
        xyListObject.$ = xyListObject.layui.jquery;
        if (xySetting.pageMode && xySetting.pageMode == 'view') {
            //查看模式
            xyListObject.pageMode = xySetting.pageMode;
        } else {
            if (xySetting.isReadonly) {
                //是否只读
                xyListObject.pageMode = 'view';
            } else {
                //是否只读
                xyListObject.pageMode = xySetting.isReadonly;
            }

        }
        //注册默认检查
        xyListObject.defaultReg();
        //过滤掉不需要显示的
        var newColumnInfo = [];
        var tempCond;
        for (var cidx = 0; cidx < xySetting.columnInfo.length; cidx++) {
            tempCond = xySetting.columnInfo[cidx].existCond;
            if (tempCond && tempCond == 'N') {
                continue;
            }
            newColumnInfo.push(xySetting.columnInfo[cidx])
        }
        //初始化表头
        xyListObject.initHead(xySetting.targetTableId, newColumnInfo);
        //注册列拉事件
        xyListObject.pullColumnEvent(xySetting.targetTableId);
    };

    //列可以拖拽事件
    xyListObject.pullColumnEvent = function (tableId) {
        //注册事件
        xyListModule.regEvent();
        //用来存储当前更改宽度的Table Cell,避免快速移动鼠标的问题
        var tTD;
        var table = document.getElementById(tableId);
        if (table.style) {
            table.oriWidth = xyListModule.trimPx(table.style.width);
        }
        if (table.oriWidth <= 0) {
            table.oriWidth = 1620;
        }
        for (var j = 0; j < table.rows[0].cells.length; j++) {
            table.rows[0].cells[j].onmousedown = function () {
                //记录单元格
                xyListModule.releaseTablePullEvent();
                tTD = xyListModule.savePullObject(this);
                if (event.offsetX > tTD.offsetWidth - 10) {
                    tTD.mouseDown = true;
                    tTD.oldX = event.x;
                    tTD.oldWidth = tTD.offsetWidth;
                }
                //记录Table宽度
                //table = tTD; while (table.tagName != ‘TABLE') table = table.parentElement;
                //tTD.tableWidth = table.offsetWidth;
                event.stopPropagation();
            };
            table.rows[0].cells[j].onmouseup = function () {
                //先结束
                xyListModule.releaseTablePullEvent();
                event.stopPropagation();
            };
            table.rows[0].cells[j].onmousemove = function () {
                //更改鼠标样式
                if (event.offsetX > this.offsetWidth - 10) this.style.cursor = 'col-resize';
                //取出暂存的Table Cell
                if (tTD == undefined) {
                    xyListModule.releaseTablePullEvent();
                    tTD = xyListModule.savePullObject(this);
                }
                //调整宽度
                if (tTD.mouseDown != null && tTD.mouseDown == true) {

                    if (tTD.oldWidth + (event.x - tTD.oldX) > 0) tTD.width = tTD.oldWidth + (event.x - tTD.oldX);
                    var oldWidth = xyListModule.trimPx(tTD.oldWidth);
                    var newWidth = xyListModule.trimPx(tTD.width);
                    //调整列宽
                    tTD.style.width = (newWidth) + "px";
                    // tTD.style.cursor = 'col-resize';
                    //调整该列中的每个Cell
                    var tempTable = tTD;
                    while (tempTable.tagName != 'TABLE') tempTable = tempTable.parentElement;
                    for (j = 0; j < tempTable.rows.length; j++) {
                        tempTable.rows[j].cells[tTD.cellIndex].width = tTD.width;
                    }
                    tempTable.width = table.oriWidth + (newWidth - oldWidth);
                    tempTable.style.width = tempTable.width + "px";
                } else {
                    if (event.offsetX > this.offsetWidth - 10) this.style.cursor = 'col-resize';
                    else this.style.cursor = 'default';
                }
            };
        }

    };


    //初始化表头
    xyListObject.initHead = function (targetTableId, columnInfo) {
        xyListObject.targetTableId = targetTableId;
        xyListObject.columnInfo = columnInfo;
        var h = "";
        h += "<thead>";
        h += "<tr onmouserover='return false'>";
        var citem;
        var oldKey = "liubq";
        for (var cidx = 0; cidx < columnInfo.length; cidx++) {
            citem = columnInfo[cidx];
            if (citem.sWidth) {
                h += "<th style='width: " + citem.sWidth + "' class='xy-table-title' >" + citem.title + "</th>";
            } else {
                h += "<th  class='xy-table-title' >" + citem.title + "</th>";
            }
            if (oldKey.indexOf("," + citem.field + ",") > 0) {
                alert("定义表格属性有问题，出现(" + citem.field + ")重复列");
                return;
            } else {
                oldKey += "," + citem.field + ",";
            }
            xyListObject.columnInfoMap[citem.field] = citem;
        }
        h += "</tr>";
        h += "</thead>";
        h += "<tbody id='" + xyListObject.targetTableId + "_body'>";
        h += "</tbody>";
        xyListObject.$("#" + xyListObject.targetTableId).html(h);
    }
    //添加行
    xyListObject.append = function (inDataList, anchor) {
        if (!inDataList) {
            return;
        }
        var dataList;
        if (!xyListModule.isArray(inDataList)) {
            dataList = [inDataList];
        } else {
            dataList = inDataList;
        }
        //添加行
        xyListObject.addRows(dataList, anchor);
    };
    //修改行
    xyListObject.modify = function (dataIndex, dataList) {
        if (!dataList) {
            return;
        }
        if (!dataIndex || dataIndex < 0) {
            xyListObject.append(dataList);
            return;
        }
        //是否是数组
        if (!xyListModule.isArray(dataList)) {
            var data = dataList;
            for (var tempKey in data) {
                xyListObject.setCellData(tempKey, dataIndex, data[tempKey]);
            }
        } else {
            var data = dataList[0];
            for (var tempKey in data) {
                xyListObject.setCellData(tempKey, dataIndex, data[tempKey]);
            }
            //选择多行时
            if (dataList.length > 1) {
                xyListObject.append(dataList.slice(1), xyListObject.getTrKey(dataIndex));
            }
        }
    };

    //循环表体
    xyListObject.rowLoop = function (callbackFun) {
        xyListObject.$("input[id='" + xyListObject.bodyIndex + "']").each(function (docIdIndex, docIdObj) {
            var dataIndex = docIdObj.value;
            callbackFun(dataIndex);
        });
    };

    //清理一列所有数据
    xyListObject.clearColumnData = function (tarKey, defaultValue) {
        var newdefaultValue = defaultValue;
        if (typeof (defaultValue) == undefined) {
            newdefaultValue = "";
        }
        xyListObject.rowLoop(function (dataIndex) {
            xyListObject.setCellData(tarKey, dataIndex, newdefaultValue);
        });
    };

    //取得一列索引
    xyListObject.getColumnIndex = function () {
        var data = [];
        xyListObject.rowLoop(function (dataIndex) {
            data.push(dataIndex);
        });
        return data;
    };
    //取得一列所有数据
    xyListObject.getColumnData = function (tarKey) {
        var data = [];
        xyListObject.rowLoop(function (dataIndex) {
            data.push(xyListObject.getCellData(tarKey, dataIndex));
        });
        return data;
    };

    //赋值一列数据
    xyListObject.setColumnData = function (pkKey, pkValue, tarKey, tarValue) {
        xyListObject.rowLoop(function (dataIndex) {
            var fieldPkValue = xyListObject.getCellData(pkKey, dataIndex);
            if (fieldPkValue == pkValue) {
                xyListObject.setCellData(tarKey, dataIndex, tarValue);
            }
        });
    };


    //取得单元格对象
    xyListObject.getCellObject = function (tarKey, tarDataIndex) {
        var fieldKey = xyListObject.getFieldKey(tarKey, tarDataIndex);
        return xyListObject.$("#" + fieldKey);
    };
    //取得数据
    xyListObject.getCellData = function (tarKey, tarDataIndex, defaultValue) {
        var newdefaultValue = defaultValue;
        if (typeof (defaultValue) == undefined) {
            newdefaultValue = "";
        }
        var cellObject = xyListObject.getCellObject(tarKey, tarDataIndex);
        if (cellObject) {
            var nowValue = cellObject.val();
            if (!nowValue) {
                return newdefaultValue;
            }
            return nowValue;
        }
        return newdefaultValue;
    };
    //赋值数据
    xyListObject.setCellData = function (tarKey, tarDataIndex, newValue) {
        var cellObject = xyListObject.getCellObject(tarKey, tarDataIndex);
        if (cellObject) {
            if (newValue) {
                cellObject.val(newValue);
            } else {
                cellObject.val("");
            }
        }
    };

    //取得所有值
    xyListObject.getData = function (check) {
        //默认检查
        if (!check) {
            check = true;
        }
        var allData = [];
        var allPass = true;
        xyListObject.$("input[id='" + xyListObject.bodyIndex + "']").each(function (docIdIndex, docIdObj) {
            var dataIndex = docIdObj.value;
            var lineObj = {"sortNum": dataIndex};
            xyListObject.$("#" + xyListObject.getTrKey(dataIndex)).find("[id$='_" + dataIndex + "']").each(function (itemIndex, itemObj) {
                var itemJqueryObj = xyListObject.$(itemObj);
                var tempKey = itemJqueryObj.attr("xykey");
                var tempId = itemJqueryObj.attr("id");
                if (tempKey) {
                    var nowColumnInfo = xyListObject.columnInfoMap[tempKey];
                    var tempValue;
                    var tempType = itemJqueryObj.attr("xytype");
                    if (tempType == 'checkbox') {
                        var checked = itemJqueryObj.is(':checked');
                        if (checked) {
                            tempValue = "Y"
                        } else {
                            tempValue = "N"
                        }
                    } else {
                        tempValue = itemJqueryObj.val();
                    }

                    if (tempValue) {
                        lineObj[tempKey] = tempValue;
                    } else {
                        tempValue = '';
                    }
                    if (check) {
                        if (nowColumnInfo) {
                            if (!xyListObject.checkLineData(tempValue, tempId, nowColumnInfo)) {
                                allPass = false;
                            }
                        }

                    }
                }
            });
            allData.push(lineObj);
        });
        if (allPass) {
            return {"data": allData, "checked": true};
        } else {
            return {"checked": false};
        }
    };
    //检查数据
    xyListObject.checkLineData = function (value, id, nowColumnInfo) {
        if (!nowColumnInfo.verify) {
            return true;
        }
        var nowVerifyList = nowColumnInfo.verify;
        if (nowVerifyList && nowVerifyList.length > 2) {
            var nowVerifyArr = nowVerifyList.split("|");
            xyListObject.$("#" + id + "_error").remove();
            for (var vIndex = 0; vIndex < nowVerifyArr.length; vIndex++) {
                var temVerify = nowVerifyArr[vIndex];
                var paramArr = temVerify.split(",");
                var funName = paramArr[0];
                if (xyListObject.verify[funName]) {
                    var otherParam = [];
                    if (paramArr.length > 1) {
                        otherParam = paramArr.splice(1);
                    }
                    var resMsg = xyListObject.verify[funName]({
                        "disKey": id,
                        "key": nowColumnInfo.field,
                        "value": value
                    }, otherParam)
                    if (resMsg && resMsg.length > 2) {
                        var h = "<div id='" + id + "_error' class='xy_error'>";
                        h += "<span class='xy_error_msg'>";
                        h += resMsg;
                        h += "</span>";
                        h += "</div>";
                        xyListObject.$("#" + id).parent().append(h);
                        return false;
                    }
                }
            }
        }
        return true;
    };

    //添加行
    xyListObject.addRows = function (dataList, anchor) {
        if (!dataList) {
            return;
        }
        var h = "";
        var data;
        for (var dataIdx = 0; dataIdx < dataList.length; dataIdx++) {
            data = dataList[dataIdx];
            var trIndex = xyListObject.nowIndex++;
            h += "<tr id='" + xyListObject.getTrKey(trIndex) + "'>";
            var citem;
            for (var cidx = 0; cidx < xyListObject.columnInfo.length; cidx++) {
                citem = xyListObject.columnInfo[cidx];
                h += "<td>";
                if (cidx == 0) {
                    h += "<input type='hidden' id='" + xyListObject.bodyIndex + "' name='" + xyListObject.bodyIndex + "' value='" + trIndex + "'>";
                }
                h += "<div class=\"xy-table-cell\">";
                var tempValue = data[citem.field];
                if (!tempValue) {
                    tempValue = "";
                }
                var metaInfo = {
                    "data": data,
                    "dataIndex": trIndex,
                    "columnInfo": citem,
                    "key": citem.field,
                    "value": tempValue
                };
                h += citem.meta(metaInfo, xyListObject)
                h += "</div>";
                h += "</td>";
            }
            h += "</tr>";
        }
        if (anchor) {
            xyListObject.$("#" + anchor).after(h);
        } else {
            xyListObject.$("#" + xyListObject.targetTableId + "_body").append(h);
        }
        //初始化时间
        if (xyListObject.toInitDateList.length > 0) {
            var nowConfig
            var laydate = xyListObject.layui.laydate;
            for (var dateIdx = 0; dateIdx < xyListObject.toInitDateList.length; dateIdx++) {
                nowConfig = xyListObject.toInitDateList[dateIdx];
                if (nowConfig) {
                    var newDateObj = laydate.render({
                        elem: '#' + nowConfig.key,
                        type: 'date',
                        trigger: 'click',
                        done: function (value, date, endDate) {
                            var nowKey = this.elem[0].id;
                            var nowInfo = xyListObject.dateObjectMap[nowKey];
                            if (nowInfo && nowInfo.relKey && nowInfo.relAction) {
                                var nowRelInfo = xyListObject.dateObjectMap[nowInfo.relKey];
                                if (nowRelInfo && nowRelInfo.dateObj) {
                                    if (nowInfo.relAction == "lt") {
                                        nowRelInfo.dateObj.config.min = {
                                            year: date.year,
                                            month: date.month - 1,
                                            date: date.date
                                        };
                                    } else if (nowInfo.relAction == "gt") {
                                        nowRelInfo.dateObj.config.max = {
                                            year: date.year,
                                            month: date.month - 1,
                                            date: date.date
                                        };
                                    }
                                }
                            }
                            xyListObject.onblurFun(nowKey);
                        }
                    });
                    if (nowConfig.cache == "Y" || nowConfig.cache == "y") {
                        nowConfig['dateObj'] = newDateObj;
                        xyListObject.dateObjectMap[nowConfig.key] = nowConfig;
                    }
                }
            }
            xyListObject.toInitDateList = [];
        }
    };
    //删除选择的行
    xyListObject.deleteSelected = function () {
        var has = false;
        xyListObject.$("input[id^='xyfirstSelect_']").each(function (docIdIndex, docIdObj) {
            if (docIdObj.checked) {
                has = true;
                var dataIndex = xyListObject.$(docIdObj).val();
                xyListObject.$("#" + xyListObject.getTrKey(dataIndex)).remove();
            }
        });
        if (!has) {
            Feng.alert("请选择待删除数据!");
        }
    };

    //删除选择的行
    xyListObject.deleteByIndex = function (dataIndex) {
        xyListObject.$("#" + xyListObject.getTrKey(dataIndex)).remove();
    };

    //清理表体数据
    xyListObject.clearData = function () {
        xyListObject.$("#" + xyListObject.targetTableId + "_body").html("");
    };
    //注册验证事件
    xyListObject.verifyReg = function (inVerify) {
        if (!inVerify) {
            inVerify = {};
        }
        for (var nowKey in inVerify) {
            xyListObject.verify[nowKey] = inVerify[nowKey];
        }
    };
    //注册变化事件
    xyListObject.eventReg = function (inEvent) {
        if (!inEvent) {
            inEvent = {};
        }
        for (var nowEventKey in inEvent) {
            xyListObject.eventFun[nowEventKey] = inEvent[nowEventKey];
        }
    };
    //删除错误
    xyListObject.onblurFun = function (fieldKey) {
        xyListObject.$("#" + fieldKey + "_error").remove();
        var tempIndex = fieldKey.lastIndexOf("_");
        var xkkey = fieldKey.substr(0, tempIndex);
        var dataIndex = fieldKey.substr(tempIndex + 1);
        var nowEvent = xyListObject.columnInfoMap[xkkey].event;
        if (!nowEvent) {
            return;
        }
        if (xyListObject.eventFun[nowEvent]) {
            var xyvalue = xyListObject.$("#" + fieldKey).val();
            xyListObject.eventFun[nowEvent](xkkey, xyvalue, dataIndex);
        }
    };


    //是否只读
    xyListObject.isReadOnly = function (columnInfo) {
        if (xyListObject.pageMode) {
            return true;
        }
        var sReadonlyFlagIn = columnInfo.sReadonly;
        var sReadonlyFlag = false;
        if (sReadonlyFlagIn) {
            if (sReadonlyFlagIn == 'Y') {
                sReadonlyFlag = true;
            }
        }
        return sReadonlyFlag;
    };
    //是否查看模式
    xyListObject.isView = function () {
        if (xyListObject.pageMode && xyListObject.pageMode == 'view') {
            return true;
        }
        return false;
    };


    //主题逻辑*************************************************************************************************************************************************************
    //检查s*******************************************************************************************************************************************************
    //注册验证事件
    xyListObject.defaultReg = function () {
        xyListObject.verify["required"] = xyListObject.checkRequired;
        xyListObject.verify["float"] = xyListObject.checkFloat;
        xyListObject.verify["int"] = xyListObject.checkInt;
        xyListObject.verify["positive"] = xyListObject.checkPositive;
        xyListObject.verify["number"] = xyListObject.checkNumber;
        xyListObject.verify["maxLen"] = xyListObject.checkMaxLen;
    }
    //必输项目检查
    xyListObject.checkRequired = function (checkInfo, param) {
        var value = checkInfo.value;
        if (!value) {
            return "这是必填字段";
        } else if (value == '') {
            return "这是必填字段";
        }
        return "";
    };
    //数字检查
    xyListObject.checkFloat = function (checkInfo, param) {
        var value = checkInfo.value;
        var reg = new RegExp(/(-?\d+)(\.\d+)?$/);
        if (value && value.length > 0) {
            if (reg.test(value)) {
                return "";
            } else if (value.length > 20) {
                return "输入长度超出限制";
            } else {
                return "请输入浮点数字";
            }
        }
        return "";
    };
    //数字检查
    xyListObject.checkNumber = function (checkInfo, param) {
        return xyListObject.checkFloat(checkInfo, param);
    };
    //整数检查
    xyListObject.checkInt = function (checkInfo, param) {
        var value = checkInfo.value;
        var reg = new RegExp(/-?\d+$/);
        if (value && value.length > 0) {
            if (reg.test(value)) {
                return "";
            } else {
                return "请输入整数";
            }
        }
        return "";
    };
    //整数检查
    xyListObject.checkPositive = function (checkInfo, param) {
        var value = checkInfo.value;
        var reg = new RegExp(/^[1-9]\d*$/);
        if (value && value.length > 0) {
            if (reg.test(value)) {
                return "";
            } else {
                return "请输入正整数";
            }
        }
        return "";
    };
    //正数检查
    xyListObject.checkPositiveFloat = function (checkInfo, param) {
        var value = checkInfo.value;
        var reg = new RegExp(/^([1-9]\d*|0)(\.\d{1,2})?$/);
        if (value && value.length > 0) {
            if (reg.test(value)) {
                return "";
            } else {
                return "请输入正数且最多两位小数";
            }
        }
        return "";
    };
    //负数检查
    xyListObject.checkMinus = function (checkInfo, param) {
        var value = checkInfo.value;
        var reg = new RegExp(/^\-((\d+(\.\d{0,2})?)|(\d*\.\d{1,2}))$/);
        if (value && value.length > 0) {
            if (reg.test(value)) {
                return "";
            } else {
                return "请输入负数且最多两位小数";
            }
        }
        return "";
    };
    //长度检查
    xyListObject.checkMaxLen = function (checkInfo, param) {
        var value = checkInfo.value;
        var len;
        if (!param && param.length == 0) {
            len = 10;
        } else {
            len = param[0];
        }
        var lenInt = parseInt(len);
        if (value && value.length > 0) {
            if (value.length > lenInt) {
                return "最多可以输入 " + lenInt + " 个字符";
            }
        }
        return "";
    };
    //检查e*******************************************************************************************************************************************************
    //字段*************************************************************************************************************************************************************
    //首列选择
    xyListObject.firstSelect = function (metaInfo, saveFlag) {
        var dataIndex = metaInfo.dataIndex;
        var h = "<input xytype='checkbox' type='checkbox' id='xyfirstSelect_" + dataIndex + "' value='" + dataIndex + "' class='xy-list-checkbox'>";
        return h;
    };
    //隐藏列
    xyListObject.hidden = function (metaInfo, name, value, saveFlag) {
        var dataIndex = metaInfo.dataIndex;
        var fieldKey = xyListObject.getFieldKey(name, dataIndex);
        if (!value) {
            value = "";
        }
        var xyKeyStr = " xykey='" + name + "'";
        if (saveFlag && saveFlag == 'N') {
            xyKeyStr = "";
        }
        return "<input " + xyKeyStr + " xytype='hidden' type='hidden' id='" + fieldKey + "' name='" + fieldKey + "' value='" + value + "'>";

    };

    //文本
    xyListObject.label = function (metaInfo) {
        var defaultValue = metaInfo.value;
        return "<div class=\"xy-table-cell\">" + defaultValue + "</div>";
    };

    //按钮
    xyListObject.button = function (metaInfo, title) {
        var dataIndex = metaInfo.dataIndex;
        var name = metaInfo.key;
        var fieldKey = xyListObject.getFieldKey(name, dataIndex);
        if (xyListObject.isView()) {
            return "";
        }
        if (xyListObject.isReadOnly(metaInfo.columnInfo)) {
            return "";
        } else {
            var onblurStr = 'onclick="xyListModule.onblurFun(\'' + xyListObject.bodyIndex + '\',\'' + fieldKey + '\')"';
            return "<input type='button' class='layui-btn layui-btn-primary layui-btn-xs'  value='" + title + "' " + onblurStr + ">";
        }
    };

    //文本
    xyListObject.text = function (metaInfo, saveFlag) {
        var dataIndex = metaInfo.dataIndex;
        var name = metaInfo.key;
        var defaultValue = metaInfo.value;
        var fieldKey = xyListObject.getFieldKey(name, dataIndex);
        if (xyListObject.isView()) {
            return "<div class=\"xy-table-cell\">" + defaultValue + "</div>";
        }
        var xyKeyStr = " xykey='" + name + "'";
        if (saveFlag && saveFlag == 'N') {
            xyKeyStr = "";
        }
        if (xyListObject.isReadOnly(metaInfo.columnInfo)) {
            return "<input " + xyKeyStr + " xytype='text' type='text' id='" + fieldKey + "' name='" + fieldKey + "' value='" + defaultValue + "' class='xy-list-input xy-readonly' readonly='readonly'>";
        } else {
            var onblurStr = 'onchange="xyListModule.onblurFun(\'' + xyListObject.bodyIndex + '\',\'' + fieldKey + '\')"';
            return "<input " + onblurStr + " " + xyKeyStr + " xytype='text' type='text' id='" + fieldKey + "' name='" + fieldKey + "' value='" + defaultValue + "' class='xy-list-input'>";
        }
    };

    //下拉
    xyListObject.select = function (metaInfo, selectConfig, saveFlag) {
        var dataIndex = metaInfo.dataIndex;
        var name = metaInfo.key;
        var defaultValue = metaInfo.value;
        var fieldKey = xyListObject.getFieldKey(name, dataIndex);
        var xyKeyStr = " xykey='" + name + "'";
        if (saveFlag && saveFlag == 'N') {
            xyKeyStr = "";
        }
        var nowConfig = selectConfig;
        if (!selectConfig) {
            nowConfig = {};
        }
        if (!nowConfig.defaultValue) {
            nowConfig.defaultValue = "";
        }
        if (xyListObject.isReadOnly(metaInfo.columnInfo)) {
            var disValue = nowConfig.defaultValue;
            var optionList = nowConfig.optionList;
            if (optionList) {
                var optionItem;
                for (var optionIdx = 0; optionIdx < optionList.length; optionIdx++) {
                    optionItem = optionList[optionIdx];
                    if (defaultValue == optionItem.key) {
                        disValue = optionItem.value;
                    }
                }
            }
            if (xyListObject.isView()) {
                return "<div class=\"xy-table-cell\">" + disValue + "</div>";
            } else {
                var h = "<input " + xyKeyStr + "  xytype='hidden' type='hidden' id='" + fieldKey + "' name='" + fieldKey + "' value='" + defaultValue + "'>";
                h += "<input type='text' id='" + fieldKey + "_dis' name='" + fieldKey + "_dis' value='" + disValue + "' class='xy-list-input xy-readonly' readonly='readonly'>";
                return h;
            }
        } else {
            var onblurStr = 'onchange="xyListModule.onblurFun(\'' + xyListObject.bodyIndex + '\',\'' + fieldKey + '\')"';
            var h = "<select " + onblurStr + " " + xyKeyStr + " xytype='select'  id='" + fieldKey + "' name='" + fieldKey + "'  class='xy-list-select'>";
            h += "<option value=''>-请选择-</option>";
            if (optionList) {
                var optionItem;
                for (var optionIdx = 0; optionIdx < optionList.length; optionIdx++) {
                    optionItem = optionList[optionIdx];
                    if (defaultValue == optionItem.key) {
                        h += "<option value='" + optionItem.key + "' selected>" + optionItem.value + "</option>";
                    } else {
                        h += "<option value='" + optionItem.key + "'>" + optionItem.value + "</option>";
                    }
                }

            }
            h += "</select>";
            return h;
        }

    };

    //选择列
    xyListObject.checkbox = function (metaInfo, saveFlag) {
        var dataIndex = metaInfo.dataIndex;
        var name = metaInfo.key;
        var defaultValue = metaInfo.value;
        var fieldKey = xyListObject.getFieldKey(name, dataIndex);
        if (xyListObject.isView()) {
            if (defaultValue == 'Y') {
                return "<div class=\"xy-table-cell\">是</div>";
            } else {
                return "<div class=\"xy-table-cell\">否</div>";
            }
        }
        var xyKeyStr = " xykey='" + name + "'";
        if (saveFlag && saveFlag == 'N') {
            xyKeyStr = "";
        }
        if (xyListObject.isReadOnly(metaInfo.columnInfo)) {
            if (defaultValue == 'Y') {
                var h = "<input " + xyKeyStr + " xytype='hidden' type='hidden' id='" + fieldKey + "' name='" + fieldKey + "' value='Y'>"
                h += "<input type='checkbox' id='" + fieldKey + "_dis' name='" + fieldKey + "_dis' value='Y' checked  class='xy-list-checkbox  xy-readonly'>"
                return h;
            } else {
                var h = "<input " + xyKeyStr + " xytype='hidden' type='hidden' id='" + fieldKey + "' name='" + fieldKey + "' value='N'>"
                h += "<input type='checkbox' id='" + fieldKey + "_dis' name='" + fieldKey + "_dis' value='N'   class='xy-list-checkbox  xy-readonly'>"
                return h;
            }
        } else {
            var onblurStr = 'onchange="xyListModule.onblurFun(\'' + xyListObject.bodyIndex + '\',\'' + fieldKey + '\')"';
            if (defaultValue == 'Y') {
                return "<input " + onblurStr + " " + xyKeyStr + " xytype='checkbox' type='checkbox' id='" + fieldKey + "' name='" + fieldKey + "' value='Y' checked  class='xy-list-checkbox' >";
            } else {
                return "<input " + onblurStr + " " + xyKeyStr + " xytype='checkbox' type='checkbox' id='" + fieldKey + "' name='" + fieldKey + "' class='xy-list-checkbox' style='display: block'>";
            }
        }


    };


    //旧方法已废弃
    xyListObject.textAndSelect = function (metaInfo, eventInfo, saveFlag) {
        return xyListObject.text(metaInfo, saveFlag);
    };
    //弹出选择
    xyListObject.texSelect0 = function (metaInfo, saveFlag) {
        var dataIndex = metaInfo.dataIndex;
        var name = metaInfo.key;
        var defaultValue = metaInfo.value;
        var fieldKey = xyListObject.getFieldKey(name, dataIndex);
        if (xyListObject.isView()) {
            return "<div class=\"xy-table-cell\">" + defaultValue + "</div>";
        }
        var xyKeyStr = " xykey='" + name + "'";
        if (saveFlag && saveFlag == 'N') {
            xyKeyStr = "";
        }
        if (xyListObject.isReadOnly(metaInfo.columnInfo)) {
            return "<input " + xyKeyStr + " xytype='text' type='text' id='" + fieldKey + "' name='" + fieldKey + "' value='" + defaultValue + "' class='xy-list-input xy-readonly' readonly='readonly'>";
        } else {
            var onblurStr = 'onclick="xyListModule.onblurFun(\'' + xyListObject.bodyIndex + '\',\'' + fieldKey + '\')"';
            return "<input " + onblurStr + " " + xyKeyStr + " xytype='text' type='text' id='" + fieldKey + "' name='" + fieldKey + "' value='" + defaultValue + "' class='xy-list-search' readonly='readonly'>";
        }
    };

    //日期
    xyListObject.date = function (metaInfo, saveFlag, dateConfig) {
        var dataIndex = metaInfo.dataIndex;
        var name = metaInfo.key;
        var defaultValue = metaInfo.value;
        var fieldKey = xyListObject.getFieldKey(name, dataIndex);
        if (xyTable.isView()) {
            return "<div class=\"xy-table-cell\">" + defaultValue + "</div>";
        }
        var xyKeyStr = " xykey='" + name + "'";
        if (saveFlag && saveFlag == 'N') {
            xyKeyStr = "";
        }
        if (xyTable.isReadOnly(metaInfo.columnInfo)) {
            return "<input " + xyKeyStr + "  xytype='date' type='text' id='" + fieldKey + "' name='" + fieldKey + "' value='" + defaultValue + "' class='xy-input xy-readonly' readonly='readonly'>";
        } else {
            var nowConfig = dateConfig;
            if (!nowConfig) {
                nowConfig = {"cache": "N"};
            } else {
                if (!nowConfig.cache) {
                    nowConfig = {"cache": "N"};
                }
            }
            nowConfig['key'] = fieldKey;
            xyListObject.toInitDateList.push(nowConfig);
            return "<input " + xyKeyStr + "  xytype='date' type='text' id='" + fieldKey + "' name='" + fieldKey + "' value='" + defaultValue + "' class='xy-date date-icon'>";
        }
    };


    //字段*************************************************************************************************************************************************************
    return xyListObject;
};
//默认
var xyTable = xyListModule.createXyList();