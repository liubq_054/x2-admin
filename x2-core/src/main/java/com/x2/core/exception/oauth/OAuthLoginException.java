package com.x2.core.exception.oauth;

import com.x2.kernel.model.exception.AbstractBaseExceptionEnum;
import com.x2.kernel.model.exception.ServiceException;

/**
 * 第三方登录异常
 *
 * @author x2
 * @Date 2019/6/9 18:43
 */
public class OAuthLoginException extends ServiceException {

    public OAuthLoginException(AbstractBaseExceptionEnum exception) {
        super(exception);
    }

}
