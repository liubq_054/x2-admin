package com.x2.core.util;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class RequestUtil {

    /**
     * 取得参数
     *
     * @param request
     * @return
     */
    public static Map<String, Object> getParam(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        Enumeration<String> ite = request.getParameterNames();
        while (ite.hasMoreElements()) {
            String key = ite.nextElement();
            map.put(key, request.getParameter(key));
        }
        return map;
    }

    /**
     * 取得参数
     *
     * @param request
     * @return
     */
    public static Map<String, Object> getWorkflowBean(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        Enumeration<String> ite = request.getParameterNames();
        while (ite.hasMoreElements()) {
            String key = ite.nextElement();
            key = key.trim();
            if (key.length() <= 1) {
                continue;
            }
            if (key.indexOf("workflowBean.") == 0) {
                key = key.substring("workflowBean.".length());
            }
            key = key.trim();
            if (key.length() <= 1) {
                continue;
            }
            if (key.endsWith("_")) {
                map.put(key, request.getParameter(key));
                map.put(key.substring(0, key.length() - 1), request.getParameter(key));
            } else {
                map.put(key, request.getParameter(key));
                map.put(key + "_", request.getParameter(key));
            }

        }
        return map;
    }
}
