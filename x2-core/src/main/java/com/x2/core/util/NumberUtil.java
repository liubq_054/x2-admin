package com.x2.core.util;

import java.math.BigDecimal;

public class NumberUtil extends cn.hutool.core.util.NumberUtil {
    public static boolean less0(BigDecimal bigNum1) {
        if (bigNum1 == null) {
            bigNum1 = BigDecimal.ZERO;
        }
        return bigNum1.compareTo(BigDecimal.ZERO) < 0;
    }

    public static boolean lessOrEqual0(BigDecimal bigNum1) {
        if (bigNum1 == null) {
            bigNum1 = BigDecimal.ZERO;
        }
        return bigNum1.compareTo(BigDecimal.ZERO) <= 0;
    }

    public static boolean ess(BigDecimal bigNum1, BigDecimal bigNum2) {
        if (bigNum1 == null) {
            bigNum1 = BigDecimal.ZERO;
        }
        if (bigNum2 == null) {
            bigNum2 = BigDecimal.ZERO;
        }
        return bigNum1.compareTo(bigNum2) < 0;
    }

    public static boolean isLessOrEqual(BigDecimal bigNum1, BigDecimal bigNum2) {
        if (bigNum1 == null) {
            bigNum1 = BigDecimal.ZERO;
        }
        if (bigNum2 == null) {
            bigNum2 = BigDecimal.ZERO;
        }
        return bigNum1.compareTo(bigNum2) <= 0;
    }

}
