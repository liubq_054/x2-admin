/**
 * Copyright 2018-2020 stylefeng & fengshuonan (sn93@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.x2.core.util;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.x2.core.properties.AppNameProperties;
import com.x2.kernel.model.exception.ServiceException;
import com.x2.kernel.model.exception.enums.CoreExceptionEnum;
import com.x2.kernel.model.util.ValidateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Closeable;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.*;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 高频方法集合
 *
 * @author x2
 * @Date 2018/3/18 21:55
 */
public class ToolUtil extends ValidateUtil {

    /**
     * 默认密码盐长度
     */
    public static final int SALT_LENGTH = 6;

    /**
     * 获取随机字符,自定义长度
     *
     * @author x2
     * @Date 2018/3/18 21:55
     */
    public static String getRandomString(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * md5加密(加盐)
     *
     * @author x2
     * @Date 2018/3/18 21:56
     */
    public static String md5Hex(String password, String salt) {
        return md5Hex(password + salt);
    }

    /**
     * md5加密(不加盐)
     *
     * @author x2
     * @Date 2018/3/18 21:56
     */
    public static String md5Hex(String str) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] bs = md5.digest(str.getBytes());
            StringBuffer md5StrBuff = new StringBuffer();
            for (int i = 0; i < bs.length; i++) {
                if (Integer.toHexString(0xFF & bs[i]).length() == 1)
                    md5StrBuff.append("0").append(Integer.toHexString(0xFF & bs[i]));
                else
                    md5StrBuff.append(Integer.toHexString(0xFF & bs[i]));
            }
            return md5StrBuff.toString();
        } catch (Exception e) {
            throw new ServiceException(CoreExceptionEnum.ENCRYPT_ERROR);
        }
    }

    /**
     * 过滤掉掉字符串中的空白
     *
     * @author x2
     * @Date 2018/3/22 15:16
     */
    public static String removeWhiteSpace(String value) {
        if (isEmpty(value)) {
            return "";
        } else {
            return value.replaceAll("\\s*", "");
        }
    }

    /**
     * 获取某个时间间隔以前的时间 时间格式：yyyy-MM-dd HH:mm:ss
     *
     * @author x2
     * @Date 2018/5/8 22:05
     */
    public static String getCreateTimeBefore(int seconds) {
        long currentTimeInMillis = Calendar.getInstance().getTimeInMillis();
        Date date = new Date(currentTimeInMillis - seconds * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }

    /**
     * 获取异常的具体信息
     *
     * @author x2
     * @Date 2017/3/30 9:21
     * @version 2.0
     */
    public static String getExceptionMsg(Throwable e) {
        StringWriter sw = new StringWriter();
        try {
            e.printStackTrace(new PrintWriter(sw));
        } finally {
            try {
                sw.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return sw.getBuffer().toString().replaceAll("\\$", "T");
    }

    /**
     * 获取应用名称
     *
     * @author x2
     * @Date 2018/5/12 上午10:24
     */
    public static String getApplicationName() {
        try {
            AppNameProperties appNameProperties =
                    SpringContextHolder.getBean(AppNameProperties.class);
            if (appNameProperties != null) {
                return appNameProperties.getName();
            } else {
                return "";
            }
        } catch (Exception e) {
            Logger logger = LoggerFactory.getLogger(ToolUtil.class);
            logger.error("获取应用名称错误！", e);
            return "";
        }
    }

    /**
     * 获取ip地址
     *
     * @author x2
     * @Date 2018/5/15 下午6:36
     */
    public static String getIP() {
        try {
            StringBuilder IFCONFIG = new StringBuilder();
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress() && inetAddress.isSiteLocalAddress()) {
                        IFCONFIG.append(inetAddress.getHostAddress().toString() + "\n");
                    }

                }
            }
            return IFCONFIG.toString();

        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 拷贝属性，为null的不拷贝
     *
     * @author x2
     * @Date 2018/7/25 下午4:41
     */
    public static void copyProperties(Object source, Object target) {
        BeanUtil.copyProperties(source, target, CopyOptions.create().setIgnoreNullValue(true).ignoreError());
    }

    /**
     * 判断是否是windows操作系统
     *
     * @author x2
     * @Date 2017/5/24 22:34
     */
    public static Boolean isWinOs() {
        String os = System.getProperty("os.name");
        if (os.toLowerCase().startsWith("win")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取临时目录
     *
     * @author x2
     * @Date 2017/5/24 22:35
     */
    public static String getTempPath() {
        return System.getProperty("java.io.tmpdir");
    }

    /**
     * 把一个数转化为int
     *
     * @author x2
     * @Date 2017/11/15 下午11:10
     */
    public static Integer toInt(Object val) {
        if (val instanceof Double) {
            BigDecimal bigDecimal = new BigDecimal((Double) val);
            return bigDecimal.intValue();
        } else {
            return Integer.valueOf(val.toString());
        }

    }

    /**
     * 是否为数字
     *
     * @author x2
     * @Date 2017/11/15 下午11:10
     */
    public static boolean isNum(Object obj) {
        try {
            Integer.parseInt(obj.toString());
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 获取项目路径
     *
     * @author x2
     * @Date 2017/11/15 下午11:10
     */
    public static String getWebRootPath(String filePath) {
        try {
            String path = ToolUtil.class.getClassLoader().getResource("").toURI().getPath();
            path = path.replace("/WEB-INF/classes/", "");
            path = path.replace("/target/classes/", "");
            path = path.replace("file:/", "");
            if (ToolUtil.isEmpty(filePath)) {
                return path;
            } else {
                return path + "/" + filePath;
            }
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取文件后缀名 不包含点
     *
     * @author x2
     * @Date 2017/11/15 下午11:10
     */
    public static String getFileSuffix(String fileWholeName) {
        if (ToolUtil.isEmpty(fileWholeName)) {
            return "none";
        }
        int lastIndexOf = fileWholeName.lastIndexOf(".");
        return fileWholeName.substring(lastIndexOf + 1);
    }


    public static Integer toInteger(String value) {
        return toInteger(value, (Integer) null);
    }

    public static Integer toInteger(String value, Integer defaultValue) {
        try {
            return Integer.parseInt(value);
        } catch (Exception var3) {
            return defaultValue;
        }
    }

    public static Long toLong(String value) {
        return toLong(value, (Long) null);
    }

    public static Long toLong(String value, Long defaultValue) {
        try {
            return Long.parseLong(value);
        } catch (Exception var3) {
            return defaultValue;
        }
    }

    public static boolean isEmpty(String word) {
        return word == null || word.trim().length() == 0;
    }


    public static String[] splitStr(String str, char c) {
        if (str == null) {
            return null;
        } else {
            str = str + c;
            int n = 0;

            for (int i = 0; i < str.length(); ++i) {
                if (str.charAt(i) == c) {
                    ++n;
                }
            }

            String[] out = new String[n];

            for (int i = 0; i < n; ++i) {
                int index = str.indexOf(c);
                out[i] = str.substring(0, index);
                str = str.substring(index + 1, str.length());
            }

            return out;
        }
    }

    public static BigDecimal add(BigDecimal arg1, BigDecimal arg2) {
        return arg1.add(arg2);
    }

    public static BigDecimal subtract(BigDecimal arg1, BigDecimal arg2) {
        return arg1.subtract(arg2);
    }

    public static BigDecimal multiply(BigDecimal arg1, BigDecimal arg2) {
        return arg1.multiply(arg2);
    }

    public static BigDecimal scale(BigDecimal arg1, int roundNum) {
        return arg1.setScale(roundNum, 4);
    }

    public static String replaceStr(String source, String oldString, String newString) {
        if (source == null) {
            return null;
        } else {
            StringBuffer output = new StringBuffer();
            int lengthOfSource = source.length();
            int lengthOfOld = oldString.length();
            int posStart = 0;
            String lower_s = source.toLowerCase();

            int pos;
            for (String lower_o = oldString.toLowerCase(); (pos = lower_s.indexOf(lower_o, posStart)) >= 0; posStart = pos + lengthOfOld) {
                output.append(source.substring(posStart, pos));
                output.append(newString);
            }

            if (posStart < lengthOfSource) {
                output.append(source.substring(posStart));
            }

            return output.toString();
        }
    }

    public static String convertContent(String value, Object objClazz) {
        return convertContent("\\$\\{\\w+\\}", value, objClazz);
    }

    public static String convertContent(String regEx, String value, Object objClazz) {
        if (objClazz != null && !isEmpty(value)) {
            Map<String, Object> columnMap = new HashMap(10);

            int i;
            try {
                BeanInfo info = Introspector.getBeanInfo(objClazz.getClass());
                PropertyDescriptor[] pds = info.getPropertyDescriptors();
                PropertyDescriptor[] var6 = pds;
                i = pds.length;

                for (int var8 = 0; var8 < i; ++var8) {
                    PropertyDescriptor pd = var6[var8];
                    columnMap.put(pd.getName(), pd.getReadMethod().invoke(objClazz));
                }
            } catch (Exception var10) {
                var10.printStackTrace();
            }

            Pattern p = Pattern.compile(regEx);
            Matcher matcher = p.matcher(value);
            StringBuffer sb = new StringBuffer();

            while (matcher.find()) {
                for (i = 0; i <= matcher.groupCount(); ++i) {
                    String word = matcher.group(i);
                    word = replaceStr(word, "{", "");
                    word = replaceStr(word, "}", "");
                    String columnValue = columnMap.get(word) == null ? "" : columnMap.get(word) + "";
                    matcher.appendReplacement(sb, columnValue);
                }
            }

            matcher.appendTail(sb);
            value = sb.toString();
            return value;
        } else {
            return value;
        }
    }

    public static List<Object> getValueParam(String value, Object objClazz) {
        if (objClazz != null && !isEmpty(value)) {
            Map<String, Object> columnMap = new HashMap(10);

            int i;
            try {
                BeanInfo info = Introspector.getBeanInfo(objClazz.getClass());
                PropertyDescriptor[] pds = info.getPropertyDescriptors();
                PropertyDescriptor[] var5 = pds;
                int var6 = pds.length;

                for (i = 0; i < var6; ++i) {
                    PropertyDescriptor pd = var5[i];
                    columnMap.put(pd.getName(), pd.getReadMethod().invoke(objClazz));
                }
            } catch (Exception var9) {
                var9.printStackTrace();
            }

            String regEx = "\\$\\{\\w+\\}";
            Pattern p = Pattern.compile(regEx);
            Matcher matcher = p.matcher(value);
            List<Object> valueList = new ArrayList();

            while (matcher.find()) {
                for (i = 0; i <= matcher.groupCount(); ++i) {
                    String word = matcher.group(i);
                    word = replaceStr(word, "${", "");
                    word = replaceStr(word, "}", "");
                    valueList.add(columnMap.get(word) == null ? "" : columnMap.get(word) + "");
                }
            }

            return valueList;
        } else {
            return null;
        }
    }

    public static String firstUpper(String word) {
        char[] chars = word.toCharArray();
        if (chars[0] >= 'a' && chars[0] <= 'z') {
            chars[0] = (char) (chars[0] - 32);
        }

        return new String(chars);
    }

    public static String returnEmpty(String word) {
        return word != null && word.trim().length() != 0 && !word.equals("null") ? word : "";
    }

    /**
     * 获取object的值
     *
     * @author x2
     * @Date 2019-04-04 17:07
     */
    public static Long toLong(Object object) {
        return NumUtil.getLong(object);
    }


    public static void closeQuietly(Closeable obj) {
        if (obj != null) {
            try {
                obj.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


}
