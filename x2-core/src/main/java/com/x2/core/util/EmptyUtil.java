package com.x2.core.util;

import java.util.Collection;
import java.util.Map;

/**
 * 判空工具类
 *
 * @author liubq
 * @since 20170711
 */
public class EmptyUtil {

    /**
     * 判空
     *
     * @param dataList
     * @return
     */
    public static boolean empty(Collection dataList) {
        if (dataList == null || dataList.size() == 0) {
            return true;
        }
        return false;
    }
    /**
     * 判空
     *
     * @param dataList
     * @return
     */
    public static boolean isEmpty(Collection dataList) {
        if (dataList == null || dataList.size() == 0) {
            return true;
        }
        return false;
    }

    /**
     * 判空
     *
     * @param map
     * @return
     */
    public static boolean empty(Map map) {
        if (map == null || map.size() == 0) {
            return true;
        }
        return false;
    }

    /**
     * 判空
     *
     * @param map
     * @return
     */
    public static boolean isEmpty(Map map) {
        if (map == null || map.size() == 0) {
            return true;
        }
        return false;
    }

    /**
     * 判空
     *
     * @param sequence
     * @return
     */
    public static boolean empty(String sequence) {
        if (sequence == null || sequence.length() == 0) {
            return true;
        }
        return false;
    }

    /**
     * 判空
     *
     * @param sequence
     * @return
     */
    public static boolean isEmpty(String sequence) {
        if (sequence == null || sequence.length() == 0) {
            return true;
        }
        return false;
    }
}
