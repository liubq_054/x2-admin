package com.x2.core.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 时间工具类
 * @author liubq
 */
public class LocalDateUtil {

    /**
     * Date 转 LocalDateTime
     *
     * @param time
     * @return
     */
    public static LocalDateTime toLocal(Date time) {
        if (time == null) {
            return null;
        }
        return time.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }


    /**
     * LocalDateTime 转 Date
     *
     * @param time
     * @return
     */
    public static Date toDate(LocalDateTime time) {
        if (time == null) {
            return null;
        }
        return Date.from(time.atZone(ZoneId.systemDefault()).toInstant());
    }


    /**
     * 字符串转LocalDateTime ，日期格式yyyy-MM-dd
     *
     * @param date
     * @return
     */
    public static LocalDateTime toLocal(String date) {
        return toLocal(date, "yyyy-MM-dd");
    }

    /**
     * 字符串转LocalDateTime ，日期格式yyyy-MM-dd HH:mm:ss
     *
     * @param date
     * @return
     */
    public static LocalDateTime toLocal2(String date) {
        return toLocal(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 字符串转LocalDateTime
     *
     * @param date
     * @param pattern
     * @return
     */
    public static LocalDateTime toLocal(String date, String pattern) {
        if (date == null || date.trim().length() == 0) {
            return null;
        }
        if (pattern == null || pattern.trim().length() == 0) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDateTime.parse(date, formatter);
    }

    /**
     * 日期格式化 转字符串，日期格式yyyy-MM-dd
     *
     * @param date
     * @return
     */
    public static String format(LocalDateTime date) {
        return format(date, "yyyy-MM-dd");
    }

    /**
     * 日期格式化 转字符串，日期格式yyyy-MM-dd HH:mm:ss
     *
     * @param date
     * @return
     */
    public static String format2(LocalDateTime date) {
        return format(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 日期格式化 转字符串
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String format(LocalDateTime date, String pattern) {
        if (date == null) {
            return null;
        }
        if (pattern == null || pattern.trim().length() == 0) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return date.format(formatter);
    }


}
