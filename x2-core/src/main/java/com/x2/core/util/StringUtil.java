package com.x2.core.util;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
    public StringUtil() {
    }

    public static String firstLower(String str) {
        if (trimIsEmpty(str)) {
            return "";
        } else {
            StringBuffer resultBuffer = new StringBuffer();
            resultBuffer.append(str.substring(0, 1).toLowerCase());
            resultBuffer.append(str.substring(1));
            String result = resultBuffer.toString();
            return result;
        }
    }

    public static String firstUpper(String str) {
        if (trimIsEmpty(str)) {
            return "";
        } else {
            StringBuffer resultBuffer = new StringBuffer();
            resultBuffer.append(str.substring(0, 1).toUpperCase());
            resultBuffer.append(str.substring(1));
            String result = resultBuffer.toString();
            return result;
        }
    }

    public static boolean trimIsEmpty(String str) {
        if (str == null) {
            return true;
        } else {
            String newStr = str.trim();
            return isEmpty(newStr);
        }
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static String getJspName(String name, boolean state) {
        if (isEmpty(name)) {
            return name;
        } else if (state) {
            return name.indexOf(".jsp") != -1 ? name : name + ".jsp";
        } else {
            return name.indexOf(".jsp") != -1 ? name.substring(0, name.indexOf(".jsp")) : name;
        }
    }

    /*public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(str).matches();
    }*/

    /** @deprecated */
    @Deprecated
    public static String escapeSQLWildcard(String str) {
        return isEmpty(str) ? str : str.replace("_", "\\_");
    }

    public static String escapeSQLSpecialChar(String str) {
        return isEmpty(str) ? str : str.replace("'", "''").replace("%", "\\%");
    }

    public static String html2Text(String inputString) {
        String htmlStr = inputString;
        String textStr = "";

        try {
            String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>";
            Pattern p_style = Pattern.compile(regEx_style, 2);
            Matcher m_style = p_style.matcher(htmlStr);
            htmlStr = m_style.replaceAll("");
            textStr = htmlStr;
        } catch (Exception var10) {
            System.err.println("Html2Text: " + var10.getMessage());
        }

        return textStr;
    }

    public static String filterHtml(String inputString) {
        String htmlStr = inputString;
        String textStr = "";

        try {
            String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>";
            String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>";
            String regEx_html = "<[^>]+>";
            Pattern p_script = Pattern.compile(regEx_script, 2);
            Matcher m_script = p_script.matcher(htmlStr);
            htmlStr = m_script.replaceAll("");
            Pattern p_style = Pattern.compile(regEx_style, 2);
            Matcher m_style = p_style.matcher(htmlStr);
            htmlStr = m_style.replaceAll("");
            Pattern p_html = Pattern.compile(regEx_html, 2);
            Matcher m_html = p_html.matcher(htmlStr);
            htmlStr = m_html.replaceAll("");
            textStr = htmlStr;
        } catch (Exception var12) {
            System.err.println("Html2Text: " + var12.getMessage());
        }

        return textStr;
    }

    public static String getTitle4M(String inputString) {
        String outString = "";
        if (inputString.length() > 15) {
            outString = inputString.substring(0, 12) + "...";
        } else {
            outString = inputString;
        }

        return outString;
    }

    public static String replaceNBSP(String inputString) {
        String outString = "";
        if (!isEmpty(inputString)) {
            outString = inputString.replace("&nbsp;", " ");
        }

        return outString;
    }

    public static String getColumnTypeStr(String type) {
        if (isEmpty(type)) {
            return "";
        } else if (type.equals(Integer.toString(4))) {
            return "integer";
        } else if (type.equals(Integer.toString(12))) {
            return "varchar";
        } else if (type.equals(Integer.toString(6))) {
            return "float";
        } else if (type.equals(Integer.toString(8))) {
            return "double";
        } else if (type.equals(Integer.toString(2))) {
            return "FLOAT";
        } else if (type.equals(Integer.toString(1))) {
            return "char";
        } else if (type.equals(Integer.toString(91))) {
            return "date";
        } else if (type.equals(Integer.toString(93))) {
            return "timestamp";
        } else if (type.equals(Integer.toString(92))) {
            return "time";
        } else if (type.equals(Integer.toString(2004))) {
            return "blob";
        } else {
            return type.equals(Integer.toString(2005)) ? "clob" : type;
        }
    }

    public static boolean checkValNotNull(Object value){
        if(value != null){
            return true;
        }
        return false;
    }

}
