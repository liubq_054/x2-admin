package com.x2.core.cache.redis;


import com.x2.core.cache.CacheTool;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.Topic;

import java.nio.charset.Charset;

/**
 * @author hy
 * @createTime 2021-05-01 08:53:19
 * @description 期望是可以监听某个key的变化，而不是失效
 */
public class RedisKeyChangeListener implements MessageListener/* extends KeyspaceEventMessageListener */ {
    private static final Topic TOPIC_ALL_KEYEVENTS = new PatternTopic("__keyevent@*"); //表示只监听所有的key
    private static final Topic TOPIC_KEYNAMESPACE_NAME = new PatternTopic("__keyspace@0__:local*");

    private static final Topic TOPIC_KEYNAMESPACE_DEL = new PatternTopic("__keyevent@0__:del");
    private String keyspaceNotificationsConfigParameter = "KEAg";

    public void initAndSetRedisConfig(RedisMessageListenerContainer listenerContainer) {
        RedisConnection connection = listenerContainer.getConnectionFactory().getConnection();
        try {
            connection.setConfig("notify-keyspace-events", keyspaceNotificationsConfigParameter);
        } finally {
            connection.close();
        }
        // 注册消息监听
        listenerContainer.addMessageListener(this, TOPIC_ALL_KEYEVENTS);
    }


    @Override
    public void onMessage(Message message, byte[] pattern) {
//        String channel = new String(message.getChannel(), Charset.forName("utf-8"));
//        String patternStr = new String(pattern, Charset.forName("utf-8"));
        String key = new String(message.getBody(), Charset.forName("utf-8"));
        if (key.startsWith("local:")) {
            System.out.println("******************************************************************key发生变化===》" + key);
            CacheTool.getInstance().clearLocalKey(key);
        }
    }

}

