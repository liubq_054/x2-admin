package com.x2.core.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class LocalCache {

    // 存储空间
    private static Map<String, Map<String, CacheData>> cacheArea = new HashMap<>();

    // 清理线程
    private static ScheduledExecutorService cleaner;

    /**
     * 清理
     */
    static {
        cleaner = Executors.newScheduledThreadPool(1);
        cleaner.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                try {
                    synchronized (LocalCache.class) {
                        for (Map.Entry<String, Map<String, CacheData>> entry : cacheArea.entrySet()) {
                            if (entry.getValue().size() > 0) {
                                List<String> rmKeyList = new ArrayList<>();
                                for (Map.Entry<String, CacheData> subEntry : entry.getValue().entrySet()) {
                                    if (isExpire(subEntry.getValue())) {
                                        rmKeyList.add(entry.getKey());
                                    }
                                }
                                for (String key : rmKeyList) {
                                    entry.getValue().remove(key);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 120, 120, TimeUnit.SECONDS);

    }

    /**
     * 取得指定key的对应的数据
     *
     * @param key 标识
     * @return 数据
     */
    public static synchronized Object get(String scopeName, String key) {
        // 赋值
        Map<String, CacheData> cache = cacheArea.get(scopeName);
        if (cache == null) {
            return null;
        }
        CacheData data = cache.get(key);
        if (data != null) {
            data.refreshTime();
            return data.getData();
        }
        return null;
    }

    /**
     * 取得指定key的对应的数据，同时更新缓存开始时间
     *
     * @param key 标识
     * @return 数据
     */
    public static synchronized Object getAndRefreshTime(String scopeName, String key) {
        // 赋值
        Map<String, CacheData> cache = cacheArea.get(scopeName);
        if (cache == null) {
            return null;
        }
        CacheData data = cache.get(key);
        if (data != null) {
            data.refreshTime();
            return data.getData();
        }
        return null;
    }

    /**
     * 缓存指定数据
     *
     * @param scopeName 区域名称
     * @param key       标识
     * @param data      数据
     */
    public static void put(String scopeName, String key, Object data) {
        put(scopeName, key, data, 0);
    }

    /**
     * 缓存指定数据
     *
     * @param scopeName    区域名称
     * @param key          标识
     * @param data         数据
     * @param expireSecond 缓存时间 单位:秒
     */
    public static synchronized void put(String scopeName, String key, Object data, int expireSecond) {
        // 赋值
        Map<String, CacheData> cache = cacheArea.get(scopeName);
        if (cache == null) {
            cache = new HashMap<>();
        }
        cache.put(key, new CacheData(data, expireSecond));
        cacheArea.put(scopeName, cache);
    }


    /**
     * 缓存指定数据
     *
     * @param scopeName    区域名称
     * @param key          标识
     * @param data         数据
     * @param expireSecond 缓存时间 单位:秒
     */
    public static synchronized boolean putByToken(String scopeName, String key, Object data, int expireSecond) {
        // 赋值
        Map<String, CacheData> cache = cacheArea.get(scopeName);
        if (cache == null) {
            cache = new HashMap<>();
        }
        //存在则不成功
        CacheData value = cache.get(key);
        if (value == null || value.getData() == null) {
            cache.put(key, new CacheData(data, expireSecond));
            cacheArea.put(scopeName, cache);
            return true;
        } else if (value.getData().equals(data)) {
            return true;
        }
        return false;
    }

    /**
     * 移除指定数据
     *
     * @param scopeName 区域名称
     * @param key       标识
     * @param data      数据
     */
    public static synchronized boolean removeByToken(String scopeName, String key, Object data) {
        // 赋值
        Map<String, CacheData> cache = cacheArea.get(scopeName);
        if (cache == null) {
            cache = new HashMap<>();
        }
        //存在则不成功
        CacheData value = cache.get(key);
        if (value == null || value.getData() == null) {
            return true;
        }
        if (!value.getData().equals(data)) {
            return false;
        }
        cache.remove(key);
        return true;
    }

    /**
     * 清理单个值
     *
     * @param key
     */
    public static synchronized void clear(String scopeName, String key) {
        // 赋值
        Map<String, CacheData> cache = cacheArea.get(scopeName);
        if (cache != null) {
            cache.remove(key);
        }
    }

    /**
     * 清理全部
     */
    public static synchronized void clearAll(String scopeName) {
        cacheArea.remove(scopeName);
    }

    /**
     * 是否过期
     *
     * @param data
     * @return true 过期了，false 没有过期
     */
    private static boolean isExpire(CacheData data) {
        if (data != null) {
            if (data.getExpire() <= 0) {
                return false;
            } else if (data.getSaveTime() + data.getExpire() + 100 >= System.currentTimeMillis()) {
                return false;
            }

        }
        return true;
    }


    /**
     * 缓存对象
     *
     * @author liubq
     */
    private static class CacheData {

        /**
         * 缓存对象
         *
         * @param t      数据
         * @param expire 缓存时间 单位:秒
         */
        CacheData(Object t, int expire) {
            this.data = t;
            this.expire = expire <= 0 ? 0 : expire * 1000;
            this.saveTime = System.currentTimeMillis();
        }

        // 数据
        private Object data;
        // 开始时间
        private long saveTime;
        // 缓存时间
        private long expire;

        /**
         * 数据
         *
         * @return
         */
        public Object getData() {
            return data;
        }


        /**
         * 缓存时间
         *
         * @return
         */
        public long getExpire() {
            return expire;
        }

        /**
         * 开始时间
         *
         * @return
         */
        public long getSaveTime() {
            return saveTime;
        }

        /**
         * 更新时间
         *
         * @return
         */
        public void refreshTime() {
            this.saveTime = System.currentTimeMillis();
        }
    }
}
