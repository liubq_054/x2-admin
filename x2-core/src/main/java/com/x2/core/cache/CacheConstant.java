package com.x2.core.cache;


/**
 * 缓存相关常量类
 */
public class CacheConstant {

    /**
     * 本地缓存前缀
     */
    public static final String LOCAL_PRE = "local:";

    /**
     * 本地key
     *
     * @param key
     * @return
     */
    public static boolean isLocalKey(String key) {
        if (key != null && key.startsWith(LOCAL_PRE)) {
            return true;
        }
        return false;
    }

}
