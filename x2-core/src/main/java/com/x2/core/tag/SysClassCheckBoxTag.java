package com.x2.core.tag;

import com.x2.core.util.EmptyUtil;
import com.x2.core.util.StringUtil;
import lombok.Data;
import org.beetl.core.tag.GeneralVarTagBinding;

import java.util.Map;

/**
 * 指定类的下拉框
 *
 * @author liubq
 * @date 2021-12-09
 */
@Data
public class SysClassCheckBoxTag extends GeneralVarTagBinding {
    private String id;
    private String name;
    private String className;
    private String layVerify;
    public String headName;
    public String headValue;
    public String headType;
    public String defaultValue;
    public String onClick;
    public String onChange;
    public String workFlowForm;
    public String itemName;

    public void initAttr() {
        Map<String, Object> attrs = this.getAttributes();
        if (attrs.size() > 0) {
            if (StringUtil.checkValNotNull(attrs.get("id"))) {
                this.setId(attrs.get("id").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("name"))) {
                this.setName(attrs.get("name").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("className"))) {
                this.setClassName(attrs.get("className").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("layVerify"))) {
                this.setLayVerify(attrs.get("layVerify").toString());
            } else {
                this.setLayVerify(this.getId());
            }
            if (StringUtil.checkValNotNull(attrs.get("headValue"))) {
                this.setHeadValue(attrs.get("headValue").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("headType"))) {
                this.setHeadType(attrs.get("headType").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("headName"))) {
                this.setHeadName(attrs.get("headName").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("defaultValue"))) {
                this.setDefaultValue(attrs.get("defaultValue").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("workFlowForm"))) {
                this.setWorkFlowForm(attrs.get("workFlowForm").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("itemName"))) {
                this.setItemName(attrs.get("itemName").toString());
            }
        }
    }

    @Override
    public void render() {
        initAttr();
        StringBuilder sb = new StringBuilder();
        try {
            Class<?> clazz = Class.forName(className);
            Object obj = clazz.newInstance();
            sb.append(" <div class='enumRadio'>");
            if (obj instanceof IClassTagSelect) {
                IClassTagSelect impl = (IClassTagSelect) obj;
                for (ClassSelectOptionVO option : impl.getOptionList()) {
                    String itemCode = option.getCode();
                    String itemName = option.getName();
                    String selected = defaultValue != null && defaultValue.equals(itemCode) ? "checked" : "";

                    String id = this.getId() + "_" + itemCode;
                    sb.append(" <input type='checkbox' name='").append(name).append("' id='").append(id).append("'")
                            .append("' title='").append(itemName).append("'")
                            .append(" value='").append(itemCode).append("'").append(selected);
                    if (!EmptyUtil.isEmpty(this.getWorkFlowForm())) {
                        sb.append(" workFlowForm='" + this.getWorkFlowForm() + "'");
                    }
                    if (this.getOnChange() != null) {
                        sb.append(" lay-filter='").append(this.getOnChange()).append("'");
                    }
                    if (!EmptyUtil.isEmpty(this.getItemName())) {
                        sb.append(" itemName='" + this.getItemName() + "'");
                    }
                    if (this.getLayVerify() != null) {
                        sb.append(" lay-verify='").append(this.getLayVerify()).append("'");
                    }
                    sb.append(" /> ");
                }
            }
            sb.append("</div> ");
            this.ctx.byteWriter.writeString(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
