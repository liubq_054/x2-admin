package com.x2.core.tag;

import java.util.List;

public interface IClassTagSelect {

    List<ClassSelectOptionVO> getOptionList();
}
