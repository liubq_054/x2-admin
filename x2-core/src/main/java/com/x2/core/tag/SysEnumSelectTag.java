package com.x2.core.tag;

import com.x2.core.util.StringUtil;
import lombok.Data;
import org.beetl.core.tag.GeneralVarTagBinding;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * 枚举类型的下拉框
 *
 * @author changp
 * @date 2020-06-12
 */
@Data
public class SysEnumSelectTag extends GeneralVarTagBinding {
    private String id;
    private String name;
    private String className;
    private String codeColumn;
    private String valueColumn;
    private String layVerify;
    public String headName;
    public String headValue;
    public String headType;
    public String defaultValue;
    public String onClick;
    public String onChange;
    public String workFlowForm;
    public String itemName;

    public void initAttr() {
        Map<String, Object> attrs = this.getAttributes();
        if (attrs.size() > 0) {
            if (StringUtil.checkValNotNull(attrs.get("id"))) {
                this.setId(attrs.get("id").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("name"))) {
                this.setName(attrs.get("name").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("className"))) {
                this.setClassName(attrs.get("className").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("codeColumn"))) {
                this.setCodeColumn(attrs.get("codeColumn").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("valueColumn"))) {
                this.setValueColumn(attrs.get("valueColumn").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("layVerify"))) {
                this.setLayVerify(attrs.get("layVerify").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("headValue"))) {
                this.setHeadValue(attrs.get("headValue").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("headType"))) {
                this.setHeadType(attrs.get("headType").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("headName"))) {
                this.setHeadName(attrs.get("headName").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("defaultValue"))) {
                this.setDefaultValue(attrs.get("defaultValue").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("onClick"))) {
                this.setOnClick(attrs.get("onClick").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("onChange"))) {
                this.setOnChange(attrs.get("onChange").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("workFlowForm"))) {
                this.setWorkFlowForm(attrs.get("workFlowForm").toString());
            }
            if (StringUtil.checkValNotNull(attrs.get("itemName"))) {
                this.setItemName(attrs.get("itemName").toString());
            }
        }
    }

    @Override
    public void render() {
        initAttr();
        StringBuilder sb = new StringBuilder();
        sb.append("<select name='" + this.getName() + "' id='" + this.getId() + "' ");

        if (layVerify != null) {
            sb.append(" lay-verify='").append(layVerify == null ? "" : layVerify).append("' ");
        } else {
            sb.append(" lay-verify='").append(this.getName()).append("' ");
        }
        if (workFlowForm != null) {
            sb.append(" workFlowForm='").append(workFlowForm == null ? "" : workFlowForm).append("' ");
        }
        if (itemName != null) {
            sb.append(" itemName='").append(itemName == null ? "" : itemName).append("' ");
        }
        sb.append(" >");

        if (headName != null) {
            sb.append("<option value='").append(headValue == null ? "" : headValue).append("' selected>")
                    .append(headName).append("</option>");
        }

        if (headType != null) {
            if ("1".equals(headType)) {
                sb.append("<option value='").append(headValue == null ? "" : headValue).append("' selected>").append(" - 全部 - ").append("</option>");
            }
            if ("2".equals(headType)) {
                sb.append("<option value='").append(headValue == null ? "" : headValue).append("' selected>").append(" - 请选择 - ").append("</option>");
            }
        }
        try {
            Class<?> clazz = Class.forName(className);
            Object[] objects = clazz.getEnumConstants();
            Method codeMethod = clazz.getMethod(codeColumn);
            Method valueMethod = clazz.getMethod(valueColumn);
            for (Object obj : objects) {
                String code = codeMethod.invoke(obj) + "";
                String selected = defaultValue != null && defaultValue.equals(code) ? "selected" : "";
                sb.append("<option value='" + codeMethod.invoke(obj) + "' " + selected + ">" + valueMethod.invoke(obj) + "</option>");
            }
            sb.append("</select>");
            this.ctx.byteWriter.writeString(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
