package com.x2.core.tag;

public class ClassSelectOptionVO {
    private String code;
    private String name;

    public ClassSelectOptionVO() {
    }

    public ClassSelectOptionVO(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
