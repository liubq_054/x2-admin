package com.x2.core.tag;
 
import com.x2.core.util.EmptyUtil;
import com.x2.core.util.StringUtil;
import lombok.Data;
import org.beetl.core.tag.GeneralVarTagBinding;

import java.lang.reflect.Method;
import java.util.Map;

@Data
public class SysEnumCheckboxTag extends GeneralVarTagBinding {
    private String id;
    private String name;
    private String className;
    private String codeColumn;
    private String valueColumn;
    private String layVerify;
    public String headName;
    public String headValue;
    public String headType;
    public String defaultValue;
    public String onClick;
    public String onChange;
    public String workFlowForm;
    public String itemName;
    public void initAttr(){
        Map<String, Object> attrs = this.getAttributes();
        if(attrs.size() > 0){
            if(StringUtil.checkValNotNull(attrs.get("workFlowForm"))){
                this.setWorkFlowForm(attrs.get("workFlowForm").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("itemName"))){
                this.setItemName(attrs.get("itemName").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("id"))){
                this.setId(attrs.get("id").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("name"))){
                this.setName(attrs.get("name").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("className"))){
                this.setClassName(attrs.get("className").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("codeColumn"))){
                this.setCodeColumn(attrs.get("codeColumn").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("valueColumn"))){
                this.setValueColumn(attrs.get("valueColumn").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("layVerify"))){
                this.setLayVerify(attrs.get("layVerify").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("headValue"))){
                this.setHeadValue(attrs.get("headValue").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("headType"))){
                this.setHeadType(attrs.get("headType").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("headName"))){
                this.setHeadName(attrs.get("headName").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("defaultValue"))){
                this.setDefaultValue(attrs.get("defaultValue").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("onClick"))){
                this.setOnClick(attrs.get("onClick").toString());
            }
            if(StringUtil.checkValNotNull(attrs.get("onChange"))){
                this.setOnChange(attrs.get("onChange").toString());
            }
        }
    }
    @Override
    public void render() {
        initAttr();
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(" <div class='enumRadio'>");
            Class<?> clazz = Class.forName(className);
            Object[] objects = clazz.getEnumConstants();
            Method codeMethod = clazz.getMethod(codeColumn);
            Method valueMethod = clazz.getMethod(valueColumn);
            for (Object obj : objects) {
                String code = codeMethod.invoke(obj) + "";
                String checked = defaultValue != null && defaultValue.equals(code) ? "checked" : "";
                String id = this.getId() + "_" + code;
                sb.append(" <input type='checkbox' name='").append(name).append("' id='").append(id).append("'")
                        .append("' title='").append(valueMethod.invoke(obj)).append("'")
                        .append(" value='").append(code).append("'").append(checked);
                if (!EmptyUtil.isEmpty(this.getWorkFlowForm())) {
                    sb.append(" workFlowForm='" + this.getWorkFlowForm() + "'");
                }
                if(this.getOnChange()!=null){
                    sb.append(" lay-filter='").append(this.getOnChange()).append("'");
                }
                if (!EmptyUtil.isEmpty(this.getItemName())){
                    sb.append(" itemName='" + this.getItemName() + "'");
                }
                if (this.getLayVerify() != null) {
                    sb.append(" lay-verify='").append(this.getLayVerify()).append("'");
                }
                sb.append(" /> ");
            }
            sb.append("</div> ");
            this.ctx.byteWriter.writeString(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
