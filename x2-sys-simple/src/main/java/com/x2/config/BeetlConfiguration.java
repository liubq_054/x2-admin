/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.x2.config;

import com.x2.base.auth.context.LoginContext;
import com.x2.base.auth.context.LoginContextHolder;
import com.x2.base.consts.ConstantsContext;
import com.x2.core.tag.SysClassCheckBoxTag;
import com.x2.core.tag.SysClassSelectTag;
import com.x2.core.tag.SysEnumSelectTag;
import com.x2.core.util.ToolUtil;
import org.beetl.ext.spring6.BeetlTemplateCustomize;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * beetl拓展配置,绑定一些工具类,方便在模板中直接调用
 *
 * @author x2
 * @Date 2018/2/22 21:03
 */
@Configuration
@ConditionalOnBean(LoginContext.class)
public class BeetlConfiguration {

    @Bean
    public BeetlTemplateCustomize beetlTemplateCustomize() {
        return groupTemplate -> {
            groupTemplate.registerTag("enum_select", SysEnumSelectTag.class);
            groupTemplate.registerTag("class_select", SysClassSelectTag.class);
            groupTemplate.registerTag("class_checkbox", SysClassCheckBoxTag.class);
            groupTemplate.registerFunctionPackage("shiro", LoginContextHolder.getContext());
            groupTemplate.registerFunctionPackage("tool", new ToolUtil());
            groupTemplate.registerFunctionPackage("constants", new ConstantsContext());
        };
    }
}
