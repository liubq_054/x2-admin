package com.x2.modular.demo.test.mapper;

import com.x2.modular.demo.test.entity.Order;
import com.x2.modular.demo.test.model.OrderBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * @title  
 * @version  
 */
public interface OrderMapper extends BaseMapper<Order> {

   List<OrderBean> queryList(@Param("paramCondition") OrderBean paramCondition);

   Page<OrderBean> queryPageList(@Param("page") Page page, @Param("paramCondition") OrderBean paramCondition);

}
