package com.x2.modular.demo.test.controller;


import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.core.base.controller.BaseController;
import com.x2.kernel.model.response.ResponseData;
import com.x2.modular.demo.test.model.OrderBean;
import com.x2.modular.demo.test.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 控制器
 *
 * @author liubq
 * @Date 2020-09-07 10:25:00
 */
@Controller
@RequestMapping("/order")
public class OrderController extends BaseController {

    private String PREFIX = "/demo/test";

    @Autowired
    private IOrderService orderService;

    /**
     * 跳转到主页面
     */
    @RequestMapping("/manage")
    public String manage() {
        return PREFIX + "/order.html";
    }

    /**
     * 新增页面
     */
    @RequestMapping("/add")
    public String add() {
        return edit();
    }

    /**
     * 编辑页面
     */
    @RequestMapping("/edit")
    public String edit() {
        return PREFIX + "/orderForm.html";
    }

    /**
     * 新增接口
     */
    @RequestMapping("/addItem")
    @ResponseBody
    public ResponseData addItem(OrderBean param) {
        try {
            this.orderService.add(param);
            return ResponseData.success();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseData.error(ex.getMessage());
        }
    }

    /**
     * 编辑接口
     */
    @RequestMapping("/editItem")
    @ResponseBody
    public ResponseData editItem(OrderBean param) {
        try {
            this.orderService.update(param);
            return ResponseData.success();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseData.error(ex.getMessage());
        }
    }

    /**
     * 删除接口
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResponseData delete(OrderBean param) {
        try {
            this.orderService.delete(param);
            return ResponseData.success();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseData.error(ex.getMessage());
        }
    }

    /**
     * 查看详情接口
     */
    @RequestMapping("/detail")
    @ResponseBody
    public ResponseData detail(OrderBean param) {
        OrderBean detail = this.orderService.findBySpec(param);
        return ResponseData.success(detail);
    }

    /**
     * 查询列表
     */
    @ResponseBody
    @RequestMapping("/list")
    public LayuiPageInfo list(OrderBean param) {
        return this.orderService.findPageBySpec(param);
    }

}

