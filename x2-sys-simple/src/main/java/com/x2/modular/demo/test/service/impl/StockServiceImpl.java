package com.x2.modular.demo.test.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.dynamic.datasource.annotation.DSTransactional;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.x2.base.pojo.page.LayuiPageFactory;
import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.core.util.ToolUtil;
import com.x2.modular.demo.test.entity.Stock;
import com.x2.modular.demo.test.mapper.StockMapper;
import com.x2.modular.demo.test.model.OrderBean;
import com.x2.modular.demo.test.model.StockBean;
import com.x2.modular.demo.test.service.IOrderService;
import com.x2.modular.demo.test.service.IStockService;
import com.x2.modular.demo.test.service.OrderProxyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Version 1.0
 */
@Service
@DS(value = "slave")
public class StockServiceImpl extends ServiceImpl<StockMapper, Stock> implements IStockService {
    //跨库直接调用，对应默认没有声明DS的，不会进行看切换，这个是开源的bug
//    @Autowired
//    private IOrderService orderService;


    @Autowired
    private OrderProxyService orderService;

    @Override
    @DSTransactional(rollbackFor = Exception.class)
    public void add(StockBean param) throws Exception {
        Stock entity = getEntity(param);
        this.save(entity);
    }

    @Override
    @DSTransactional(rollbackFor = Exception.class)
    public void update(StockBean param) throws Exception {
        Stock newEntity = getEntity(param);
        this.updateById(newEntity);
        OrderBean order = new OrderBean();
        ToolUtil.copyProperties(newEntity, order);
        order.setCreateTime(new Date());
        order.setId(null);
        orderService.add(order);
    }

    @Override
    @DSTransactional(rollbackFor = Exception.class)
    public void delete(StockBean param) throws Exception {
        this.removeById(getKey(param));
    }

    @Override
    public StockBean findBySpec(StockBean param) {
        List<StockBean> list = baseMapper.queryList(param);
        if (list == null || list.size() == 0) {
            return null;
        }
        return list.get(0);
    }


    @Override
    public List<StockBean> findListBySpec(StockBean param) {
        return baseMapper.queryList(param);
    }

    @Override
    public LayuiPageInfo findPageBySpec(StockBean param) {
        Page pageContext = getPageContext();
        pageContext.addOrder(OrderItem.desc("t.id"));
        IPage page = this.baseMapper.queryPageList(pageContext, param);
        return LayuiPageFactory.createPageInfo(page);
    }

    private Serializable getKey(StockBean param) {
        return param.getId();
    }

    private Page getPageContext() {
        return LayuiPageFactory.defaultPage();
    }

    private Stock getEntity(StockBean param) {
        Stock entity = new Stock();
        ToolUtil.copyProperties(param, entity);
        return entity;
    }

}

