package com.x2.modular.demo.test.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.dynamic.datasource.annotation.DSTransactional;
import com.baomidou.mybatisplus.extension.service.IService;
import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.core.util.ToolUtil;
import com.x2.modular.demo.test.entity.Order;
import com.x2.modular.demo.test.entity.Stock;
import com.x2.modular.demo.test.model.OrderBean;
import com.x2.modular.demo.test.model.StockBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 服务
 *
 * @Version 1.0
 */
@Service
@DS("master")
public class OrderProxyService {

    @Autowired
    private IOrderService orderService;

    @DSTransactional(rollbackFor = Exception.class)
    public void add(OrderBean order) throws Exception {
        orderService.add(order);
    }

}
