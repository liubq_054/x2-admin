package com.x2.modular.demo.test.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.x2.base.pojo.page.LayuiPageInfo;
import com.x2.modular.demo.test.entity.Order;
import com.x2.modular.demo.test.model.OrderBean;

import java.util.List;

/**
 * 服务
 *
 * @Version 1.0
 */
public interface IOrderService extends IService<Order> {

    void add(OrderBean param) throws Exception;

    void update(OrderBean param) throws Exception;

    void delete(OrderBean param) throws Exception;

    OrderBean findBySpec(OrderBean param);

    List<OrderBean> findListBySpec(OrderBean param);

    LayuiPageInfo findPageBySpec(OrderBean param);

}
