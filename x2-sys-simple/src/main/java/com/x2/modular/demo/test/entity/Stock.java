package com.x2.modular.demo.test.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.alibaba.fastjson.annotation.JSONField;
import java.util.Date;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 属性
 * @author liubq
 * @version 2020-07-10
 */
@TableName("test_stock")
@Data
public class Stock implements Serializable {

	private static final long serialVersionUID = 1L;

    //id
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private Integer id;
	//productId
	@TableField("product_id")
	private Integer productId;
	//count
	@TableField("count")
	private Integer count;
}