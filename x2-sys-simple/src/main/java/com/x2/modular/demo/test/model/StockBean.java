package com.x2.modular.demo.test.model;

import com.x2.modular.demo.test.entity.Stock;
import lombok.Data;

import java.io.Serializable;

/**
 * @author liubq
 * @version 2020-07-10
 */
@Data
public class StockBean extends Stock implements Serializable {

    private static final long serialVersionUID = 1L;


}