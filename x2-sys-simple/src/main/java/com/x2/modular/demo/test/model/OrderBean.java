package com.x2.modular.demo.test.model;

import com.x2.modular.demo.test.entity.Order;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author liubq
 * @version 2020-07-10
 */
@Data
public class OrderBean extends Order implements Serializable {

    private static final long serialVersionUID = 1L;
    //createTime开始
    private Date createTimeBegin;
    //createTime结束
    private Date createTimeEnd;


}