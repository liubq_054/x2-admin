package com.x2;

import org.beetl.ext.spring6.EnableBeetl;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * SpringBoot方式启动类
 *
 * @author x2
 * @Date 2017/5/21 12:06
 */
@SpringBootApplication()
@EnableTransactionManagement
@MapperScan(basePackages = {"com.x2.**.mapper"})
@EnableBeetl
public class SimpleDemoApplication {
    private final static Logger logger = LoggerFactory.getLogger(SimpleDemoApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SimpleDemoApplication.class, args);
        logger.info(SimpleDemoApplication.class.getSimpleName() + " is success!");
    }
}
