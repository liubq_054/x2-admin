package com.x2.core.auth.userdetail;

import com.x2.base.auth.model.LoginUser;
import com.x2.core.auth.LoginUtil;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 用户详情信息获取
 *
 * @author x2
 * @Date 2019-09-28 14:07
 */
@Service("jwtUserDetailsService")
public class JwtUserDetailsServiceImpl implements UserDetailsService {
    @Override
    public LoginUser loadUserByUsername(String username) throws UsernameNotFoundException {
        return LoginUtil.createLoginUser();
    }
}
