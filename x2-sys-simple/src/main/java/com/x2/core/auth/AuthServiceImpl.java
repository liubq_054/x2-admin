/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.x2.core.auth;

import com.x2.base.auth.model.AuthVo;
import com.x2.base.auth.model.LoginUser;
import com.x2.base.auth.service.AuthService;
import com.x2.core.util.SpringContextHolder;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
@DependsOn("springContextHolder")
@Transactional(readOnly = true)
public class AuthServiceImpl implements AuthService {

    public static AuthService me() {
        return SpringContextHolder.getBean(AuthService.class);
    }

    @Override
    public String login( String username) {
        return null;
    }

    @Override
    public String login(String username, String password) {
        return null;
    }



    @Override
    public void addLoginCookie(String token) {

    }

    @Override
    public void logout() {
    }


    @Override
    public LoginUser user(String account) {
        return LoginUtil.createLoginUser();
    }

    @Override
    public List<String> findPermissionsByRoleId(Long roleId) {
        return Arrays.asList("1");
    }

    @Override
    public boolean check(String[] roleNames) {
        return true;
    }

    @Override
    public boolean checkAll() {
        return true;
    }

}
