package com.x2.core.auth.util;

import com.x2.base.pojo.node.MenuNode;
import com.x2.core.listener.ConfigListener;

import java.util.ArrayList;
import java.util.List;

public class SysMenuDef {

    /**
     * 所有菜单
     *
     * @return
     */
    public static List<MenuNode> getManageMenus() {
        String cxt = ConfigListener.getConf().get("contextPath");
        MenuNode top = new MenuNode();
        top.setId(200L);
        top.setParentId(0L);
        top.setName("工作台");
        top.setNum(1);
        top.setIcon("zmdi zmdi-star");
        top.setIsmenu("Y");
        top.setLevels(0);

        MenuNode item1 = new MenuNode();
        item1.setId(210L);
        item1.setParentId(200L);
        item1.setName("首页");
        item1.setNum(2);
        item1.setIcon("zmdi zmdi-star");
        item1.setUrl(cxt + "/index/home");
        item1.setIsmenu("Y");
        item1.setLevels(1);
        item1.setSystemType("200");

        MenuNode item3 = new MenuNode();
        item3.setId(212L);
        item3.setParentId(200L);
        item3.setName("测试订单");
        item3.setNum(2);
        item3.setIcon("zmdi zmdi-star");
        item3.setUrl(cxt + "/order/manage");
        item3.setIsmenu("Y");
        item3.setLevels(1);
        item3.setSystemType("200");

        MenuNode item2 = new MenuNode();
        item2.setId(211L);
        item2.setParentId(200L);
        item2.setName("测试库存");
        item2.setNum(2);
        item2.setIcon("zmdi zmdi-star");
        item2.setUrl(cxt + "/stock/manage");
        item2.setIsmenu("Y");
        item2.setLevels(1);
        item2.setSystemType("200");

        List<MenuNode> list = new ArrayList<>();
        list.add(top);
        list.add(item1);
        list.add(item2);
        list.add(item3);
        return list;
    }

}
