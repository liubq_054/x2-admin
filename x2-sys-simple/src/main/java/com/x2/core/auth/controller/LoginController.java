/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.x2.core.auth.controller;

import com.x2.core.base.controller.BaseController;
import com.x2.core.auth.util.SysMenuUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * 登录控制器
 *
 * @author x2
 * @Date 2017年1月10日 下午8:25:24
 */
@Controller
public class LoginController extends BaseController {


    /**
     * 跳转到主页
     *
     * @author x2
     * @Date 2018/12/23 5:41 PM
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String loginGet(Model model) {
        Map<String, Object> userIndexInfo = SysMenuUtil.getUserIndexInfo();
        model.addAllAttributes(userIndexInfo);
        return "/index.html";
    }

    /**
     * 跳转到登录页面
     *
     * @author x2
     * @Date 2018/12/23 5:41 PM
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPost(Model model) {
        Map<String, Object> userIndexInfo = SysMenuUtil.getUserIndexInfo();
        model.addAllAttributes(userIndexInfo);
        return "/index.html";
    }


    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Model model) {
        return "/home.html";
    }

}