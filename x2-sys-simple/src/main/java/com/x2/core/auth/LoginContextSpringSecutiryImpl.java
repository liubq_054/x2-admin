package com.x2.core.auth;

import com.x2.base.auth.context.LoginContext;
import com.x2.base.auth.model.LoginUser;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * 用户登录上下文
 *
 * @author x2
 * @Date 2019/7/18 22:27
 */
@Component(value="loginContext")
public class LoginContextSpringSecutiryImpl implements LoginContext {

    @Override
    public LoginUser getUser() {
        return LoginUtil.createLoginUser();
    }

    @Override
    public String getToken() {
        return "";
    }

    @Override
    public boolean hasLogin() {
        return true;
    }

    @Override
    public Long getUserId() {
        return 1L;
    }

    @Override
    public boolean hasRole(String roleName) {
        return true;
    }

    @Override
    public boolean hasAnyRoles(String roleNames) {
        return true;
    }

    @Override
    public boolean hasPermission(String permission) {
        return true;
    }

    @Override
    public boolean isAdmin() {
        return true;
    }
}
