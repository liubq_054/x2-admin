package com.x2.core.auth;

import com.x2.base.auth.model.LoginUser;

import java.util.HashMap;

public class LoginUtil {

    /**
     * 用户
     *
     * @return
     */
    public static LoginUser createLoginUser() {
        LoginUser loginUser = new LoginUser();
        loginUser.setId(1L);
        loginUser.setAccount("admin");
        loginUser.setName("管理员");
        loginUser.setExtendMap(new HashMap<>());
        return loginUser;
    }
}
