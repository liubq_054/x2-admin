layui.use(['form', 'admin', 'ax',  'func'], function () {
    var $ = layui.jquery;
    var $ax = layui.ax;
    var form = layui.form;
    var admin = layui.admin;
    var func = layui.func;
    var laydate = layui.laydate;
    //管理
    var OrderModule = {
        formId: "orderForm"
    };

    laydate.render({
        elem: '#createTime', //开始时间
        type: "date",
        shortcuts: [
            {
                text: "昨天",
                value: function(){
                    var now = new Date();
                    now.setDate(now.getDate() - 1);
                    return now;
                }()
            },
            { text: "今天", value: Date.now() },
            {
                text: "明天",
                value: function(){
                    var now = new Date();
                    now.setDate(now.getDate() + 1);
                    return now;
                }()
            },
            {
                text: "上个月",
                value: function(){
                    var now = new Date();
                    // now.setDate(now.getDate() - 1);
                    now.setMonth(now.getMonth() - 1);
                    return [now];
                }()
            },
            {
                text: "上个月的前一天",
                value: function(){
                    var now = new Date();
                    now.setMonth(now.getMonth() - 1);
                    now.setDate(now.getDate() - 1);
                    return [now];
                }()
            },
            {
                text: "某一天",
                value: "2016-10-14 11:32:32"
            }
        ]
    });


    //新增 0， 编辑 1，其它 2
    OrderModule.openFlag = 0;
    //获取详情信息，填充表单
    var editId = Feng.getUrlParam("id");
    var openMode = Feng.getUrlParam("openMode");
    if(editId){
        OrderModule.openFlag = 1;
        new $ax(Feng.ctxPath + "/order/detail?id=" + editId,function(result){
            form.val(OrderModule.formId, result.data);
            if(openMode == 'view' || openMode == 'look'){
                OrderModule.openFlag = 2;
                xyForm.readOnly(layui,OrderModule.formId);
            }
        }).start();
    }

    //表单提交事件
    form.on('submit(btnSubmit)', function (data) {
        var url = Feng.ctxPath + "/order/addItem";
        if(OrderModule.openFlag > 0){
            url = Feng.ctxPath + "/order/editItem";
        }
        var ajax = new $ax(url, function (data) {
            if(data.success){
                Feng.success("保存成功！");
                //传给上个页面，刷新table用
                admin.putTempData('formOk', true);
                //关掉对话框
                admin.closeThisDialog();
            } else {
                Feng.alert(data.message)
            }
        }, function (data) {
            Feng.error("保存失败！" + data.responseJSON.message)
        });
        ajax.set(data.field);
        ajax.start();
        return false;
    });

});