layui.use(['table', 'admin', 'ax', 'func'], function () {
    var $ = layui.$;
    var table = layui.table;
    var $ax = layui.ax;
    var admin = layui.admin;
    var func = layui.func;
    var laydate = layui.laydate;
    //管理
    var Order = {
        tableId: "orderTable"
    };

    laydate.render({
        elem: '#query_createTime' //开始时间
        ,format: 'yyyy-MM-dd' //指定时间格式
        ,range:true
    });

    //初始化表格的列
    Order.initColumn = function () {
        return [[
            {field: 'productId', sort: true, title: 'productId'},
            {field: 'count', sort: true, title: 'count'},
            {field: 'createTime', sort: true, title: 'createTime'},
            {align: 'center', toolbar: '#tableBar', title: '操作', width:250,  fixed: 'right'}
        ]];
    };
    //点击查询按钮
    Order.queryParams = function () {
        var queryData = {};
        var query_createTime = $("#query_createTime").val();
        if (query_createTime) {
            queryData['createTimeBegin'] = query_createTime.substr(0, 10) + " 00:00:00";
            queryData['createTimeEnd'] = query_createTime.substr(13, 10) + " 23:59:59";
        }

        queryData['n_'] = new Date().getTime();
        return queryData;
    };
    //点击查询按钮
    Order.search = function () {
        table.reload(Order.tableId, {page: {curr: 1}});
    };
    //弹出添加对话框
    Order.openAddDlg = function () {
        func.open({
            title: '添加',
            height: '420',
            content: Feng.ctxPath + '/order/add',
            tableId: Order.tableId
        });
    };
    //点击编辑
    Order.openEditDlg = function (data) {
        func.openEx({
            title: '修改',
            height: '420',
            content: Feng.ctxPath + '/order/edit?id=' + data.id,
            tableId: Order.tableId
        });
    };
    //点击查看
    Order.openViewDlg = function (data) {
        func.openEx({
            title: '查看',
            height: '420',
            content: Feng.ctxPath + '/order/edit?openMode=look&id=' + data.id,
            tableId: Order.tableId
        });
    };
    //点击删除
    Order.onDeleteItem = function (data) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/order/delete", function (data) {
                if(data.success){
                    Feng.success("删除成功!");
                    table.reload(Order.tableId);
                } else {
                    Feng.error(data.message);
                }
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("id", data.id);
            ajax.start();
        };
        Feng.confirm("是否删除?", operation);
    };
    // 渲染表格
    Order.tableResult = table.render({
        elem: '#' + Order.tableId,
        url: Feng.ctxPath + '/order/list',
        queryParams: function(){return Order.queryParams()},
        page: true,
        limit:20,
        height: "full-98",
        cellMinWidth: 100,
        cols: Order.initColumn()
    });
    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        Order.search();
    });
    // 重置按钮点击事件
    $("#btnReset").click(function () {
        $('#searchForm')[0].reset();
    });
    // 添加按钮点击事件
    $('#btnAdd').click(function () {
        Order.openAddDlg();
    });
    // 工具条点击事件
    table.on('tool(' + Order.tableId + ')', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'edit') {
            Order.openEditDlg(data);
        } else if (layEvent === 'view') {
            Order.openViewDlg(data);
        } else if (layEvent === 'delete') {
            Order.onDeleteItem(data);
        }
    });
});